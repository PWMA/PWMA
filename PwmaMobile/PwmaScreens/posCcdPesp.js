import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaChart from '../PwmaComponents/PwmaChart';

export default class posCcdPesp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {},
      error: {},
      informationSystem: 'N.A.',
      gProps: {data: [ 0 ], xAccessor: d => d[0], yAccessor: d => d[1]},
    };
    this.ws = '';
    this.wsListener = this.wsListener.bind(this);
  }
  componentDidMount() {
    this.ws = this.props.globalParams.initWs();
    if (this.ws) {
      if (!this.ws.readyState) {
        this.ws.onopen = () => {
          this.subscribeVar();
        };
      } else {
        this.subscribeVar();
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      const obj = `tango://ken.elettra.trieste.it:20000/test/hamamatsustoragebridge/olivo/monitored`;
      fetch(`${this.props.globalParams.server}/cs/${obj}`, {
        method: 'UNSUBSCRIBE',
      }).then((response) => {
        if (response.status>=300) {
          console.log('response', response.status, response._bodyText, obj);
        }
      })
      .catch((err) => {
        console.log('.catch((err)', err, obj);
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  wsListener(event) {
    if (event.data === undefined) return;
    const eventData = JSON.parse(event.data);
    if (eventData.data === undefined || eventData.data.value === undefined) return;
    this.setState({ gProps: {data: eventData.data.value, xAccessor: d => d[0], yAccessor: d => d[1]} });
  }
  subscribeVar() {
    // const obj = `tango://ken.elettra.trieste.it:20000/pos/pespstore/ccd-pesp_pos.01>GetHorProfile`;
    // const obj = `tango://srv-tango-srf.fcs.elettra.trieste.it:20000/sl/diagnostics/spctm100_slr.01/Data`;
    const obj = `tango://ken.elettra.trieste.it:20000/test/hamamatsustoragebridge/olivo/monitored`;
    fetch(`${this.props.globalParams.server}/cs/${obj}`, {
      method: 'SUBSCRIBE',
    }).then((response) => {
      if (response.status>=300) {
          const value = this.state.value;
          const error = this.state.error;
          value[obj] = 'Error.';
          error[obj] = response._bodyText;
          this.setState({ value, error, });
        console.log('response', response.status, response._bodyText, obj);
      }
    })
    .catch((err) => {
      console.log('.catch((err)', err, obj);
      // console.error(`Error reading ${obj}\n${err}`);
      const value = this.state.value;
      const error = this.state.error;
      value[obj] = 'N.A.';
      error[obj] = err;
    });
  }
  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        <PwmaChart {...this.state.gProps} xScaleType="linear" height={300} title="pos/pespstore/ccd-pesp_pos.01->GetHorProfile" max={500000} />
      </PwmaMain>
    );
  }
}

posCcdPesp.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
