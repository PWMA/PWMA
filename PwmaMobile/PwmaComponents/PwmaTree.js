// derived from https://github.com/rnative/react-native-treeview
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  Image,
  // Animated,
  TouchableOpacity,
} from 'react-native';

let id = 0;
let instanceCounter = 0;

const styles = {
  tree: {
    padding: 10,
  },
  rootnode: {
    paddingBottom: 10,
  },
  node: {
    paddingTop: 8,
  },
  item: {
    flexDirection: 'row',
  },
  children: {
    paddingLeft: 20,
  },
  icon: {
    paddingRight: 10,
    color: '#333',
    alignSelf: 'center',
  },
  image: {
    paddingTop: 24,
    paddingRight: 28,
  },
  roottext: {
    fontSize: 20,
  },
  text: {
    fontSize: 20,
  },
  button: {
    // https://facebook.github.io/react-native/docs/pixelratio.html
    fontSize: 20,
    borderRadius: 3,
    backgroundColor: '#2196F3',
    borderColor: '#2196F3',
    color: 'white',
  },
};

// method "require()" requires a constant https://stackoverflow.com/questions/30854232/
// todo: use icons instead of images
const blueRight = require('../img/blue_right.png');
const blueDown = require('../img/blue_down.png');
const redRight = require('../img/red_right.png');
const redDown = require('../img/red_down.png');
const orangeRight = require('../img/orange_right.png');
const orangeDown = require('../img/orange_down.png');
const greenRight = require('../img/green_right.png');
const greenDown = require('../img/green_down.png');
const grayRight = require('../img/gray_right.png');
const grayDown = require('../img/gray_down.png');

const icons = {
  blue: { right: blueRight, down: blueDown },
  red: { right: redRight, down: redDown },
  orange: { right: orangeRight, down: orangeDown },
  green: { right: greenRight, down: greenDown },
  gray: { right: grayRight, down: grayDown },
};

function getStyle(type, tag) {
  return [styles[tag], styles[type + tag]];
}

class PwmaTree extends Component {

  constructor(props) {
    super(props);
    const expanded = [];
    if (this.props.defaultPosition === 'close') {
      // console.debug(`initTree(), data.length: ${this.props.data.length}`);
      // initTree(this.props.data, expanded);
    }
    this.state = {
      expanded,
    };
  }
  componentWillReceiveProps() {
    instanceCounter += 1;
    // console.log(`PwmaTree::instanceCounter: ${instanceCounter}`);
  }
  getNodeView(type, node) {
    if (this.props.hideRoot && type === 'root') {
      return (
        <View />
      );
    }
    const { expanded } = this.state;
    // const iconSize = type === 'root' ? 22 : 20;
    const hasChildren = !!node.data;
    const direction = expanded[node.id] ? 'down' : 'right';
    const nIcon = icons[node.color ? node.color : 'blue'][direction];
    const icon = node.icon ? node.icon : nIcon;
    return (
      <View style={getStyle(type, 'item')}>
        {
          !hasChildren && !node.icon ? null :
          <Image style={getStyle(type, 'image')} source={icon} />
        }
        <Text style={getStyle(type, !hasChildren ? 'button' : 'text')}> {node.title} </Text>
      </View>
    );
  }

  getNode(type, node) {
    const { expanded } = this.state;
    const { renderItem, appendItem } = this.props;
    // const hasChildren = !!node.data;

    return (
      <View key={`${node.id}_${instanceCounter}`} style={getStyle(type, 'node')} >
        <View style={getStyle(type, 'item')} >
          <TouchableOpacity onPress={() => this.toggleState.bind(this)(node)} >
            {renderItem ? renderItem(type, node) : this.getNodeView(type, node)}
          </TouchableOpacity>
          {appendItem && appendItem(type, node)}
        </View>
        <View style={styles.children}>
          {
            (this.props.hideRoot === false || type === 'children') && !expanded[node.id] ? null : this.getTree('children', node.data || [])
          }
        </View>
      </View>
    );
  }

  getTree(type, d) {
    const nodes = [];
    const data = d;
    for (let i = 0; i < data.length; i += 1) {
      if (data[i].id === undefined) { data[i].id = id; id += 1; }
      nodes.push(this.getNode(type, data[i]));
    }
    return nodes;
  }

  toggleState(node) {
    // console.debug(`toggleState, node.id: ${node.id}, node.title: ${node.title}`);
    const { expanded } = this.state;
    const { onItemClicked } = this.props;

    expanded[node.id] = !expanded[node.id];
    this.setState({
      expanded,
    });
    if (onItemClicked) {
      onItemClicked(node);
    }
  }

  render() {
    const { data } = this.props;
    return (
      <View style={styles.tree}>
        {this.getTree('root', data)}
      </View>
    );
  }
}

PwmaTree.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  onItemClicked: PropTypes.func,
  hideRoot: PropTypes.bool,
  defaultPosition: PropTypes.oneOf(['close', 'open']),
  renderItem: PropTypes.func,
  appendItem: PropTypes.func,
};

PwmaTree.defaultProps = {
  data: [],
  onItemClicked: null,
  hideRoot: true,
  defaultPosition: 'close',
  renderItem: null,
  appendItem: null,
};

export default PwmaTree;
