import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ScrollView,
  StyleSheet,
} from 'react-native';
import PwmaModal from '../PwmaComponents/PwmaModal';
import PwmaLabel from '../PwmaComponents/PwmaLabel';
import PwmaInput from '../PwmaComponents/PwmaInput';

export default class PwmaMain extends Component {
  constructor() {
    super();
    this.state = {
      alarm: null,
    };
    this.alarmCallback = this.alarmCallback.bind(this);
    this.alarmAck = this.alarmAck.bind(this);
  }
  componentDidMount() {
    if (this.props.globalParams) this.props.globalParams.registerAlarmCallback(this.alarmCallback);
  }
  componentWillUnmount() {
    if (this.props.globalParams) this.props.globalParams.registerAlarmCallback(null);
  }

  alarmCallback(message) {
    this.setState({ alarm: message });
  }
  alarmAck() {
    if (this.props.globalParams.alarmNotify) this.props.globalParams.alarmNotify(this.state.alarm);
    this.setState({ alarm: null });
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: this.props.alignItems,
        backgroundColor: '#F0F5FF',
      },
    });
    return (
      <View style={styles.container}>
        <ScrollView>
          <PwmaModal visible={this.state.alarm !== null} >
            <PwmaLabel text={this.state.alarm ? (this.state.alarm === 'ws' ? ' May be you are not connected to the VPN' : (this.state.alarm === 'rest' ? ' Are you connected to the VPN?' : ' Alarm notification')) : ''} style={{ fontSize: 24, color: '#000000' }} />
            <PwmaLabel text={this.state.alarm ? (this.state.alarm === 'ws' ? 'Connection lost' : (this.state.alarm === 'rest' ? 'Connot connect server' : this.state.alarm.tag)) : ''} />
            <PwmaInput text="Dismiss" isButton callback={this.alarmAck} buttonWidth={360} />
          </PwmaModal>
          {this.props.children}
        </ScrollView>
      </View>
    );
  }
}
PwmaMain.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  alignItems: PropTypes.string,
  globalParams: PropTypes.shape({
    initWs: PropTypes.func,
    registerAlarmCallback: PropTypes.func.isRequired,
    navigation: PropTypes.object,
    alarmNotify: PropTypes.func,
  }),
};
PwmaMain.defaultProps = {
  alignItems: 'stretch',
  globalParams: null,
};
