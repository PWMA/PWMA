import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';

import update from 'react/lib/update';

class PwmaContainer extends Component {
	
	constructor(props) {
		super(props);
		this.state = {isOpen: true};
	}
	
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
	
	render() {
		
		const styles = {
			container: {
				align: 'center'
			}
		};
		
		return (
			<section style={styles.container}>
				<button onClick={() => this.toggle()}>{this.props.label}</button><br />
				{this.state.isOpen && (
					<section style={{ backgroundColor: "#f8fff8", display: this.state.display, height: "100%", minHeight: "300px"}}>
						{ this.props.children }
					</section>
				)}
			</section>
		);
	}
}

export default PwmaContainer;	

PwmaContainer.propTypes = {
	label: PropTypes.string,
	children: PropTypes.element
};

PwmaContainer.defaultProps = {
	label: "",
	children: null
};
