// use pos/pespstore/ccd-pesp_pos.01/gethorprofile
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaContainer from '../PwmaComponents/PwmaContainer';
import PwmaChart from '../PwmaComponents/PwmaChart';

const domain = 'tango://srv-tango-srf.fcs.elettra.trieste.it:20000';
const padresDomain = 'tango://srv-padres-srf.fcs.elettra.trieste.it:20000';
const beamtoValues = ['N.A.', 'gun', 'linac', 'fel', 'Fel 1', 'Fel 2', 'Fel 1 - Padres', 'Fel 2 - Padres'];
const xAccessor = d => new Date(d[0]);
const yAccessor = d => d[1];
let beamto = { data: 'N.A.' };
let opMode = { data: 'N.A.' };
let bc1 = { data: 'N.A.' };
let bc2 = { data: 'N.A.' };
let pulseLegth = { data: 'N.A.' };
let lockstatus = { data: 'N.A.' };
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}

export default class FermiStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/Undulator_access_state': 'N.A.',
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/misc/fermistatus/beamto': 'N.A.',
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/bc01/power_supply/psb_bc01.01/state': 'N.A.',
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/misc/beam_status_f/bc01_energy': 'N.A.',
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/bc02/power_supply/psb_bc02.01/state': 'N.A.',
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/misc/beam_status_f/bc02_energy': 'N.A.',
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/misc/beam_status_f/sl_pulse_length_s': 'N.A.',
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/sl/timing/tmu_sl.01/lockstatus': 'N.A.',
      },
      fel: false,
      intensitySrc: [],
      intensityUpdate: false,
      rmsSrc: [],
      rmsUpdate: false,
      error: {},
    };
    this.wsListener = this.wsListener.bind(this);
    this.readVar = this.readVar.bind(this);
  }
  componentDidMount() {
    this.ws = this.props.globalParams.initWs();
    console.log(this.ws===null);
    if (this.ws) {
      if (!this.ws.readyState) {
        console.log('readyState', this.ws.readyState);
        this.ws.onopen = () => {
          this.subscribeVar();
        };
      } else {
        this.subscribeVar();
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      Object.keys(this.state.value).map((obj) => {
        console.log(`unsubscribe: ${obj}`);
        // TODO don't unsubscribe var in error
        fetch(`${this.props.globalParams.server}/cs/subscriptions`, {
          method: 'DELETE',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify( [ obj ]),
        });
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
  }
  loadGraph(fel) {
    const intensitySrc = fel === 'FEL-2' ?
      'http://padresproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=padres&start=last%202%20hours&ts=02078&no_pretimer&no_posttimer' :
      'http://padresproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=padres&start=last%202%20hours&ts=02076&no_pretimer&no_posttimer';
    const rmsSrc = fel === 'FEL-2' ?
      'http://padresproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=padres&start=last%202%20hours&ts=02243&no_pretimer&no_posttimer' :
      'http://padresproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=padres&start=last%202%20hours&ts=02241&no_pretimer&no_posttimer';
    console.log('fetch intensity');
    fetch(intensitySrc)
    .then(response => response.json())
    .then((responseJson) => {
      console.log('response intensity', responseJson);
      if (responseJson.ts[0].data && responseJson.ts[0].data.length) {
        const graphData = responseJson.ts[0].data;
        this.setState({intensitySrc: graphData, intensityUpdate: true});
      }
    });
    fetch(rmsSrc)
    .then(response => response.json())
    .then((responseJson) => {
      console.log('response rms');
      if (responseJson.ts[0].data && responseJson.ts[0].data.length) {
        const rmsData = responseJson.ts[0].data;
        this.setState({rmsSrc: rmsData, rmsUpdate: true});
      }
    });
  }
  wsListener(event) {
    // console.log('Message from server', event.data);
    const value = this.state.value;
    let fel = this.state.fel;
    let intensitySrc = this.state.intensitySrc;
    let rmsSrc = this.state.rmsSrc;
    const error = this.state.error;
    if (typeof event.data !== 'string' || event.data.length === 0) return;
    const eventData = JSON.parse(event.data);
    const date = getDate();
    value[eventData.event] = eventData.data.error ? 'Error' : eventData.data.value;
    error[eventData.event] = eventData.data.error ? eventData.data.error : '';
    if (eventData.event === `${domain}/f/access_control/safety/undulator_access_state` && !eventData.data.error) {
      if (eventData.data.value[5]) value[eventData.event] = 'FEL-1';
      else if (eventData.data.value[6]) value[eventData.event] = 'FEL-2';
      else value[eventData.event] = ' ';
      fel = value[eventData.event];
      if (fel !== this.state.fel) this.loadGraph(fel);
    }
    if (eventData.event === `${domain}/f/misc/fermistatus/beamto`) {
      beamto = { data: beamtoValues[value[eventData.event] - 0], alt: `src: ${eventData.event}\nerror: ${error}\nlastUpdate: ${date}` };
      opMode = { data: 'User Dedicated', alt: `Operating mode\nlastUpdate: ${date}` };
    }
    if (eventData.event === `${domain}/f/modulators/modulators/kgglobalstat`) {
      if (eventData.data.value === 0) value[eventData.event] = 'OFF';
      if (eventData.data.value === 1) value[eventData.event] = 'ON';
    }
    if (eventData.event === `${domain}/sl/timing/tmu_sl.01/lockstatus`) {
      let data = eventData.data.value;
      if (data && data.length > 8) data = `${data.substring(0, 6)}...`;
      lockstatus = { data, alt: `value: ${eventData.data.value}\nsrc: ${domain}/sl/timing/tmu_sl.01/lockstatus\nerror: ${error[eventData.event]}\nlastUpdate: ${date}` };
    }
    if (eventData.event === `${domain}/f/misc/fermistatus/seed_laser_status`) {
      switch (eventData.data.value) {
        case 0:
        case 3:
        case 7: value[eventData.event] = '---'; break;
        case 1: value[eventData.event] = 'ATTENUATING'; break;
        case 2:
        case 4:
        case 6: value[eventData.event] = 'OPEN'; break;
        case 5: value[eventData.event] = 'CLOSED'; break;
        default: value[eventData.event] = '???';
      }
    }
    if (eventData.event === `${domain}/fel01/master/master_fel01.01/polarization`) {
      switch (eventData.data.value) {
        case 0: value[eventData.event] = 'N.A.'; break;
        case 1: value[eventData.event] = 'Linear vertical'; break;
        case 2: value[eventData.event] = 'Linear horizontal'; break;
        case 3: value[eventData.event] = 'Right circular'; break;
        case 4: value[eventData.event] = 'Left circular'; break;
        default: value[eventData.event] = '???';
      }
    }
    if (eventData.event === `${domain}/fel02/master_s01/master_s01_fel02.01/polarization`) {
      switch (eventData.data.value) {
        case 0: value[eventData.event] = 'N.A.'; break;
        case 1: value[eventData.event] = 'Linear vertical'; break;
        case 2: value[eventData.event] = 'Linear horizontal'; break;
        case 3: value[eventData.event] = 'Right circular'; break;
        case 4: value[eventData.event] = 'Left circular'; break;
        default: value[eventData.event] = '???';
      }
    }
    if (eventData.event === `${domain}/fel02/master_s02/master_s02_fel02.01/polarization`) {
      switch (eventData.data.value) {
        case 0: value[eventData.event] = 'N.A.'; break;
        case 1: value[eventData.event] = 'Linear vertical'; break;
        case 2: value[eventData.event] = 'Linear horizontal'; break;
        case 3: value[eventData.event] = 'Right circular'; break;
        case 4: value[eventData.event] = 'Left circular'; break;
        default: value[eventData.event] = '???';
      }
    }
    if (eventData.event === `${domain}/bc01/power_supply/psb_bc01.01/state` || eventData.event === `${domain}/f/misc/beam_status_f/bc01_energy`) {
      const energy = value[`${domain}/f/misc/beam_status_f/bc01_energy`];
      const energyStr = (value[`${domain}/bc01/power_supply/psb_bc01.01/state`] === 'ON') && energy ? (` - ${energy + 0.005}`).substring(0, 7) : ' ';
      bc1 = { data: `${value[`${domain}/bc01/power_supply/psb_bc01.01/state`]}${energyStr}`, alt: `src: ${domain}/bc01/power_supply/psb_bc01.01/state, ${domain}/f/misc/beam_status_f/bc01_energy\nenergy: ${energy}` };
    }
    if (eventData.event === `${domain}/bc02/power_supply/psb_bc02.01/state` || eventData.event === `${domain}/f/misc/beam_status_f/bc02_energy`) {
      bc2 = { data: value[`${domain}/bc02/power_supply/psb_bc02.01/state`] + (value[`${domain}/bc02/power_supply/psb_bc02.01/state`] === 'ON' ? (` - ${value[`${domain}/f/misc/beam_status_f/bc02_energy`]}`) : ' '), alt: 'bc2' };
    }
    if (eventData.event === `${domain}/f/misc/beam_status_f/sl_pulse_length_s`) {
      pulseLegth = { data: value[`${domain}/f/misc/beam_status_f/sl_pulse_length_s`].split('_')[0], alt: `src: ${domain}/f/misc/beam_status_f/sl_pulse_length_s` };
    }
    if (eventData.event === `${domain}/f/misc/beam_status_f/dbd_energy`) {
      value[eventData.event] = value[eventData.event].toFixed(2);
    }
    if (eventData.event === `${domain}/f/misc/fermistatus/seed_laser_pulse_energy`) {
      value[eventData.event] = value[eventData.event].toFixed(4);
    }
    this.setState({ value, error, fel, dateTime: getDate() });
  }
  subscribeVar() {
    console.log('subscribeVar()');
    Object.keys(this.state.value).map((obj) => {
      console.log(`subscribe: ${obj}`);
      fetch(`${this.props.globalParams.server}/cs/${obj}`, {
        method: 'SUBSCRIBE',
      }).then((response) => {
        if (response.status>=300) {
          if (obj === `${domain}/f/misc/fermistatus/beamto`) {
            beamto = { data: 'Error.', alt: `src: ${obj}\nerr: ${response._bodyText}` };
          }
          else {
            const value = this.state.value;
            const error = this.state.error;
            value[obj] = 'Error.';
            error[obj] = response._bodyText;
            this.setState({ value, error, });
          }
          console.log('response', response.status, response._bodyText, obj);
        }
      })
      .catch((err) => {
        console.log('.catch((err)', err, obj);
        // console.error(`Error reading ${obj}\n${err}`);
        if (obj === `${domain}/f/misc/fermistatus/beamto`) {
          beamto = { data: 'N.A.', alt: `src: ${obj}\nerror: ${err}` };
        }
        const value = this.state.value;
        const error = this.state.error;
        value[obj] = 'N.A.';
        error[obj] = err;
        /*
        this.setState({ value, error, });
        */
      });
      // console.error(obj);
      return false;
    });
  }
  readVar(src) {
    const lvar = src.toLowerCase();
    if (!this.state.value[lvar]) {
      this.state.value[lvar] = 'N.A.';
      // if (this.ws) this.ws.send(`{"data":{"type":"subscribe"},"event":"${lvar}"}`);
    }
    return { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${getDate()}` };
  }

  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        {this.state.intensityUpdate && (
          <PwmaChart title={`${this.state.fel} I0 monitor Intensity (last 2 hours)`} key="chart1" data={this.state.intensitySrc} update={false} xAccessor={xAccessor} yAccessor={yAccessor} height={180} max={100} min={0} fill={true} xScaleType="time" />
        )}
        {this.state.rmsUpdate && (
          <PwmaChart title={`${this.state.fel} I0 monitor RMS (last 2 hours)`} key="chart2" data={this.state.rmsSrc} update={false} xAccessor={xAccessor} yAccessor={yAccessor} height={180} max={100} min={0} fill={true} xScaleType="time" />
        )}
        <PwmaContainer label="Status">
          <PwmaScalar label="Beam to" valueWidth={180} value={beamto} />
          <PwmaScalar label="FEL active" valueWidth={180} value={this.readVar(`${domain}/f/access_control/safety/undulator_access_state`)} />
          <PwmaScalar label="Operating Mode" valueWidth={180} value={opMode} labelAlign="right" />
          <PwmaScalar label="Beam Line" valueWidth={180} value={this.readVar(`${padresDomain}/pos/posmaster/pathto/Beamline`)} labelAlign="right" />
        </PwmaContainer>
        <PwmaContainer label="Linac / e-beam">
          <PwmaScalar label="RF plants" valueWidth={120} unit=" " value={this.readVar(`${domain}/f/modulators/modulators/kgglobalstat`)} />
          <PwmaScalar label="E-beam Energy" valueWidth={120} unit="GeV" value={this.readVar(`${domain}/f/misc/beam_status_f/dbd_energy`)} />
          <PwmaScalar label="BC-1/Energy" valueWidth={120} unit="GeV" value={bc1} />
          <PwmaScalar label="BC-2/Energy" valueWidth={120} unit="GeV" value={bc2} />
          <PwmaScalar label="Bunch Charge" valueWidth={120} unit="pC" value={this.readVar(`${domain}/lh/diagnostics/cm_lh.01/charge`)} />
        </PwmaContainer>
        <PwmaContainer label="Seed Laser">
          <PwmaScalar label="Status" valueWidth={160} unit=" " value={this.readVar(`${domain}/f/misc/fermistatus/seed_laser_status`)} />
          <PwmaScalar label="Locking" valueWidth={160} unit=" " value={lockstatus} />
          <PwmaScalar label="Pulse energy" valueWidth={160} unit="uJ" value={this.readVar(`${domain}/f/misc/fermistatus/seed_laser_pulse_energy`)} />
          <PwmaScalar label="Pulse length" valueWidth={160} unit="fs" value={pulseLegth} />
          <PwmaScalar label="Wavelength" valueWidth={160} unit="nm" value={this.readVar(`${domain}/f/misc/beam_status_f/sl01_wavelength`)} />
        </PwmaContainer>
        <PwmaContainer label="FEL-1" visible={this.readVar(`${domain}/f/access_control/safety/undulator_access_state`).data === 'FEL-1'}>
          <PwmaScalar label="Harmonic" valueWidth={170} unit=" " value={this.readVar(`${domain}/f/misc/beam_status_f/FEL01_harmonic_number`)} />
          <PwmaScalar label="Wavelenght" valueWidth={170} unit="nm" value={this.readVar(`${domain}/f/misc/beam_status_f/FEL01_wavelength`)} />
          <PwmaScalar label="Polarization" valueWidth={170} unit=" " value={this.readVar(`${domain}/FEL01/master/master_FEL01.01/polarization`)} />
          <PwmaScalar label="Intensity" valueWidth={170} unit="uJ" value={this.readVar(`${padresDomain}/pfe_f01/diagnostics/iom_pfe_f01.01/IAvg`)} />
          <PwmaScalar label="RMS" valueWidth={170} unit="%" value={this.readVar(`${padresDomain}/pfe_f01/diagnostics/iom_pfe_f01.01/IStd`)} />
        </PwmaContainer>
        <PwmaContainer label="FEL-2 First Stage" visible={this.readVar(`${domain}/f/access_control/safety/undulator_access_state`).data === 'FEL-2'}>
          <PwmaScalar label="Harmonic" unit=" " value={this.readVar(`${domain}/f/misc/beam_status_f/FEL02_FirstStage_harmonic_number`)} />
          <PwmaScalar label="Wavelenght" unit="nm" value={this.readVar(`${domain}/f/misc/beam_status_f/FEL02_FirstStage_wavelength`)} />
          <PwmaScalar label="Polarization" unit=" " value={this.readVar(`${domain}/FEL02/master_s01/master_s01_FEL02.01/polarization`)} />
          <PwmaScalar label="Intensity" unit="uJ" value={this.readVar(`${padresDomain}/pfe_f02/diagnostics/iom_pfe_f02.01/IAvg`)} />
          <PwmaScalar label="RMS" unit="%" value={this.readVar(`${padresDomain}/pfe_f02/diagnostics/iom_pfe_f02.01/IStd`)} />
        </PwmaContainer>
        <PwmaContainer label="FEL-2 Second Stage" visible={this.readVar(`${domain}/f/access_control/safety/undulator_access_state`).data === 'FEL-2'}>
          <PwmaScalar label="Harmonic" unit=" " value={this.readVar(`${domain}/f/misc/beam_status_f/FEL02_SecondStage_harmonic_number`)} />
          <PwmaScalar label="Wavelenght" unit="nm" value={this.readVar(`${domain}/f/misc/beam_status_f/FEL02_SecondStage_wavelength`)} />
          <PwmaScalar label="Polarization" unit=" " value={this.readVar(`${domain}/FEL02/master_s02/master_s02_FEL02.01/polarization`)} />
          <PwmaScalar label="Intensity" unit="uJ" value={this.readVar(`${padresDomain}/pfe_f02/diagnostics/iom_pfe_f02.02/IAvg`)} />
          <PwmaScalar label="RMS" unit="%" value={this.readVar(`${padresDomain}/pfe_f02/diagnostics/iom_pfe_f02.02/IStd`)} />
        </PwmaContainer>
        <PwmaContainer label="Spectrometer">
          <PwmaScalar label="Wavelenght" unit="nm" value={this.readVar(`${padresDomain}/pos/pesp/ccd-pesp_pos.01/SpectrumPeakPos_d`)} />
          <PwmaScalar label="Bandwidth" unit="nm" value={this.readVar(`${padresDomain}/pos/pesp/ccd-pesp_pos.01/SpectrumFWHM_d`)} />
        </PwmaContainer>
      </PwmaMain>
    );
  }
}

FermiStatus.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
