import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';

import redLed from '../img/ledred.png';
import greenLed from '../img/ledgreen.png';
import grayLed from '../img/ledgray.png';

const dragSpec = {
	beginDrag(props, monitor, component) {
		return {id: props.id, index: props.index, parentId: props.parentId};
	}
};

const dropSpec = {
	hover(props, monitor, component) {

		if(monitor.getItem().parentId && monitor.getItem().parentId !== component.decoratedComponentInstance.props.parentId) {
			return;
		}
		
		var thisIndex = props.index;
		var dragIndex = monitor.getItem().index;

		if(dragIndex === undefined || thisIndex === dragIndex) {
			return;
		}
		
		// Determine rectangle on screen
		const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

		// Get vertical middle
		const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

		// Determine mouse position
		const clientOffset = monitor.getClientOffset();

		// Get pixels to the top
		const hoverClientY = clientOffset.y - hoverBoundingRect.top;

		// Only perform the move when the mouse has crossed half of the items height
		// When dragging downwards, only move when the cursor is below 50%
		// When dragging upwards, only move when the cursor is above 50%

		// Dragging downwards
		if (dragIndex < thisIndex && hoverClientY < hoverMiddleY) {
		  return;
		}

		// Dragging upwards
		if (dragIndex > thisIndex && hoverClientY > hoverMiddleY) {
		  return;
		}
		
		props.moveComponent(dragIndex, thisIndex, monitor.getItem().parentId);
		monitor.getItem().index = thisIndex;
	}
};
	  
function dragCollect(connect, monitor) {

	return {
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging()
	}
}

function dropCollect(connect, monitor) {

	return {
		connectDropTarget: connect.dropTarget()
	}
}

class PwmaLedArray extends Component {
	
	constructor(props) {
		
		super(props);

		//this.labelArray = this.props.labels ? this.props.labels.split(';') : [];
		
		this.globalStyle = this.globalStyle.bind(this);
		this.cellColor = this.cellColor.bind(this);
		this.cellValue = this.cellValue.bind(this);
		this.onClick = this.onClick.bind(this);
	}
	
	cellValue(index) {
		
		if(!this.labelArray) {
			return '';
		}
		
		return this.labelArray.length > index && this.labelArray[index] ? this.labelArray[index] : this.labelArray[0];
	}

	globalStyle() {
		
		const flexDirection = this.props.flexDirection !== undefined ? this.props.flexDirection : 'column';
		const justifyContent = this.props.justifyContent !== undefined ? this.props.justifyContent : 'flex-start';
		const alignItems = this.props.alignItems !== undefined ? this.props.alignItems : 'center';

		return {
			flex: 1,
			flexDirection,
			justifyContent,
			alignItems,
			margin: 10,
			padding: 10,
			border: 'thin solid #666'
		};
	}
		
	//TODO capire come usare
	onClick(e) {
		this.props.link(this.props.labels);
	}
	
	cellColor(index) {
		
		if(!this.props.value || !this.props.value.data || this.props.value.data.length <= index)  {
			return grayLed;
		}
		
		let v = this.props.value.data[index];
		if(this.props.trueColor === 'green') {
			v = !v;
		}
		
		return v ? redLed : greenLed;
	}

	render() {

		const flexDirection = this.props.flexDirection !== undefined ? this.props.flexDirection : 'column';
		const justifyContent = this.props.justifyContent !== undefined ? this.props.justifyContent : 'flex-start';
		const alignItems = this.props.alignItems !== undefined ? this.props.alignItems : 'center';
		
		const styles = {
			ledArray: {
				flex: 1,
				flexDirection,
				justifyContent,
				alignItems,
				margin: 10,
				padding: 10,
				border: 'thin solid #666'
			}
		};		
		
		this.labelArray = this.props.labels ? this.props.labels.split(';') : [];

		var valuesCounter = this.props.value != null && this.props.value.data != null ? this.props.value.data.length : 0;
		var labelsCounter = this.labelArray != null && this.labelArray.length > 0 ? this.labelArray.length : 0;
		var leds = [];
		
		if(valuesCounter > 0 || labelsCounter > 0) {
			
			for(var i = 0; i < valuesCounter || i < labelsCounter; i++) {
				leds.push({led: this.cellColor(i), label: this.cellValue(i)});
			}
			
		} else {
			leds.push({led: grayLed, label: ""});
		}
		
		
		/*
		
				{this.props.value && typeof this.props.value.data === 'object' && this.props.value.data.map((v, index) => (
					<div key={index} style={{ flex: this.cellValue(index).length ? 1 : 0, flexDirection: 'row' }}>
						<img src={this.cellColor(index)} />
						<span style={{ marginLeft: 5 }}>{this.cellValue(index)}</span>
					</div>))
				}
		*/
		
		return (
			<div style={styles.ledArray}>
				{leds.map((v, index) => (
					<div key={index} style={{ flex: v.label && v.label.length ? 1 : 0, flexDirection: 'row' }}>
						<img src={v.led} />
						<span style={{ marginLeft: 5 }}>{v.label}</span>
					</div>))
				}				
			</div>
		);
	}
}

export default PwmaLedArray;	
	 
PwmaLedArray.propTypes = {
	value: PropTypes.shape({
		data: PropTypes.oneOfType([
			PropTypes.arrayOf(PropTypes.bool),
			PropTypes.object,
		]),
		alt: PropTypes.string
	}),
	labels: PropTypes.string,
	trueColor: PropTypes.string,
	link: PropTypes.func,
	flexDirection: PropTypes.string,
	justifyContent: PropTypes.string,
	alignItems: PropTypes.string,
};

PwmaLedArray.defaultProps = {
	value: null,
	labels: null,
	trueColor: 'red',
	link: function() {},
	flexDirection: 'column',
	justifyContent: 'flex-start',
	alignItems: 'flex-start',
};

/*
value: {data: [false,false,false,false,false], alt: "my_funny_led_array"},
labels: 'DR_A2_fail: switch porta A2 failure;RF_2_OFF_fail: feedback R.F. 2 off failure;RF_3_OFF_fail: feedback R.F. 3 off failure;FULL_EXT_KEY_A2_fail:ingresso cumulativo testimoni esterni A2 failure;INT_KEY_A2_fail: ingresso cumulativo testimoni interni A2 failure',
*/