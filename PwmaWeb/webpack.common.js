const path = require('path');
module.exports = {
  entry: [
    './Pwma.js',
  ],
  output: {
    path: __dirname,
    filename: 'main-bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          query: {
            presets: ['react']
          }
        }
      }, 
      {
        test: /\.(png|jpg|gif)$/,
        use: {
          loader: 'url-loader?mimetype=image/png'
        }
      }
    ]
  }
};