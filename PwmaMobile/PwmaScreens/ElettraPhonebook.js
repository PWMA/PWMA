import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  CheckBox,
  View,
  ScrollView,
  StyleSheet,
} from 'react-native';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaInput from '../PwmaComponents/PwmaInput';
import PwmaLabel from '../PwmaComponents/PwmaLabel';

/* global fetch */
let id = 0;
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#F0F5FF',
  },
});

class ElettraPhonebook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screens: [],
      value: [],
      buttonBackground: '#2196F3',
    };
    this.ws = '';
    this.callBack = this.callBack.bind(this);
    this.getList = this.getList.bind(this);
    this.CheckBoxChange = this.CheckBoxChange.bind(this);
    this.addContact = this.addContact.bind(this);
  }
  componentDidMount() {
    console.log('ElettraPhonebook::componentDidMount()', `http://pwma.elettra.eu/pwma_screens.php?listScreens`);
    fetch(`http://www.elettra.eu/index.php?option=com_phonebook&view=contacts&Itemid=705&query=exportvcards&blank_mode=1&lang=en`)
     .then((mytext) => {
      // console.log('mytext: ', mytext._bodyInit);
      const myarray = mytext._bodyInit.split('FN:');
      this.setState({ screens: myarray });
    }).catch((error) => {
      console.warn(`There has been a problem with your fetch operation: ${error.message}`);
    });
  }

  CheckBoxChange(id) {
    if (id===-1) {
      const target = this.props.globalParams.navigation.state.params.param.target;
      console.log('CheckBoxChange', target);
      this.setState((prevState, props) => {
        const value = [];
        value[-1] = !prevState.value[-1];
        for (let j in prevState.screens) {
          const rows = prevState.screens[j].split('\r\n')[4];
          const note = !rows ? '' : rows.split('\n')[0];
          const group = note.indexOf('NOTE:')>-1 ? (note.indexOf('Vacuum and Optical Engineering')==-1 ? note.split(':')[1] : 'Vacuum and Optical'): 'No Group';
          if (group === target) {
            // console.log('CheckBoxChange', j);
            value[j] = value[-1];
          }
        }
        return {value};
      });
    }
    else {
      this.setState((prevState, props) => {
        const value = prevState.value;
        value[id] = !value[id];
        return {value};
      });
    }
    this.setState({buttonBackground: '#2196F3'});
  }

  addContact() {
    this.setState({buttonBackground: '#E3C621'});
    var Contacts = require('react-native-contacts');
    for (let i = 1; i < this.state.value.length; i += 1) {
      if (this.state.value[i]) {
        const names = this.state.screens[i].split('\r\n')[1].split(':')[1].split(';');
        const telArray = this.state.screens[i].split('\r\n')[3].split('TEL:')[1]? this.state.screens[i].split('\r\n')[3].split('\n') : [];
        const phoneNumbers = [];
        for (let j = 0; j < telArray.length; j += 2) {
          // custom label not supported, see https://github.com/rt2zz/react-native-contacts/blob/master/android/src/main/java/com/rt2zz/reactnativecontacts/ContactsManager.java ::mapStringToPhoneType() https://github.com/rt2zz/react-native-contacts/pull/142
          // phoneNumbers.push({ label: telArray[j+1].split('Label:')[1], number: telArray[j].split('TEL:')[1] });
          phoneNumbers.push({ label: telArray[j+1].split('Label:')[1]==='Mobile'? 'mobile': 'work', number: telArray[j].split('TEL:')[1] });
        }
        const mailArray = this.state.screens[i].split('\r\n')[2].split('EMAIL')[1]? this.state.screens[i].split('\r\n')[2].split('\n') : [];
        const emailAddresses = [];
        for (let k = 0; k < mailArray.length; k += 2) {
          console.log(k, mailArray[k], mailArray[k].split(':')[1]);
          emailAddresses.push({ label: 'work', email: mailArray[k].split(':')[1] });
        }
        console.log(i, names[0], telArray, mailArray);
        var newPerson = {
          phoneNumbers,
          emailAddresses,
          familyName: names[0],
          givenName: names[1],
        };
        console.log('Contacts.getContactsMatchingString()', names[0]);
        // https://github.com/wumke/react-native-immediate-phone-call
        /* Contacts.getContactsMatchingString(names[0], (err, contacts) => {
          if (err !== 'denied') {
            console.log(contacts)
          }
        });
        */
        Contacts.addContact(newPerson, (err) => {
          if(err) {
             console.log('Contacts.addContact(err)',err);
             this.setState({buttonBackground: '#F32121'});
           }
        });
      }
    }
    this.setState({buttonBackground: '#40C040'});
  }

  callBack(data) {
    console.log(data);
    const { navigate } = this.props.globalParams.navigation;
    const domain = '';
    const param = data;
    navigate('SubPage', { title: data.target.split('.json')[0], component: 'ElettraPhonebook', device: param, domain, param });
  }

  getList(data) {
    console.debug('ElettraPhonebook, props', this.props.globalParams.navigation.state.params);
    const target = this.props.globalParams.navigation.state.params.param ? this.props.globalParams.navigation.state.params.param.target : 0;
    const nodes = [];
    const groups = [];
    let groupString = '';
    if (target === 0) {
      for (let i = 0; i < data.length; i += 1) {
        const rows = data[i].split('\r\n')[4];
        const note = !rows ? '' : rows.split('\n')[0];
        const group = note.indexOf('NOTE:')>-1 ? (note.indexOf('Vacuum and Optical Engineering')==-1 ? note.split(':')[1] : 'Mechanical Vacuum and Optical'): 'No Group';
        groups[group] = groups[group]? groups[group]+1: 1;
      }
      let i = 0;
      for (j in groups) {
        nodes[i++] = (<PwmaInput key={j} text={`${j} - ${groups[j]}`} isButton={true} callback={this.callBack} target={j} onChangeText={this.callBack} buttonWidth={360} />);
      }
    }
    else {
      const nameArray = [];
      for (let i = 0; i < data.length; i += 1) {
        const rows = data[i].split('\r\n')[4];
        const note = !rows ? '' : rows.split('\n')[0];
        const group = note.indexOf('NOTE:')>-1 ? (note.indexOf('Vacuum and Optical Engineering')==-1 ? note.split(':')[1] : 'Mechanical Vacuum and Optical'): 'No Group';
        const spaces = (data[i].split('N:')[0].match(/ /g) || []).length;
        if (group === target && spaces>0 && spaces<=5) {
          const NameSplitted = data[i].split('N:')[0].replace('\r\n','').split(' ');
          if (NameSplitted[1].indexOf('@ELETTRA')>-1 || NameSplitted[1].indexOf('VUOTO')>-1) continue;
          surname = 1;
          for (j=1; j<NameSplitted.length; j++) {if (NameSplitted[j]==NameSplitted[j].toUpperCase()) {surname=j; break;}}
          nameArray.push({surname: NameSplitted.slice(surname).join(' '), fullname: data[i].split('N:')[0].replace('\r\n',''), i: i});
          console.log(data[i].split('N:')[0].replace('\r\n',''),'surname',NameSplitted.slice(surname).join(' '));
          // nodes[i] = (<View key={i} style={{flex: 1, flexDirection: 'row'}}><CheckBox onValueChange={value => this.CheckBoxChange(i)} value={this.state.value[i]} /><Text>{data[i].split('N:')[0]}</Text></View>);
        }
      }
      sortedArray = nameArray.sort(function(a, b) {
          return a.surname.localeCompare(b.surname);
      });
      for (let i = 0; i < sortedArray.length; i += 1) {
        nodes[i] = (<View key={sortedArray[i].i} style={{flex: 1, flexDirection: 'row'}}><CheckBox onValueChange={value => this.CheckBoxChange(sortedArray[i].i)} value={this.state.value[sortedArray[i].i]} /><Text>{sortedArray[i].fullname}</Text></View>);
      }
    }
    return nodes;
  }

  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        <View style={styles.container}>
          <PwmaLabel text="" />
          {this.props.globalParams.navigation.state.params.param && (<View key="add" style={{flexDirection: 'row'}}><CheckBox onValueChange={value => this.CheckBoxChange(-1)} value={this.state.value[-1]} /><PwmaInput buttonBackground={this.state.buttonBackground} text="Add to contacts" buttonWidth={300} isButton={true} callback={this.addContact} /></View>)}
          {this.props.globalParams.navigation.state.params.param ? (<ScrollView>{this.getList(this.state.screens)}</ScrollView>) : this.getList(this.state.screens)}
        </View>
      </PwmaMain>
    );
  }
}

export default ElettraPhonebook;

ElettraPhonebook.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
