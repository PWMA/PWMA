[libri](https://github.com/vhf/free-programming-books/blob/master/javascript-frameworks-resources.md)

[SSE](http://stackoverflow.com/questions/11077857/what-are-long-polling-websockets-server-sent-events-sse-and-comet?rq=1)

[SSE by MDN](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events)

[SSE by W3S](https://www.w3schools.com/html/html5_serversentevents.asp)

[High Performance Browser Networking](https://hpbn.co)

[Vue.js](http://vuejs.org/v2/guide/comparison.html)

[React](https://facebook.github.io/react/docs/hello-world.html)

[React Native](http://facebook.github.io/react-native/)

[Why React Native](https://www.smashingmagazine.com/2016/04/consider-react-native-mobile-app/)

[React designer (by fatiherikli)](https://github.com/fatiherikli/react-designer)

[React designer (by rsamec)](https://github.com/rsamec/react-designer)

[React SSE](http://stackoverflow.com/questions/26899973/react-server-side-rendering-without-polling-for-changes)

[react-eventsource](http://blog.revolunet.com/react-eventsource/#/)

[React devtools](https://facebook.github.io/react/blog/2015/09/02/new-react-developer-tools.html)

[React inline style](http://stackoverflow.com/questions/26882177/react-js-inline-style-best-practices)

[React + D3](https://oli.me.uk//2015/09/09/d3-within-react-the-right-way/)

[React + D3](http://www.adeveloperdiary.com/react-js/integrate-react-and-d3/)

[SVG + Arrow](http://jsfiddle.net/x2kxwyt0/)

[React + CSS in JS](https://vimeo.com/116209150) (https://medium.com/@dbow1234/component-style-b2b8be6931d3) (http://blog.vjeux.com/)

[React Native 4 web](https://www.smashingmagazine.com/2016/08/a-glimpse-into-the-future-with-react-native-for-web/)

[React Native Web](https://github.com/necolas/react-native-web)

[React Native Web starter](https://github.com/grabcode/react-native-web-starter)

[flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) 

[flexbox + react-native] (https://medium.com/the-react-native-log/a-mini-course-on-react-native-flexbox-2832a1ccc6) [tutorial](https://developerlife.com/2017/04/26/flexbox-layouts-and-lists-with-react-native/)

[esLint + airbnb](https://www.npmjs.com/package/eslint-config-airbnb) except rule no-mixed-operators, node_modules/eslint-config-airbnb/node_modules/eslint-config-airbnb-base/rules/style.js line 222

[esLint + airbnb + explanation](https://github.com/airbnb/javascript)

[esLint + visual studio](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

[react + Bootstrap](https://react-bootstrap.github.io/introduction.html)

[react + SVG + debug](https://github.com/react-native-community/react-native-svg/issues/211)

[test drag-and-drop in control room](http://ecsproxy.elettra.trieste.it/docs/canone2/dragNdrop.html)

[react-native components] (https://js.coach/react-native)

[react-native notifications] (https://github.com/zo0r/react-native-push-notification)

[react-native refs width](https://github.com/facebook/react-native/issues/953#issuecomment-95357692)

[Zotero](https://www.zotero.org/download/)

[pqstream](https://github.com/tmc/pqstream/blob/master/README.md)

[FCM + RN](https://invertase.io/react-native-firebase/#/modules/cloud-messaging)

[HTTP/WebSocket/SSE server](https://github.com/Corvusoft/restbed)

[React Native fetch() Network Request Failed](https://code.i-harness.com/en/q/24a3a36)

