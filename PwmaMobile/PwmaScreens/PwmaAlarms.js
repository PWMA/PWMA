import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  pixelRatio,
  Dimensions,
  Linking,
} from 'react-native';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaLabel from '../PwmaComponents/PwmaLabel';
import PwmaContainer from '../PwmaComponents/PwmaContainer';
import PwmaStorage from '../PwmaComponents/PwmaStorage';
import PwmaInput from '../PwmaComponents/PwmaInput';
import PwmaModal from '../PwmaComponents/PwmaModal';

import PwmaCredentials from '../PwmaComponents/PwmaCredentials';
// https://www.npmjs.com/package/base-64
const base64 = require('base-64');

const configurationOptions = {
  debug: true,
};

let myGlobalParams = null;

let id = 0;
function getDate(timestamp) {
  if (timestamp > 0) {
    const m = new Date(timestamp*1000);
    return m.toString()
  }
  return 'never';
}

const subscribedStyle = {
  fontSize: 26,
  color: '#3333ff',
  marginHorizontal: 0,
  fontWeight: 'bold',
  backgroundColor: '#f8fff8',
  marginBottom: 0,
  width: Dimensions.get('window').width-30,
};

const titleStyle = {
  fontSize: 26,
  color: '#3333ff',
  marginHorizontal: 0,
  fontWeight: 'bold',
  backgroundColor: '#f8fff8',
  marginTop: 20,
  marginBottom: 3,
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class PwmaAlarms extends Component {
  constructor(props) {
    super(props);
    // console.log('PwmaAlarms::constructor()');
    this.state = {
      subscribed: {},
      mine: {},
      others: {},
      value: {},
      available: {},
      altVisible: {},
      token: null,
      info: false,
      username: null,
      modalVisible: false,
      modalMessage: '',
    };
    this.localAlarmCallback = null;
    this.token = null;
    this.alarmCallback = this.alarmCallback.bind(this);
    this.saveValue = this.saveValue.bind(this);
    this.deleteAlarm = this.deleteAlarm.bind(this);
    this.editAlarm = this.editAlarm.bind(this);
    this.infoAlarm = this.infoAlarm.bind(this);
    this.addAlarm = this.addAlarm.bind(this);
    this.ackAlarm = this.ackAlarm.bind(this);
    this.plotAlarm = this.plotAlarm.bind(this);
    this.chartAlarm = this.chartAlarm.bind(this);
    this.resetToken = this.resetToken.bind(this);
    this.resetModal = this.resetModal.bind(this);
    this.getToken = this.getToken.bind(this);
    this.getUsername = this.getUsername.bind(this);
    this.saveUsername = this.saveUsername.bind(this);
    this.onSwitchConfirmed = this.onSwitchConfirmed.bind(this);
  }
  componentDidMount() {
    // console.log('PwmaAlarms::componentDidMount()', this.props.globalParams);
    // if (this.props.globalParams) this.props.globalParams.registerAlarmCallback(this.alarmCallback);
    /*myGlobalParams = {
      initWs: this.props.globalParams.initWs,
      registerAlarmCallback: this.localRegisterAlarmCallback,
      navigation: this.props.globalParams.navigation,
    };*/
    PwmaStorage('get', 'PwmaToken', this.getToken);
  }
  componentWillUnmount() {
    if (this.props.globalParams) this.props.globalParams.registerAlarmCallback(null);
  }

  saveUsername(myUsername) {
    console.log('myUsername', myUsername.value);
    PwmaStorage('set', 'PwmaUsername', myUsername.value);
    this.getUsername([ myUsername.value ]);
  }
  clearUsername() {
    PwmaStorage('remove', 'PwmaUsername', '');
    this.setState({ username: null });
  }
  getUsername(myUsername) {
    const username = myUsername === null ? null: myUsername[0];
    if (myUsername === null){
      console.log('user is Anonymous');
      this.setState({ username: null, modalVisible: true });
    }
    else {
      this.username = myUsername[0];
      this.setState({ username: myUsername[0] });
    }
    // console.log(`http://pwma.elettra.eu/pwma_alarms.php?list&username=${username}&token=${this.token}`);
    fetch(`http://pwma.elettra.eu/pwma_alarms.php?list&username=${username}&token=${this.token}`)
    .then(response => response.json())
    .then((alarms) => {
      // console.log(alarms);
      for (let j in alarms.subscribed) {
        alarms.subscribed[j].acknowledged = 'unknown';
        this.setState({ subscribed: alarms.subscribed },()=>{
        // This is the setState callback, so if the fetch doesn't return any value the state remains 'unknown'
          console.log(`fetch(${this.props.globalParams.server}/formula/${j})`);
          fetch(`${this.props.globalParams.server}/formula/${j}`,{headers: {token: `${this.token}`}})
          // .then(resp => resp.json())
          .then(resp => {
            console.log('response', resp);
            if (resp.status < 300) return resp.json();
            return {acknowledged: 'unknown', last_occurrence: 'unknown'};
          })
          .then((respJson) => {
            alarms.subscribed[j].acknowledged = respJson.acknowledged;
            alarms.subscribed[j].last_occurrence = respJson.last_occurrence;
            this.setState({ subscribed: alarms.subscribed });
          })
          .catch(err => {
              console.log(`ERR: `,err);
              fetch(`${this.props.globalParams.server}/formula/${j}`,{headers: {token: `${this.token}`}})
              .then(resp => console.log('retry, resp:', resp));
          });
        });
      }
      this.setState({ mine: alarms.mine, others: alarms.others });
    });
  }

  getToken(myToken) {
    // console.log('getToken', myToken);
    this.token = myToken[0];
    // send token to web server fetch('http://ecsproxy.elettra.trieste.it/docs/lib/save.php?token='+myToken[0]);
    this.setState({ token: myToken[0] });
    // console.log(`${this.props.globalParams.server}/formula/${myToken[0]}`);
    PwmaStorage('get', 'PwmaUsername', this.getUsername);
  }

  // this is only a place holder for PwmaMain component
  localRegisterAlarmCallback(param) {
    console.log('PwmaAlarm::localRegisterAlarmCallback()', param);
    this.localAlarmCallback = param;
  }
  alarmCallback(message) {
    // console.log('PwmaAlarm::alarmCallback()+++++', message.title);
    let title = message? message.title : '???';
    this.setState((prevState) => {
      if (prevState.subscribed && prevState.subscribed[title]){
        prevState.subscribed[title].acknowledged = false;
        prevState.subscribed[title].active = true;
        return {subscribed: prevState.subscribed};
      }
    });
  }
  saveValue(value) {
    // console.log('PwmaAlarms::saveValue(value)', value);
    this.setState({ alarmList: value });
  }
  async ackAlarm(param) {
    console.log('PwmaAlarms::ackAlarm(param)', `${this.props.globalParams.server}/formula/${param.target} {${this.token}}`);
    await sleep(1000);
    fetch(`${this.props.globalParams.server}/formula/${param.target}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        token: this.token,
      },
      body: JSON.stringify({ack: 1}),
    })
    .then(response => {
      console.log('PwmaAlarms::ackAlarm(param), response', response);
      if (response.status < 300) {
        fetch(`${this.props.globalParams.server}/formula/${param.target}`,{headers: {token: this.token}})
        .then(response => response.json())
        // .then(response => {console.log('response', response); return response.json();})
        .then((responseJson) => {
          console.log('ackAlarm(), acknowledged', responseJson.acknowledged);
          // console.log(subscribed);
          this.setState((prevState) => {
            prevState.subscribed[param.target].acknowledged = responseJson.acknowledged;
            return {subscribed: prevState.subscribed};
          });
        })
        .catch(err => {
            console.log(`PwmaAlarms::ackAlarm(), fetch(${this.props.globalParams.server}/formula/${param.target})`,err);
        });
      }
    });
  }
  infoAlarm() {
    this.setState((prevState) => {
      return {info: !prevState.info};
    });
  }
  editAlarm(param) {
    // console.log('PwmaAlarms::editAlarm(param)', param);
    this.setState((prevState) => {
      let altVisible = prevState.altVisible;
      altVisible[param.target] = !altVisible[param.target];
      return {altVisible};
    });
  }
  changeAlarm(param) {
    console.log(`changeAlarm() ${param.target} = ${param.formula.toLowerCase()}`);
    this.setState((prevState) => {
      prevState.mine[param.target].formula = param.formula.toLowerCase();
      return {mine: prevState.mine};
    });
  }
  endEditingAlarm(target) {
    console.log(`endEditingAlarm()`, target, this.state.mine[target].formula.toLowerCase());
    fetch(`http://pwma.elettra.eu/pwma_alarms.php?update&username=${this.state.username}&label=${target}&token=${this.token}&formula=${this.state.mine[target].formula.toLowerCase()}`)
    .then(response => {
      console.log(response);
    });
  }
  changeExpiration(param) {
    console.log(`changeExpiration() ${param.target} = ${param.expiration}`);
    this.setState((prevState) => {
      prevState.mine[param.target].dateexpiration = param.expiration;
      return {mine: prevState.mine};
    });
  }
  endEditingExpiration(target) {
    console.log(`endEditingExpiration()`, target, this.state.mine[target].dateexpiration);
    fetch(`http://pwma.elettra.eu/pwma_alarms.php?update&username=${this.state.username}&label=${target}&token=${this.token}&dateexpiration=${this.state.mine[target].dateexpiration}`)
    .then(response => {
      console.log(response);
    });
  }
  changeDelay(param) {
    console.log(`changeDelay() ${param.target} = ${param.delay}`);
    this.setState((prevState) => {
      prevState.mine[param.target].delay = param.delay;
      return {mine: prevState.mine};
    });
  }
  endEditingDelay(target) {
    console.log(`endEditingDelay()`, target, this.state.mine[target].delay);
    fetch(`http://pwma.elettra.eu/pwma_alarms.php?update&username=${this.state.username}&label=${target}&token=${this.token}&delay=${this.state.mine[target].delay}`)
    .then(response => {
      console.log(response);
    });
  }
  changeNote(param) {
    console.log(`changeNote() ${param.target} = ${param.note.toLowerCase()}`);
    this.setState((prevState) => {
      prevState.mine[param.target].note = param.note.toLowerCase();
      return {mine: prevState.mine};
    });
  }
  endEditingNote(target) {
    console.log(`endEditingNote()`, target, this.state.mine[target].note.toLowerCase());
    fetch(`http://pwma.elettra.eu/pwma_alarms.php?update&username=${this.state.username}&label=${target}&token=${this.token}&note=${this.state.mine[target].note.toLowerCase()}`)
    .then(response => {
      console.log(response);
    });
  }
  plotAlarm(alarm) {
    const ftoken = alarm.target.formula.split(/[ <>=+*|&]/);
    console.log(ftoken);
    const charts = [];
    let j = 0;
    let hh = 8;
    let conf = "";
    for (let i = 0; i < ftoken.length; i++) {
      if (ftoken[i].indexOf(':20000/') !== -1) {
        const ft = ftoken[i].split(':20000/');
        charts[j] = ft[1];
        j += 1;
        if (ftoken[i].indexOf('tango://tom.ecs.elettra.trieste.it:20000/') !== -1) {
          conf = "elettra";
        }
        else if (ftoken[i].indexOf('tango://srv-tango-srf.fcs.elettra.trieste.it:20000/') !== -1) {
          conf = "fermi";
        }
        else if (ftoken[i].indexOf('tango://srv-padres-srf.fcs.elettra.trieste.it:20000/') !== -1) {
          conf = "padres";
        }
        else if (ftoken[i].indexOf('tango://infra-control-01.elettra.trieste.it:20000/') !== -1) {
          conf = "infrastructure";
          hh = 48;
        }
      }
    }
    const src = `http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/redirect_service.php?conf=${conf}&start=last%20${hh}%20hours&s=${charts.join(';')}`;
    Linking.openURL(src).catch(err => console.error('An error occurred', err));
  }
  chartAlarm(alarm) {
    const { navigate } = this.props.globalParams.navigation;
    const ftoken = alarm.target.formula.split(/[ <>=+*|&]/);
    const charts = [];
    for (var i = 0; i < ftoken.length; i++) {
      if (ftoken[i].indexOf('://') !== -1) {
        charts.push(ftoken[i]);
      }
    }
    navigate('SubPage', { title: alarm.target.label, component: 'multiChart', device: charts.join(',') });
  }
  deleteAlarm(param) {
    console.log('PwmaAlarms::deleteAlarm(param)', param, `${this.props.globalParams.server}/formula/${param.target}`, this.state.subscribed[param.target].username);
    fetch(`${this.props.globalParams.server}/formula/${param.target}`, {
      method: 'UNSUBSCRIBE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        token: this.token,
      },
    })
    .then(response => {
      console.log(response);
      if (response.status === 200) {
        this.setState((prevState) => {
          if (prevState.subscribed[param.target].username===this.state.username || prevState.subscribed[param.target].username===this.state.username[0]) {
            prevState.mine[param.target] = prevState.subscribed[param.target];
          }
          else {
            prevState.others[param.target] = prevState.subscribed[param.target];
          }
          delete prevState.subscribed[param.target];
          return {subscribed: prevState.subscribed};
        });
      }
    });
  }
  addAlarm(param) {
    const i = param.target;
    const alarm = this.state.mine[i]!==undefined ? this.state.mine[i] : this.state.others[i];
    console.log('addAlarm', `${this.props.globalParams.server}/formula/${alarm.label}`, alarm.label, this.token, alarm.formula);
    fetch(`${this.props.globalParams.server}/formula/${i}`, {
      method: 'SUBSCRIBE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        token: this.token,
        username: this.state.username,
      },
    })
    .then(response => {
      console.log('addAlarm response',response);
      if (response.status === 200) {
        this.setState((prevState) => {
          prevState.subscribed[i] = {
            label: i,
            formula: alarm.formula.toLowerCase(),
            acknowledged: true,
            active: false,
            username: prevState.mine[i]!==undefined ? this.state.username : '--UNKNOWN--',
            dateexpiration: alarm.dateexpiration!==undefined ? alarm.dateexpiration: null,
            note: alarm.note!==undefined ? alarm.note: null,
            delay: alarm.delay!==undefined ? alarm.delay: 0,
          };
          if (prevState.mine[i]!==undefined) {delete prevState.mine[i];} else {delete prevState.others[i];}
          return {subscribed: prevState.subscribed, mine: prevState.mine, others: prevState.others};
        });
      }
      else {
        this.setState({ modalVisible: true, modalMessage: `ERROR\n${response._bodyText}`});
      }
    });
  }
  resetModal() {
    this.setState({ modalVisible: false, modalMessage: '' });
  }
  resetToken() {
    console.log('PwmaAlarms::resetToken()', this.state.token);
    PwmaStorage('set', 'PwmaPreviousToken', this.state.token);
    PwmaStorage('remove', 'PwmaToken', '');
  }
  aColor(acknowledged) {
    if (acknowledged === 'unknown') return "#fff030";
    if (acknowledged) return null;
    return "#e04030";
  }
  onSwitchConfirmed(username, password) {
    const serverArray = this.props.globalParams.server.split('/');
    serverArray.pop();
    const url = serverArray.join('/');
    console.debug(`${url}/auth`,username);
    var headers = new Headers();
    headers.append("Authorization", "Basic " + base64.encode(`${username}:${password}`));
    headers.append("Accept", 'application/json');
    // headers.append('Content-Type', 'application/json');
    fetch(`${url}/auth`, {
      method: 'POST',
      headers: headers,
    })
    .then(response => {
      console.log('FermiPressures::onSwitchConfirmed, response', response);
      if (response.status==200) {
        this.setState({ modalVisible: false });
        this.saveUsername({value: [username]});
      }
      else {
        this.setState({ errorMessage: 'ERROR: invalid username or password' });
      }
    })
    .catch(err => {
        console.log(`PwmaAlarms::onSwitchConfirmed, fetch(${url}/auth)`,err);
        this.setState({ errorMessage: 'ERROR: credentials cannot be checked' });
    });
  }

  render() {
    const styles = StyleSheet.create({
      button: {
        fontSize: 6*pixelRatio,
        fontWeight: 'bold',
        textAlign: 'center',
        textAlignVertical: 'center',
        height: 8*pixelRatio,
        margin: 10,
        borderWidth: 1,
        width: 100,
        overflow: 'hidden',
        borderRadius: 3,
        backgroundColor: '#2196F3',
        borderColor: '#2196F3',
        color: 'white',
      },
    });
    return (
      <PwmaMain globalParams={{registerAlarmCallback: this.props.globalParams.registerAlarmCallback, alarmNotify: this.alarmCallback}} >
        <PwmaModal visible={this.state.modalVisible} >
          <Text style={{ margin: 10 }}>{this.state.modalMessage}</Text>
          <TouchableOpacity onPress={() => this.resetModal()}>
            <Text style={styles.button}>OK</Text>
          </TouchableOpacity>
        </PwmaModal>
        {this.state.info === true && (
          <View style={{ flexDirection: 'column', margin: 10 }}>
            <PwmaInput text="close" isButton={true} callback={this.infoAlarm} />
            <Text>token:</Text>
            <Text>{this.state.token}</Text>
            <TouchableOpacity onPress={() => this.resetToken()}>
              <Text style={styles.button}>RESET TOKEN</Text>
            </TouchableOpacity>
            {this.state.username !== null && (
              <View style={{ flexDirection: 'column' }}>
                <Text>username:</Text>
                <Text>{this.state.username}</Text>
                <TouchableOpacity onPress={() => this.clearUsername()}>
                  <Text style={styles.button}>RESET USERNAME</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}


        <View style={{ flexDirection: 'row', paddingTop: 5 }}><PwmaLabel text=" Subscribed Alarms" style={subscribedStyle} /><PwmaInput height={36} text="i" isButton={true} buttonWidth={20} callback={this.infoAlarm} /></View>
        {Object.keys(this.state.subscribed).map((i) => (
          <View key={`key${i}`} style={{backgroundColor: this.aColor(this.state.subscribed[i].acknowledged)}}>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <PwmaInput text="detail" isButton={true} buttonWidth={70} target={this.state.subscribed[i].label} callback={this.editAlarm} label={this.state.subscribed[i].label} labelWidth={Dimensions.get('window').width-180-(this.state.subscribed[i].acknowledged ? 0 : 60)} backgroundColor={this.aColor(this.state.subscribed[i].acknowledged)} />
              <PwmaInput text="remove" isButton={true} buttonWidth={80} target={this.state.subscribed[i].label} callback={this.deleteAlarm} backgroundColor={this.aColor(this.state.subscribed[i].acknowledged)} />
              {!this.state.subscribed[i].acknowledged &&
                (<PwmaInput text="ack" isButton={true} buttonWidth={60} target={this.state.subscribed[i].label} callback={this.ackAlarm} backgroundColor={this.aColor(this.state.subscribed[i].acknowledged)} />)
              }
            </View>
            {this.state.altVisible[this.state.subscribed[i].label] && (
              <View key={`d${i}`} style={{padding: 5}}>
                <Text>{"formula:"}</Text>
                <Text style={{ color: this.state.subscribed[i].acknowledged? '#008000': '#FFFFFF' }}>{this.state.subscribed[i].formula}</Text>
                {/* TODO: restore last occurrence*/}
                <Text>{"last occurrence:"}</Text>
                <Text style={{ color: '#0000A0' }}>{getDate(this.state.subscribed[i].last_occurrence)}</Text>
                <Text>{"created by:"}</Text>
                <Text style={{ color: '#0000A0' }}>{this.state.subscribed[i].username}</Text>
                <Text>{"delay:"}</Text>
                <Text style={{ color: '#0000A0' }}>{this.state.subscribed[i].delay? this.state.subscribed[i].delay: '0'}</Text>
                <Text>{"explanation:"}</Text>
                <Text style={{ color: '#0000A0' }}>{this.state.subscribed[i].note}</Text>
                {/* elettra specific part: show historic chart of each variable used by alarms */}
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <PwmaInput text="chart" isButton={true} buttonWidth={60} target={this.state.subscribed[i]} callback={this.chartAlarm} backgroundColor={this.aColor(this.state.subscribed[i].acknowledged)} />
                  <PwmaInput text="eGiga2m" isButton={true} buttonWidth={80} target={this.state.subscribed[i]} callback={this.plotAlarm} backgroundColor={this.aColor(this.state.subscribed[i].acknowledged)} />
                  <Text>{" only if in HDB(pp)"}</Text>
                </View>
                {/* end of elettra specific part */}
              </View>
            )}
          </View>
        ))}

        <PwmaLabel text=" My Alarms" style={titleStyle} />
        <Text style={{marginHorizontal: 8}}>{"You can modify your alarms even if they have been subscribed by other users"}</Text>
        {Object.keys(this.state.mine).map((i) => (
          <View key={`my${i}`}>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <PwmaInput text="detail" buttonWidth={70} target={this.state.mine[i].label} callback={this.editAlarm} label={this.state.mine[i].label} labelWidth={Dimensions.get('window').width-180} backgroundColor={null} />
                  <PwmaInput text="add" buttonWidth={80} isButton={true} labelWidth={Dimensions.get('window').width-100} target={i} callback={this.addAlarm} backgroundColor={null}  />
                </View>
                  {this.state.altVisible[this.state.mine[i].label] && (
                    <View style={{padding: 5}}>
                      <Text>{"formula:"}</Text>
                      <TextInput multiline={true} numberOfLines = {Math.round(this.state.mine[i].formula.length/50.0)+2}
                          style={{borderColor: 'gray', backgroundColor: 'white', borderWidth: 1, margin: 3}}
                          onChangeText={(text) => this.changeAlarm({formula: text, target: this.state.mine[i].label})}
                          onEndEditing={() => this.endEditingAlarm(this.state.mine[i].label)}
                          value={this.state.mine[i].formula}
                      />
                      <Text>{"expiration:"}</Text>
                      <TextInput keyboardType="numeric"
                          style={{borderColor: 'gray', backgroundColor: 'white', borderWidth: 1, margin: 3}}
                          onChangeText={(text) => this.changeExpiration({expiration: text, target: this.state.mine[i].label})}
                          onEndEditing={() => this.endEditingExpiration(this.state.mine[i].label)}
                          value={this.state.mine[i].dateexpiration ? this.state.mine[i].dateexpiration.substring(0,10) : ''}
                      />
                      <Text>{"delay:"}</Text>
                      <TextInput keyboardType="numeric"
                          style={{borderColor: 'gray', backgroundColor: 'white', borderWidth: 1, margin: 3}}
                          onChangeText={(text) => this.changeDelay({delay: text, target: this.state.mine[i].label})}
                          onEndEditing={() => this.endEditingDelay(this.state.mine[i].label)}
                          value={this.state.mine[i].delay? this.state.mine[i].delay: '0'}
                      />
                      <Text>{"explanation:"}</Text>
                      <TextInput multiline={true} numberOfLines = {2+(this.state.mine[i].note? Math.round(this.state.mine[i].note.length/50.0):0)}
                          style={{borderColor: 'gray', backgroundColor: 'white', borderWidth: 1, margin: 3}}
                          onChangeText={(text) => this.changeNote({note: text, target: this.state.mine[i].label})}
                          onEndEditing={() => this.endEditingNote(this.state.mine[i].label)}
                          value={this.state.mine[i].note}
                      />
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <PwmaInput text="chart" isButton={true} buttonWidth={60} target={this.state.mine[i]} callback={this.chartAlarm} />
                        <PwmaInput text="eGiga2m" isButton={true} buttonWidth={80} target={this.state.mine[i]} callback={this.plotAlarm} />
                        <Text>{" only if in HDB(pp)"}</Text>
                      </View>
                    </View>
                  )}
          </View>
        ))}

        <PwmaLabel text=" Other Alarms" style={titleStyle} />
        <Text style={{marginHorizontal: 8}}>{"Alarms created by other users can be subscribed, but the owner can change his own alarms without any notice"}</Text>
        {Object.keys(this.state.others).map((i) => (
          <View key={`other${i}`}>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <PwmaInput text="detail" buttonWidth={70} target={this.state.others[i].label} callback={this.editAlarm} label={this.state.others[i].label} labelWidth={Dimensions.get('window').width-180} backgroundColor={null} />
                  <PwmaInput text="add" buttonWidth={80} isButton={true} labelWidth={Dimensions.get('window').width-100} target={i} callback={this.addAlarm} backgroundColor={null}  />
                </View>
                  {this.state.altVisible[this.state.others[i].label] && (
                    <View style={{padding: 5}}>
                      <Text>{"created by:"}</Text>
                      <Text style={{ color: '#0000A0' }}>{this.state.others[i].username}</Text>
                      <Text>{"formula:"}</Text>
                      <Text style={{ color: '#008000' }}>{this.state.others[i].formula}</Text>
                      <Text>{"delay:"}</Text>
                      <Text style={{ color: '#0000A0' }}>{this.state.others[i].delay? this.state.others[i].delay: '0'}</Text>
                      <Text>{"explanation:"}</Text>
                      <Text style={{ color: '#0000A0' }}>{this.state.others[i].note}</Text>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <PwmaInput text="chart" isButton={true} buttonWidth={60} target={this.state.others[i]} callback={this.chartAlarm} />
                        <PwmaInput text="eGiga2m" isButton={true} buttonWidth={80} target={this.state.others[i]} callback={this.plotAlarm} />
                        <Text>{" only if in HDB(pp)"}</Text>
                      </View>
                    </View>
                  )}
          </View>
        ))}
        <Text>{"\n\n\n\n"}</Text>
      </PwmaMain>
    );
  }
}

export default PwmaAlarms;

PwmaAlarms.propTypes = {
  globalParams: PropTypes.shape({
    initWs: PropTypes.func.isRequired,
    registerAlarmCallback: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
    page: PropTypes.string,
    server: PropTypes.string.isRequired,
  }).isRequired,
};
