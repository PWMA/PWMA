import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaContainer from '../PwmaComponents/PwmaContainer';

const domain = 'tango://srv-tango-srf.fcs.elettra.trieste.it:20000';
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}
function truncateString(str, len) {
  if (str.length > len) return `${str.substring(0, len - 3)}...`;
  return str;
}

export default class FermiPressures extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {},
      error: {},
      informationSystem: 'N.A.',
      dateTime: '',
    };
    this.ws = '';
    this.readVar = this.readVar.bind(this);
    this.wsListener = this.wsListener.bind(this);
  }
  componentDidMount() {
    console.log('FermiPressures::componentDidMount()');
    this.ws = this.props.globalParams.initWs();
    if (this.ws) {
      if (!this.ws.readyState) {
        this.ws.onopen = () => {
          this.subscribeVar();
        };
      } else {
        this.subscribeVar();
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      Object.keys(this.state.value).map((obj) => {
        console.log(`unsubscribe ${obj}`);
        fetch(`${this.props.globalParams.server}/cs/${obj}`, {
          method: 'UNSUBSCRIBE',
        })
        .then((response) => {
          if (response.status>=300) {
            console.log('response', response.status, response._bodyText, obj);
          }
        })
        .catch((err) => {
          console.log('.catch((err)', err, obj);
        });
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  wsListener(event) {
    console.log('Message from server', event.data);
    const value = this.state.value;
    const error = this.state.error;
    const eventData = JSON.parse(event.data);
    value[eventData.event] = eventData.data.error !== '' ? 'Error' : eventData.data.value;
    error[eventData.event] = eventData.data.error !== '' ? eventData.data.error : '';
    this.setState({ value, error, dateTime: getDate() });
  }
  subscribeVar() {
    console.log('subscribeVar()');
    const value = this.state.value;
    const error = this.state.error;
    Object.keys(this.state.value).map((obj) => {
      console.log(`subscribe: ${obj}`);
      fetch(`${this.props.globalParams.server}/cs/${obj}`, {method: 'SUBSCRIBE'})
      // .then(responseJson => responseJson.json())
      .then(responseJson => {console.log(`${this.props.globalParams.server}/cs/${obj}`, responseJson); return responseJson.json();})
      .then((response) => {
        if (response.status>=300) {
          value[obj] = 'Error.';
          error[obj] = response._bodyText;
          this.setState({ value, error, });
          console.log('response', response.status, response._bodyText, obj);
        }
        else {
          if (response.data.value) {
            console.log(obj, response.data.value);
            value[obj] = response.data.value;
            // this.setState({ value });
          }
        }
      })
      .catch((err) => {
        console.log('.catch((err)', err, obj);
        const value = this.state.value;
        const error = this.state.error;
        value[obj] = 'N.A.';
        error[obj] = err;
        /*
        this.setState({ value, error, });
        */
      });
      // console.error(obj);
      return false;
    });
  }
  readVar(src) {
    const lvar = src.toLowerCase();
    if (!this.state.value[lvar]) {
      this.state.value[lvar] = 'N.A.';
    }
    return { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${getDate()}` };
  }
  render() {
    return (
      <PwmaMain>
        <PwmaScalar unit="mBar" label="sip150_bc01.01" value={this.readVar(`${domain}/bc01/vacuum/sip150_bc01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip150_bc01.02" value={this.readVar(`${domain}/bc01/vacuum/sip150_bc01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_bc01.01" value={this.readVar(`${domain}/bc01/vacuum/sip300_bc01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_bc01.02" value={this.readVar(`${domain}/bc01/vacuum/sip300_bc01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_bc01.01" value={this.readVar(`${domain}/bc01/vacuum/sip55_bc01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_bc01.02" value={this.readVar(`${domain}/bc01/vacuum/sip55_bc01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_bc01.03" value={this.readVar(`${domain}/bc01/vacuum/sip55_bc01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_bc01.04" value={this.readVar(`${domain}/bc01/vacuum/sip55_bc01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_bc01.01" value={this.readVar(`${domain}/bc01/vacuum/sip75_bc01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_bc01.02" value={this.readVar(`${domain}/bc01/vacuum/sip75_bc01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_bc01.03" value={this.readVar(`${domain}/bc01/vacuum/sip75_bc01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_bc01.04" value={this.readVar(`${domain}/bc01/vacuum/sip75_bc01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_bc01.05" value={this.readVar(`${domain}/bc01/vacuum/sip75_bc01.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_bc01.06" value={this.readVar(`${domain}/bc01/vacuum/sip75_bc01.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_bc02.01" value={this.readVar(`${domain}/bc02/vacuum/sip55_bc02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_bc02.02" value={this.readVar(`${domain}/bc02/vacuum/sip55_bc02.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip150_dbd.01" value={this.readVar(`${domain}/dbd/vacuum/sip150_dbd.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_dbd.01" value={this.readVar(`${domain}/dbd/vacuum/sip75_dbd.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_fel02.02" value={this.readVar(`${domain}/fel02/vacuum/sip55_fel02.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_fel02.03" value={this.readVar(`${domain}/fel02/vacuum/sip55_fel02.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_fel02.04" value={this.readVar(`${domain}/fel02/vacuum/sip55_fel02.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_fel02.06" value={this.readVar(`${domain}/fel02/vacuum/sip55_fel02.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip150_inj.01" value={this.readVar(`${domain}/inj/vacuum/sip150_inj.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_inj.01" value={this.readVar(`${domain}/inj/vacuum/sip55_inj.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_inj.01" value={this.readVar(`${domain}/inj/vacuum/sip75_inj.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_inj.02" value={this.readVar(`${domain}/inj/vacuum/sip75_inj.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.01" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.02" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.03" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.04" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.05" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.06" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.07" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.08" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel01.09" value={this.readVar(`${domain}/iufel01/vacuum/sip55_iufel01.09/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.01" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.02" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.03" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.04" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.05" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.06" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.07" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.08" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_iufel02.09" value={this.readVar(`${domain}/iufel02/vacuum/sip55_iufel02.09/pressure`)} />
        <PwmaScalar unit="mBar" label="sip02_kg04.01" value={this.readVar(`${domain}/kg04/vacuum/sip02_kg04.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip08_kg04.01" value={this.readVar(`${domain}/kg04/vacuum/sip08_kg04.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip08_kg04.02" value={this.readVar(`${domain}/kg04/vacuum/sip08_kg04.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip08_kg04.03" value={this.readVar(`${domain}/kg04/vacuum/sip08_kg04.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.01" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.02" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.03" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.04" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.05" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.06" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.07" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l01.08" value={this.readVar(`${domain}/l01/vacuum/sip300_l01.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.01" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.02" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.03" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.04" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.05" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.06" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.07" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l01.08" value={this.readVar(`${domain}/l01/vacuum/sip75_l01.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.01" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.02" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.03" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.04" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.05" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.06" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.01" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.02" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.03" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.04" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.05" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.06" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l03.01" value={this.readVar(`${domain}/l03/vacuum/sip300_l03.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l03.02" value={this.readVar(`${domain}/l03/vacuum/sip300_l03.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l03.03" value={this.readVar(`${domain}/l03/vacuum/sip300_l03.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l03.04" value={this.readVar(`${domain}/l03/vacuum/sip300_l03.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.01" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.02" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.03" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.04" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.05" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.06" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.07" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l03.08" value={this.readVar(`${domain}/l03/vacuum/sip75_l03.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.01" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.02" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.03" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.04" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.05" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.06" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.07" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.08" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.09" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.09/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.11" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.11/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l04.12" value={this.readVar(`${domain}/l04/vacuum/sip300_l04.12/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.01" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.02" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.03" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.04" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.05" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.06" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.07" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.08" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.09" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.09/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.10" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.10/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.11" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.11/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.12" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.12/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.13" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.13/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.14" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.14/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.15" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.15/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.16" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.16/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.17" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.17/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.18" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.18/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.19" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.19/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.20" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.20/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.23" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.23/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.24" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.24/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.25" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.25/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.26" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.26/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.27" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.27/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.28" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.28/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_lh.01" value={this.readVar(`${domain}/lh/vacuum/sip55_lh.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_lh.02" value={this.readVar(`${domain}/lh/vacuum/sip55_lh.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_lh.03" value={this.readVar(`${domain}/lh/vacuum/sip55_lh.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.01" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.02" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.03" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.04" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.05" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.06" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.07" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel01.08" value={this.readVar(`${domain}/mbd_fel01/vacuum/sip55_mbd_fel01.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel02.01" value={this.readVar(`${domain}/mbd_fel02/vacuum/sip55_mbd_fel02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel02.02" value={this.readVar(`${domain}/mbd_fel02/vacuum/sip55_mbd_fel02.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel02.03" value={this.readVar(`${domain}/mbd_fel02/vacuum/sip55_mbd_fel02.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel02.04" value={this.readVar(`${domain}/mbd_fel02/vacuum/sip55_mbd_fel02.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_mbd_fel02.05" value={this.readVar(`${domain}/mbd_fel02/vacuum/sip55_mbd_fel02.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_mbd.01/" value={this.readVar(`${domain}/mbd/vacuum/sip75_mbd.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_scl.01" value={this.readVar(`${domain}/scl/vacuum/sip300_scl.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_scl.01" value={this.readVar(`${domain}/scl/vacuum/sip55_scl.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_scl.02" value={this.readVar(`${domain}/scl/vacuum/sip55_scl.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_scl.03" value={this.readVar(`${domain}/scl/vacuum/sip55_scl.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip150_sfel01.01" value={this.readVar(`${domain}/sfel01/vacuum/sip150_sfel01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_sfel01.01" value={this.readVar(`${domain}/sfel01/vacuum/sip300_sfel01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel01.01" value={this.readVar(`${domain}/sfel01/vacuum/sip55_sfel01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel01.02" value={this.readVar(`${domain}/sfel01/vacuum/sip55_sfel01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel01.03" value={this.readVar(`${domain}/sfel01/vacuum/sip55_sfel01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel01.04" value={this.readVar(`${domain}/sfel01/vacuum/sip55_sfel01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel01.05" value={this.readVar(`${domain}/sfel01/vacuum/sip55_sfel01.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel01.06" value={this.readVar(`${domain}/sfel01/vacuum/sip55_sfel01.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip150_sfel02.01" value={this.readVar(`${domain}/sfel02/vacuum/sip150_sfel02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_sfel02.01" value={this.readVar(`${domain}/sfel02/vacuum/sip300_sfel02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel02.01" value={this.readVar(`${domain}/sfel02/vacuum/sip55_sfel02.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel02.02" value={this.readVar(`${domain}/sfel02/vacuum/sip55_sfel02.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_sfel02.04" value={this.readVar(`${domain}/sfel02/vacuum/sip55_sfel02.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_spbc01.01" value={this.readVar(`${domain}/spbc01/vacuum/sip55_spbc01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_spbc01.02" value={this.readVar(`${domain}/spbc01/vacuum/sip55_spbc01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_spinj.01" value={this.readVar(`${domain}/spinj/vacuum/sip75_spinj.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_splh.01" value={this.readVar(`${domain}/splh/vacuum/sip75_splh.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_tls.01" value={this.readVar(`${domain}/tls/vacuum/sip300_tls.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_tls.02" value={this.readVar(`${domain}/tls/vacuum/sip300_tls.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_tls.01" value={this.readVar(`${domain}/tls/vacuum/sip55_tls.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_tls.01" value={this.readVar(`${domain}/tls/vacuum/sip75_tls.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.01" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.02" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.03" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.04" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.05" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.06" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.06/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.07" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_xbl01.08" value={this.readVar(`${domain}/xbl01/vacuum/sip20_xbl01.08/pressure`)} />
        <PwmaScalar unit="mBar" label="sip40_xbl01.01" value={this.readVar(`${domain}/xbl01/vacuum/sip40_xbl01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip40_xbl01.02" value={this.readVar(`${domain}/xbl01/vacuum/sip40_xbl01.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip40_xbl01.03" value={this.readVar(`${domain}/xbl01/vacuum/sip40_xbl01.03/pressure`)} />
        <PwmaScalar unit="mBar" label="psnip_kg01.01" value={this.readVar(`${domain}/kg01/vacuum/psnip_kg01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="tib_eos_sfel01.01" value={this.readVar(`${domain}/sfel01/climate/tib_eos_sfel01.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_l00.01" value={this.readVar(`${domain}/l00/vacuum/sip20_l00.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_l00.02" value={this.readVar(`${domain}/l00/vacuum/sip20_l00.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_l00.03" value={this.readVar(`${domain}/l00/vacuum/sip20_l00.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip20_l00.04" value={this.readVar(`${domain}/l00/vacuum/sip20_l00.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_l00.01" value={this.readVar(`${domain}/l00/vacuum/sip55_l00.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_l00.02" value={this.readVar(`${domain}/l00/vacuum/sip55_l00.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_l00.03" value={this.readVar(`${domain}/l00/vacuum/sip55_l00.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip55_l00.04" value={this.readVar(`${domain}/l00/vacuum/sip55_l00.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l00.01" value={this.readVar(`${domain}/l00/vacuum/sip75_l00.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l00.02" value={this.readVar(`${domain}/l00/vacuum/sip75_l00.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l00.03" value={this.readVar(`${domain}/l00/vacuum/sip75_l00.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l00.04" value={this.readVar(`${domain}/l00/vacuum/sip75_l00.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.07" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip300_l02.08" value={this.readVar(`${domain}/l02/vacuum/sip300_l02.08/pressure`)} />
        <PwmaScalar unit="mBar" label="psnip_kg09.01" value={this.readVar(`${domain}/kg09/vacuum/psnip_kg09.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_pil.01" value={this.readVar(`${domain}/pil/vacuum/vgpi_pil.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_lhl.01" value={this.readVar(`${domain}/lhl/vacuum/vgpi_lhl.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.07" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l02.08" value={this.readVar(`${domain}/l02/vacuum/sip75_l02.08/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_fel01_sl.01" value={this.readVar(`${domain}/sl/vacuum/vgpi_fel01_sl.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_fel02_sl.01" value={this.readVar(`${domain}/sl/vacuum/vgpi_fel02_sl.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.21" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.21/pressure`)} />
        <PwmaScalar unit="mBar" label="sip75_l04.22" value={this.readVar(`${domain}/l04/vacuum/sip75_l04.22/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_uh.01" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_uh.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_uh.02" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_uh.02/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_uh.03" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_uh.03/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_uh.04" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_uh.04/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_ehf.01" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_ehf.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_ehf.02" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_ehf.02/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_ehf.03" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_ehf.03/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_ehf.04" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_ehf.04/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_sh.01" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_sh.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.01" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.01/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.02" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.02/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.03" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.03/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.04" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.05" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.05/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.06" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.06/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_ctf.01" value={this.readVar(`${domain}/ctf/vacuum/vgpi_ctf.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_ctf.02" value={this.readVar(`${domain}/ctf/vacuum/vgpi_ctf.02/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpe_ctf.03" value={this.readVar(`${domain}/ctf/vacuum/vgpe_ctf.03/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_ctf.03" value={this.readVar(`${domain}/ctf/vacuum/vgpi_ctf.03/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpe_ctf.04" value={this.readVar(`${domain}/ctf/vacuum/vgpe_ctf.04/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.07" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.07/pressure`)} />
        <PwmaScalar unit="mBar" label="sip_ctf.08" value={this.readVar(`${domain}/ctf/vacuum/sip_ctf.08/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpr_ctf.01" value={this.readVar(`${domain}/ctf/vacuum/vgpr_ctf.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgba_ctf.01" value={this.readVar(`${domain}/ctf/vacuum/vgba_ctf.01/pressure`)} />
        <PwmaScalar unit="mBar" label="vgpi_slu_utdr.01" value={this.readVar(`${domain}/slu/vacuum/vgpi_slu_utdr.01/pressure`)} />
      </PwmaMain>
    );
  }
}
/*
<PwmaScalar unit="mBar" label="tib_ltdr.01" value={this.readVar(`${domain}/ltdr/climate/tib_ltdr.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_lh.01" value={this.readVar(`${domain}/lh/vacuum/vgpe_lh.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_lh.02" value={this.readVar(`${domain}/lh/vacuum/vgpe_lh.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_lh.01" value={this.readVar(`${domain}/lh/vacuum/vgpi_lh.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_inj.02" value={this.readVar(`${domain}/inj/vacuum/vgpe_inj.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_inj.03" value={this.readVar(`${domain}/inj/vacuum/vgpe_inj.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_inj.04" value={this.readVar(`${domain}/inj/vacuum/vgpe_inj.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_inj.02" value={this.readVar(`${domain}/inj/vacuum/vgpi_inj.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_inj.03" value={this.readVar(`${domain}/inj/vacuum/vgpi_inj.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_inj.04" value={this.readVar(`${domain}/inj/vacuum/vgpi_inj.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l00.01" value={this.readVar(`${domain}/l00/vacuum/vgpe_l00.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l00.02" value={this.readVar(`${domain}/l00/vacuum/vgpe_l00.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc01.01" value={this.readVar(`${domain}/bc01/vacuum/vgpe_bc01.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc01.02" value={this.readVar(`${domain}/bc01/vacuum/vgpe_bc01.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc01.03" value={this.readVar(`${domain}/bc01/vacuum/vgpe_bc01.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc01.04" value={this.readVar(`${domain}/bc01/vacuum/vgpe_bc01.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc01.05" value={this.readVar(`${domain}/bc01/vacuum/vgpe_bc01.05/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc01.06" value={this.readVar(`${domain}/bc01/vacuum/vgpe_bc01.06/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc01.07" value={this.readVar(`${domain}/bc01/vacuum/vgpe_bc01.07/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_bc01.01" value={this.readVar(`${domain}/bc01/vacuum/vgpi_bc01.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_bc01.02" value={this.readVar(`${domain}/bc01/vacuum/vgpi_bc01.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_bc01.03" value={this.readVar(`${domain}/bc01/vacuum/vgpi_bc01.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_bc01.04" value={this.readVar(`${domain}/bc01/vacuum/vgpi_bc01.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_bc02.01" value={this.readVar(`${domain}/bc02/vacuum/vgpe_bc02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_bc02.01" value={this.readVar(`${domain}/bc02/vacuum/vgpi_bc02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_ctf.01" value={this.readVar(`${domain}/ctf/vacuum/vgpe_ctf.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_ctf.02" value={this.readVar(`${domain}/ctf/vacuum/vgpe_ctf.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_dbd.01" value={this.readVar(`${domain}/dbd/vacuum/vgpe_dbd.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_dbd.02" value={this.readVar(`${domain}/dbd/vacuum/vgpe_dbd.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_dbd.01" value={this.readVar(`${domain}/dbd/vacuum/vgpi_dbd.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_fel02.01" value={this.readVar(`${domain}/fel02/vacuum/vgpe_fel02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_fel02.02" value={this.readVar(`${domain}/fel02/vacuum/vgpe_fel02.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_iufel02.01" value={this.readVar(`${domain}/iufel02/vacuum/vgpe_iufel02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_iufel02.02" value={this.readVar(`${domain}/iufel02/vacuum/vgpe_iufel02.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_iufel02.03" value={this.readVar(`${domain}/iufel02/vacuum/vgpe_iufel02.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_iufel02.03" value={this.readVar(`${domain}/iufel02/vacuum/vgpi_iufel02.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l01.01" value={this.readVar(`${domain}/l01/vacuum/vgpe_l01.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l01.02" value={this.readVar(`${domain}/l01/vacuum/vgpe_l01.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l01.03" value={this.readVar(`${domain}/l01/vacuum/vgpe_l01.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l01.04" value={this.readVar(`${domain}/l01/vacuum/vgpe_l01.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_l01.02" value={this.readVar(`${domain}/l01/vacuum/vgpi_l01.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_l01.03" value={this.readVar(`${domain}/l01/vacuum/vgpi_l01.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_l01.04" value={this.readVar(`${domain}/l01/vacuum/vgpi_l01.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l02.01" value={this.readVar(`${domain}/l02/vacuum/vgpe_l02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l02.02" value={this.readVar(`${domain}/l02/vacuum/vgpe_l02.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l02.03" value={this.readVar(`${domain}/l02/vacuum/vgpe_l02.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l02.04" value={this.readVar(`${domain}/l02/vacuum/vgpe_l02.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_l02.01" value={this.readVar(`${domain}/l02/vacuum/vgpi_l02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_l02.02" value={this.readVar(`${domain}/l02/vacuum/vgpi_l02.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l03.01" value={this.readVar(`${domain}/l03/vacuum/vgpe_l03.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l03.02" value={this.readVar(`${domain}/l03/vacuum/vgpe_l03.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_l03.01" value={this.readVar(`${domain}/l03/vacuum/vgpi_l03.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_l03.02" value={this.readVar(`${domain}/l03/vacuum/vgpi_l03.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l04.04" value={this.readVar(`${domain}/l04/vacuum/vgpe_l04.04/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_l04.05" value={this.readVar(`${domain}/l04/vacuum/vgpe_l04.05/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_mbd_fel02.01" value={this.readVar(`${domain}/mbd_fel02/vacuum/vgpe_mbd_fel02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_mbd_fel02.02" value={this.readVar(`${domain}/mbd_fel02/vacuum/vgpe_mbd_fel02.02/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_mbd_fel02.03" value={this.readVar(`${domain}/mbd_fel02/vacuum/vgpe_mbd_fel02.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_sfel02.01" value={this.readVar(`${domain}/sfel02/vacuum/vgpe_sfel02.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_spbc01.01" value={this.readVar(`${domain}/spbc01/vacuum/vgpe_spbc01.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_spinj.01" value={this.readVar(`${domain}/spinj/vacuum/vgpe_spinj.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_splh.01" value={this.readVar(`${domain}/splh/vacuum/vgpe_splh.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_tls.01" value={this.readVar(`${domain}/tls/vacuum/vgpe_tls.01/pressure`)} />
<PwmaScalar unit="mBar" label="vgpe_tls.03" value={this.readVar(`${domain}/tls/vacuum/vgpe_tls.03/pressure`)} />
<PwmaScalar unit="mBar" label="vgpi_tls.01" value={this.readVar(`${domain}/tls/vacuum/vgpi_tls.01/pressure`)} />
*/
FermiPressures.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
