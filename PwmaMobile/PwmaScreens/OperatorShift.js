import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  ScrollView,
  Text,
} from 'react-native';

// adapted from https://stackoverflow.com/questions/1284314/easter-date-in-javascript
function dayAfterEaster(Y) {
  const C = Math.floor(Y/100);
  const N = Y - 19*Math.floor(Y/19);
  const K = Math.floor((C - 17)/25);
  let I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
  I = I - 30*Math.floor((I/30));
  I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
  let J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
  J = J - 7*Math.floor(J/7);
  const L = I - J;
  let M = 3 + Math.floor((L + 40)/44);
  let D = L + 28 - 31*Math.floor(M/4);
  if (D === 31) { D = 0; M += 1; }
  return padout(D+1) + '/' + padout(M);
}

function padout(number) { return (number < 10) ? '0' + number : number; }

export default class OperatorShift extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operatorShift: [],
      year: '',
    };
    this.evalRowStyle = this.evalRowStyle.bind(this);
  }
  componentDidMount() {
    fetch('http://ecsproxy.elettra.eu/docs/fermi/app/turni_operatori.php?csv')
    .then((response) => {
      const data = response._bodyText.trim().split('\n');
      const year = data.shift().substring(6,10);
      const operatorShift = [];
      // data.map((val,key) => {/* don't skip empty days if (val.search(";<br>;<br>;<br>") === -1) */ operatorShift[key] = val.split(';')});
      data.map((val,key) => {if (val.search(";<br>;<br>;<br>") === -1) operatorShift[key] = val.split(';')});
      this.setState({ operatorShift, year});
    });
  }
  evalRowStyle(day, i) {
    if (i===0) return this.styles.header;
    const ddmm = day.split('/');
    const d = new Date(this.state.year, ddmm[1]-1, ddmm[0], 12, 0, 0, 0);
    console.log(`Date(${this.state.year}, ${ddmm[1]-1}, ${ddmm[0]}, 12, 0, 0, 0);`, d.getDay());
    if (d.getDay() === 0) return this.styles.rowHoliday;
    if (day === dayAfterEaster(this.state.year) || day === '01/01' || day === '06/01' || day === '25/04' || day === '01/05' || day === '02/06' || day === '15/08' || day === '01/11' || day === '03/11' || day === '08/12' || day === '25/12' || day === '26/12') return this.styles.rowHoliday;
    if (d.getDay() === 6) return this.styles.rowSaturday;
    if (i%2===1) return this.styles.rowOdd;
    return this.styles.rowEven;
  }
  styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'stretch',
      backgroundColor: '#F0F5FF',
    },
    table: {
      flexDirection: 'column',
      flexWrap: 'nowrap',
    },
    header: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
      backgroundColor: '#ffe080',
    },
    rowEven: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
      backgroundColor: '#ffffe0',
    },
    rowOdd: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
      backgroundColor: '#e0ffe0',
    },
    rowSaturday: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
      backgroundColor: '#ffe0e0',
    },
    rowHoliday: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
      backgroundColor: '#ffc0c0',
    },
  });

  render() {
    return (
      <View style={this.styles.container}>
        <ScrollView>
          <Text key={'year'} style={{marginRight: 5, }}>
            Year: {this.state.year}
          </Text>
          <View style={this.styles.table}>
            {this.state.operatorShift.map((row, i) =>
              (
                <View key={i} style={this.evalRowStyle(row[0], i)}>
                  <Text style={{marginRight: 5, flex: 0.5,}}>{row[0]}</Text>
                  <Text style={{marginRight: 5, flex: 1,}}>{row[1].replace("<br>", "\n")}</Text>
                  <Text style={{marginRight: 5, flex: 1,}}>{row[2].replace("<br>", "\n")}</Text>
                  <Text style={{marginRight: 5, flex: 1,}}>{row[3].replace("<br>", "\n")}</Text>
                </View>
              )
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}
