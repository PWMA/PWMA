import React, { Component } from 'react';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaContainer from '../PwmaComponents/PwmaContainer';
import PwmaChart from '../PwmaComponents/PwmaChart';

const domain = 'tango://tom.ecs.elettra.trieste.it:20000';

function getWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}
function truncateString(str, len) {
  if (str.length > len) return `${str.substring(0, len - 3)}...`;
  return str;
}

export default class ElettraStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {},
      error: {},
      informationSystem: 'N.A.',
      dateTime: '',
      graphData: [],
      graphReady: false,
    };
    this.ws = '';
    this.readVar = this.readVar.bind(this);
    this.wsListener = this.wsListener.bind(this);
  }
  readVar(src) {
    const lvar = src.toLowerCase();
    if (!this.state.value[lvar]) {
      this.state.value[lvar] = 'N.A.';
    }
    const date = this.getDate();
    return {data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${date}`};
  }
  getDate() {
    var m = new Date();
    return m.getUTCDate() +"/"+ (m.getUTCMonth()+1) +"/"+ m.getUTCFullYear() + " " + m.getUTCHours() + ":" + m.getUTCMinutes() + ":" + m.getUTCSeconds();;
  }
  componentDidMount() {
    console.log('componentDidMount()');
    console.log('ElettraStatus, this.props', this.props);

    var _that = this;
    console.log('fetch("https://pwma.elettra.eu/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%208%20hours&ts=00046&no_pretimer&no_posttimer")');
    fetch("https://pwma.elettra.eu/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%208%20hours&ts=00046&no_pretimer&no_posttimer")
    .then((response) => response.json())
    .then((responseJson) => {
      console.log('responseJson', responseJson);
      if(responseJson.ts[0].data === undefined) {
        return;
      }
      _that.setState({graphData: responseJson.ts[0].data});
    });

    fetch('https://pwma.elettra.eu/misc/information_system.php')
    .then((response) => {
      // console.log('response: ', response);
      return response.text();
    }).then((mytext) => {
      // console.log(mytext);
      this.setState({ informationSystem: mytext.trim(), dateTime: this.getDate(), });
    }).catch(function(error) {
      console.log('There has been a problem with your fetch operation: ' + error.message);
    });
    //.then((response) => {this.setState({ informationSystem: response._bodyText.trim(), dateTime: this.getDate(), });});
    // fetch('http://pwma/misc/information_system.php').then((response) => console.debug('FETCH: ',response));
    const that = this;
    // this.ws = this.props.initWs();
    this.ws = this.props.globalParams.initWs();
    console.log(this.ws===null);
    if (this.ws) {
      if (!this.ws.readyState) {
        console.log('readyState', this.ws.readyState);
        this.ws.onopen = () => {
          this.subscribeVar();
        };
      } else {
        this.subscribeVar();
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      Object.keys(this.state.value).map((obj) => {
        console.log(`unsubscribe ${obj}`);
        fetch(`${this.props.globalParams.server}/cs/${obj}`, {
          method: 'UNSUBSCRIBE',
        })
        .then((response) => {
          if (response.status>=300) {
            console.log('response', response.status, response._bodyText, obj);
          }
        })
        .catch((err) => {
          console.log('.catch((err)', err, obj);
        });
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  wsListener(event) {
    console.log('Message from server', event.data);
    const value = this.state.value;
    const error = this.state.error;
    const eventData = JSON.parse(event.data);
    value[eventData.event] = eventData.data.error !== "" ? 'Error' : eventData.data.value;
    error[eventData.event] = eventData.data.error !== "" ? eventData.data.error : '';
    this.setState({ value, error, dateTime: getDate() });
  }
  subscribeVar() {
    console.log('subscribeVar()');
    Object.keys(this.state.value).map((obj) => {
      console.log(`subscribe: ${this.props.globalParams.server}/${obj}`);
      fetch(`${this.props.globalParams.server}/cs/${obj}`, {
        method: 'SUBSCRIBE',
      }).then((response) => {
        if (response.status>=300) {
          const value = this.state.value;
          const error = this.state.error;
          value[obj] = 'Error.';
          error[obj] = response._bodyText;
          this.setState({ value, error, });
          console.log('response', response.status, response._bodyText, obj);
        }
      })
      .catch((err) => {
        console.log('.catch((err)', err, obj);
        const value = this.state.value;
        const error = this.state.error;
        value[obj] = 'N.A.';
        error[obj] = err;
        /*
        this.setState({ value, error, });
        */
      });
      // console.error(obj);
      return false;
    });
  }
  readVar(src) {
    const lvar = src.toLowerCase();
    if (!this.state.value[lvar]) {
      this.state.value[lvar] = 'N.A.';
    }
    if (!isNaN(this.state.value[lvar])) this.state.value[lvar] = Number(this.state.value[lvar]).toPrecision(4);
    return { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${getDate()}` };
  }

  render() {
    return (
      <PwmaMain>
        <PwmaChart width={getWidth()} color="#48F"  title="SR Current (last 8 hours)" value={this.state.graphData}/>
        <PwmaContainer label="STATUS">
          <PwmaScalar label="Current" unit="[mA]" value={this.readVar(`${domain}/sr/DIAGNOSTICS/DCCT_S4/Current`)} />
          <PwmaScalar label="Energy" unit="[GeV]" value={this.readVar(`${domain}/sr/DIAGNOSTICS/DCCT_S4/Energy`)} />
          <PwmaScalar label="Lifetime" unit="[h]" value={this.readVar(`${domain}/sr/DIAGNOSTICS/DCCT_S4/LifetimeHours`)} />
          <PwmaScalar
            label="Info" valueWidth={308}
            value={{
              data: truncateString(this.state.informationSystem, 32),
              alt: this.state.informationSystem,
            }}
          />
        </PwmaContainer>
        <PwmaContainer label="CONTROL ROOM">
          <PwmaScalar valueWidth={240} label="Machine Info" value={this.readVar(`${domain}/elettra/status/userstatus/machinestatus`)} />
          <PwmaScalar valueWidth={240} label="Operation Mode" value={this.readVar(`${domain}/elettra/status/userstatus/operationmode`)} />
          <PwmaScalar valueWidth={240} label="Bunch Mode" value={this.readVar(`${domain}/elettra/status/userstatus/bunchmode`)} />
          <PwmaScalar valueWidth={240} label="Control Room" value={this.readVar(`${domain}/elettra/status/userstatus/controlroom`)} />
        </PwmaContainer>
        <PwmaContainer label="LAST USER DEDICATED INJECTION">
          <PwmaScalar valueWidth={150} label="date" value={this.readVar(`${domain}/elettra/status/userstatus/lastudinjon`)} />
          <PwmaScalar valueWidth={150} label="time" value={this.readVar(`${domain}/elettra/status/userstatus/lastudinjtime`)} />
          <PwmaScalar valueWidth={150} label="Next injection on" value={{data: "N.A.", alt: "Not available\nsrc: elettra/status/userstatus/nextudinjtime"}} />
        </PwmaContainer>
      </PwmaMain>
    );
  }
}