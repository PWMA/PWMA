import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaContainer from '../PwmaComponents/PwmaContainer';
import PwmaChart from '../PwmaComponents/PwmaChart';
import PwmaLedArray from '../PwmaComponents/PwmaLedArray';
import PwmaStorage from '../PwmaComponents/PwmaStorage';
// import md5 from '../md5';

/* global fetch */
let id = 0;
let username = '';
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}
const xAccessor = d => new Date(d[0]);
const yAccessor = d => d[1];

class PwmaLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panel: [],
      value: {},
      error: {},
      dateTime: null,
      graphData: [],
      notSubscribed: [],
      intervalId: false,
    };
    this.ws = '';
    this.addVar = this.addVar.bind(this);
    this.setVar = this.setVar.bind(this);
    this.registerVar = this.registerVar.bind(this);
    this.wsListener = this.wsListener.bind(this);
    this.getUsername = this.getUsername.bind(this);
    this.initPeriodicGet = this.initPeriodicGet.bind(this);
    this.periodicGet = this.periodicGet.bind(this);
  }
  componentDidMount() {
    PwmaStorage('get', 'PwmaUsername', this.getUsername);
    let url = `http://pwma.elettra.eu/pwma_screens.php?${this.props.globalParams.page}`;
     // check if this.props.globalParams.page is a number (screenId)
    if (!isNaN(parseFloat(this.props.globalParams.page)) && isFinite(this.props.globalParams.page)) {
      url = `http://pwma.elettra.eu/pwma_screens.php?openScreenId=${this.props.globalParams.page}&username=${username}`;
    }
    // console.log('PwmaLoader::componentDidMount()', url);
    fetch(url)
    .then(response => response.json())
    .then((mytext) => {
      // console.log('mytext: ', mytext);
      this.setState({ panel: mytext.cmpts });
    }).catch((error) => {
      console.debug(`There has been a problem with your fetch operation: ${error.message}`);
    });
    this.ws = this.props.globalParams.initWs();
    if (this.ws) {
      this.ws.addEventListener('message', this.wsListener);
    }
    setTimeout(this.initPeriodicGet, 2000);
  }
  componentWillUnmount() {
    if (this.ws) {
      const that = this;
      Object.keys(this.state.value).map((obj) => {
        // console.log(`unsubscribe ${obj}`);
        // that.ws.send(`{"data":{"type":"unsubscribe"},"event":"${obj}"}`);
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
    if (this.state.intervalId) clearInterval(this.state.intervalId);
  }
  initPeriodicGet() {
    if (this.state.notSubscribed.length) {
      var intervalId = setInterval(this.periodicGet, 1000);
      this.setState({intervalId: intervalId});
    }
  }
  periodicGet() {
    for (i=0; i<this.state.notSubscribed.length; i++) {
			this.fetchVar(this.state.notSubscribed[i]);
		}
  }
  fetchVar(obj) {
    console.log(`fetchVar(https://pwma.elettra.eu:10443/v1/cs/${obj})`);
    fetch(`https://pwma.elettra.eu:10443/v1/cs/${obj}`)
    .then((response) => {
      if (response.status<300 && response._bodyText !== undefined) {
        const eventData = JSON.parse(response._bodyText);
        if (eventData.error === undefined || eventData.value === undefined) return false;
        this.setState((prevState) => {
          prevState.value[obj] = {data: eventData.value, alt: `${obj}\nerr: ${eventData.error}\nlastUpdate: `+getDate()};
          prevState.error[obj] = eventData.error !== "" ? eventData.error : '';
          return {value: prevState.value, error: prevState.error, dateTime: getDate()};
        });
      }
    })
    .catch((err) => {
      console.log('.catch((err)', err, obj);
    });
  }

  getList(d) {
    const nodes = [];
    const data = d;
    console.log('typeof data', typeof data);
    if (!!data) for (let i = 0; i < data.length; i += 1) {
      if (data[i].id === undefined) {
        data[i].id = id; id += 1;
        // console.log(`--id: ${id} src: ${data[i].src}`);
      }
      nodes.push(this.getNode(data[i]));
    }
    return nodes;
  }
  setVar(src) {
    const lvar = src.toLowerCase();
    // console.log('this.state.value[lvar]', this.state.value[lvar]);
    return (typeof this.state.value[lvar] === "undefined")? {data: 'n.a.', alt: ''}: this.state.value[lvar];
  }
  getUsername(myUsername) {
    if (myUsername === null){
      console.log('user is Anonymous');
    }
    else {
      username = myUsername[0];
    }
  }
  getNode(node) {
    // console.log('PwmaLoader::getNode:', node);
    if (node.component === 'PwmaScalar') {
      if (!node.src || node.src.length<1) {
        // console.log('getNode(), PwmaScalar, src: null')
        return (
          <PwmaScalar
            key={node.id} label={node.label}
            value={{ data: '', alt:'' }} unit={node.unit ? node.unit : ''}
          />
        );
      }
      const lvar = this.addVar(node.src, 'N.A.');
      return (
        <PwmaScalar
          key={node.id} label={node.label}
          unit={node.unit ? node.unit : ''}
          // value={JSON({data: this.state.value[lvar], alt: ''}}) unit={node.unit ? node.unit : ''}
          value={this.setVar(lvar)}
        />
      );
    }
    if (node.component === 'PwmaLedArray') {
      if (!node.src || node.src.length<1) {
        // console.log('getNode(), PwmaLedArray, src: null')
        return (
          <PwmaLedArray
            key={node.id} flags={node.flags} label={node.label}
            value={{ data: [], alt:'' }}
          />
        );
      }
      const lvar = this.addVar(node.src, []);
      // console.log('PwmaLoader::PwmaLedArray, lvar:', lvar);
      return (
        <PwmaLedArray
          key={node.id} flags={node.flags} label={node.label}
          value={this.state.value[lvar]}
        />
      );
    }
    if (node.component === 'PwmaChart') {
      // console.log('node.component === PwmaChart', node.id, typeof this.state.graphData[node.id]);
      if (typeof node.src !== 'string') return null;
      if (typeof this.state.graphData[node.id] !== 'undefined') {
        return (
          <PwmaChart key={node.id} data={this.state.graphData[node.id]} xAccessor={xAccessor} yAccessor={yAccessor} title={node.label} xScaleType="time" />
        );
      }
      this.state.graphData[node.id] = [[1500000000000,0],[1500001000000,0]];
      if (node.src.indexOf('tango://tom.ecs.elettra.trieste.it:20000/') !== -1) {
        offset = 41;
        src = `http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%2012%20hours&s=${node.src.substring(offset)}&no_pretimer&no_posttimer&decimation_samples=100`;
      }
      else if (node.src.indexOf('tango://srv-tango-srf.fcs.elettra.trieste.it:20000/') !== -1) {
        offset = 51;
        src = `http://fcsproxy.elettra.eu/docs/egiga2m/lib/service/hdbpp_plot_service.php?conf=fermi&start=last%2012%20hours&s=${node.src.substring(offset)}&no_pretimer&no_posttimer&decimation_samples=100`;
      }
      else if (node.src.indexOf('tango://srv-padres-srf.fcs.elettra.trieste.it:20000/') !== -1) {
        offset = 52;
        src = `http://padrescsproxy.elettra.eu/docs/egiga2m/lib/service/hdbpp_plot_service.php?conf=fermi&start=last%2012%20hours&s=${node.src.substring(offset)}&no_pretimer&no_posttimer&decimation_samples=100`;
      }
      else if (node.src.indexOf('tango://infra-control-01.elettra.trieste.it:20000/') !== -1) {
        offset = 50;
        src = `http://fcsproxy.elettra.eu/docs/egiga2m/lib/service/hdbpp_plot_service.php?conf=infrastructure&start=last%2048%20hours&s=${node.src.substring(offset)}&no_pretimer&no_posttimer&decimation_samples=100`;
      }
      fetch(src)
      .then(response => {
        // console.log('node.id, response', node.id, response);
        return response.json();
      })
      .then((responseJson) => {
        // console.log('response graphProps', node, responseJson.ts[0].data);
        this.setState(prevState => {
          const graphData = prevState.graphData;
          graphData[node.id] = responseJson.ts[0].data;
          // console.log('node.id', node.id);
          return {graphData}
        });
      });
      return (
        <PwmaChart key={node.id} data={this.state.graphData[node.id]} xAccessor={xAccessor} yAccessor={yAccessor} title={node.label} xScaleType="time" />
      );
    }
    if (node.component === 'PwmaMain') {
      return (
        <PwmaMain>
          {this.getList(node.children)}
        </PwmaMain>
      );
    }
    if (node.component === 'PwmaContainer') {
      return (
        <PwmaContainer key={node.id} label={node.label}>
          {this.getList(node.children)}
        </PwmaContainer>
      );
    }
    // console.log('PwmaLoader::getNode, done');
    return null;
  }

  registerVar(src) {
    // console.log(`fetch(${this.props.globalParams.server}/cs/${src} {method: 'SUBSCRIBE'})`);
    fetch(`${this.props.globalParams.server}/cs/${src}`, {
      method: 'SUBSCRIBE',
    }).then((response) => {
      // console.log('response', response, src);
      if (response.status>=300) {
        this.setState((prevState) => {
          prevState.value[src] = {data: 'Error', alt: `src: ${src}\nerror: ${response._bodyText}\nlastUpdate: ${getDate()}`};
          prevState.error[src] = response._bodyText;
          return {value: prevState.value, error: prevState.error, dateTime: getDate()};
        });
        // console.log('resp', response.status, response._bodyText, src);
      }
      else if (response._bodyText !== undefined) {
        const eventData = JSON.parse(response._bodyText);
        if (eventData.data === undefined || eventData.data.error === undefined || eventData.data.value === undefined) return false;
        this.setState((prevState) => {
          prevState.value[src] = {data: (eventData.data.error !== "" ? 'Error': eventData.data.value), alt: `src: ${src}\nerror: ${eventData.data.error}\nlastUpdate: ${getDate()}`};
          prevState.error[src] = eventData.data.error !== "" ? eventData.data.error : '';
          return {value: prevState.value, error: prevState.error, dateTime: getDate()};
        });
      }
    })
    .catch((err) => {
      this.setState((prevState) => {
        prevState.value[src] = {data: 'N.A.', alt: `src: ${src}\nerror: ${err}\nlastUpdate: ${getDate()}`};
        prevState.error[src] = err;
        return {value: prevState.value, error: prevState.error, dateTime: getDate()};
      });
    });
  }
  addVar(src, dataDefault) {
    const lvar = src.toLowerCase();
    // console.log('addVar()', lvar);
    if (!this.state.value[lvar]) {
      // console.log('addVar() !this.state.value', lvar);
      this.state.value[lvar] = { data: dataDefault, alt: `src: ${src}\nerror:\nlastUpdate:` };
      if (this.ws) {
        if (this.ws.readyState === 0) {
          this.ws.onopen = () => {
            // console.log('+++ addVar() !this.ws.readyState', this.ws.readyState);
            // this.ws.send(`{"data":{"type":"subscribe"},"event":"${lvar}"}`);
            this.registerVar(lvar);
          };
        } else if (this.ws.readyState === 1) { // 1 == OPEN, https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/readyState
          // console.log('+++--- addVar() this.ws.readyState', this.ws.readyState);
          // this.ws.send(`{"data":{"type":"subscribe"},"event":"${lvar}"}`);
          this.registerVar(lvar);
        } else {
          console.log('--- addVar() this.ws.readyState', this.ws.readyState);
          if (typeof this.state.notSubscribed[lvar] == 'undefined') {
            this.setState((prevState) => {
              return {notSubscribed: [...prevState.notSubscribed, lvar]};
            });
          }
        }
      }
      else {
        console.log('addVar(), cannot add ', src);
      }
    }
    return lvar;
  }
  wsListener(event) {
    const eventData = JSON.parse(event.data);
    if (!eventData || !eventData.event || typeof this.state.value[eventData.event] === "undefined") return;
    // console.log('eventData', eventData.event, eventData.data.value);
    const value = this.state.value;
    value[eventData.event] = {
      data: eventData.data.error!=="" ? 'Error' : eventData.data.value,
      alt: `src: ${eventData.event}\nerror: ${eventData.data.error}\nlastUpdate: ${getDate()}`,
    };
    this.setState({ value });
  }

  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        {this.getList(this.state.panel)}
      </PwmaMain>
    );
  }
}

export default PwmaLoader;

PwmaLoader.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
