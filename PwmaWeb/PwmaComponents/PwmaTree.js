// derived from https://github.com/rnative/react-native-treeview
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Link,
} from 'react-router-dom';

let id = 0;

const styles = {
  tree: {
    padding: 10,
  },
  rootnode: {
    paddingBottom: 10,
  },
  node: {
    paddingTop: 10,
  },
  item: {
    flexDirection: 'row',
  },
  children: {
    paddingLeft: 20,
  },
  icon: {
    paddingRight: 10,
    color: '#333',
    alignSelf: 'center',
  },
  roottext: {
    fontSize: 18,
  },
  text: {
    borderRadius: 3,
    backgroundColor: '#2196F3',
    borderColor: '#2196F3',
    color: 'white',
    textDecoration: 'none',
  },
};
let cid = 0;
function initTree(data, collapsed) {
  const c = collapsed;
  for (let i = 0; i < data.length; i += 1) {
    c[cid] = true;
    cid += 1;
    if (data[i].data) initTree(data[i].data, collapsed);
  }
}

class PwmaTree extends Component {

  constructor(props) {
    super(props);
    const collapsed = [];
    if (this.props.defaultPosition === 'close') {
      initTree(this.props.data, collapsed);
    }
    this.state = {
      collapsed,
    };
    this.toggleState = this.toggleState.bind(this);
  }

  getNodeView(type, node) {
    if (this.props.hideRoot && type === 'root') {
      return (
        <div />
      );
    }
    const { collapsed } = this.state;
    const hasChildren = !!node.data;
    const nIcon = collapsed[node.id] ? '../img/blue_right.png' : '../img/blue_down.png';
    const icon = node.icon ? node.icon : nIcon;
    const alt = collapsed[node.id] ? '>' : 'V';
    return (
      <div style={styles.item}>
        {
          !hasChildren && !node.icon ? null : <img alt={alt} src={icon} onClick={this.toggleState.bind(null, node)} />
        }
        {
          !hasChildren ?
            <Link to={`/${node.component}`} style={styles.text}>{node.title} </Link> :
            <span style={styles.none} onClick={this.toggleState.bind(null, node)}> {node.title} </span>
        }
      </div>
    );
  }

  getNode(type, node) {
    const { collapsed } = this.state;
    const { renderItem, appendItem } = this.props;
    console.debug(`getNode, type: ${type}, node.id: ${node.id}, collapsed[node.id]: ${collapsed[node.id]}`);
    // <span onClick={this.toggleState(node.id)}>
    return (
      <div key={node.id} style={styles.node} >
        <div style={styles.item} >
          <span>
            {renderItem ?
              renderItem(type, node) :
              this.getNodeView(type, node)}
          </span>
          {appendItem && appendItem(type, node)}
        </div>
        <div style={styles.children}>
          {
            (this.props.hideRoot === false || type === 'children') && collapsed[node.id] ? null : this.getTree('children', node.data || [])
          }
        </div>
      </div>
    );
  }

  getTree(type, d) {
    const nodes = [];
    const data = d;
    for (let i = 0; i < data.length; i += 1) {
      if (data[i].id === undefined) { data[i].id = id; id += 1; }
      nodes.push(this.getNode(type, data[i]));
    }
    return nodes;
  }

  toggleState(node) {console.log('toggleState', node);
    console.debug(`toggleState, nodeId: ${node.id}`);
    const { collapsed } = this.state;
    const { onItemClicked } = this.props;

    collapsed[node.id] = !collapsed[node.id];
    this.setState({
      collapsed,
    });
    if (onItemClicked) {
      onItemClicked(node);
    }
  }

  render() {
    return (
      <div style={styles.tree}>
        {this.getTree('root', this.props.data)}
      </div>
    );
  }
}

PwmaTree.propTypes = {
  data: PropTypes.array.isRequired,
  onItemClicked: PropTypes.func,
  hideRoot: PropTypes.bool,
  defaultPosition: PropTypes.oneOf(['close', 'open']),
  renderItem: PropTypes.func,
  appendItem: PropTypes.func,
};

PwmaTree.defaultProps = {
  onItemClicked: null,
  hideRoot: true,
  defaultPosition: 'close',
  renderItem: null,
  appendItem: null,
};

export default PwmaTree;
