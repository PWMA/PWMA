import _ from 'lodash';

import React, { Component } from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';

import { ItemTypes } from './designer/Constants';
import { DragSource, DropTarget } from 'react-dnd';

import moment from 'moment';

const parseDate = d3.time.format("%m-%d-%Y").parse;

const dragSpec = {
	beginDrag(props, monitor, component) {
		return {id: props.id, index: props.index, parentId: props.parentId};
	}
};

const dropSpec = {
	hover(props, monitor, component) {

		if(monitor.getItem().parentId && monitor.getItem().parentId !== component.decoratedComponentInstance.props.parentId) {
			return;
		}
		
		var thisIndex = props.index;
		var dragIndex = monitor.getItem().index;

		if(dragIndex === undefined || thisIndex === dragIndex) {
			return;
		}
		
		// Determine rectangle on screen
		const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

		// Get vertical middle
		const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

		// Determine mouse position
		const clientOffset = monitor.getClientOffset();

		// Get pixels to the top
		const hoverClientY = clientOffset.y - hoverBoundingRect.top;

		// Only perform the move when the mouse has crossed half of the items height
		// When dragging downwards, only move when the cursor is below 50%
		// When dragging upwards, only move when the cursor is above 50%

		// Dragging downwards
		if (dragIndex < thisIndex && hoverClientY < hoverMiddleY) {
		  return;
		}

		// Dragging upwards
		if (dragIndex > thisIndex && hoverClientY > hoverMiddleY) {
		  return;
		}
		
		props.moveComponent(dragIndex, thisIndex, monitor.getItem().parentId);
		monitor.getItem().index = thisIndex;
	}
};
	  
function dragCollect(connect, monitor) {

	return {
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging()
	}
}

function dropCollect(connect, monitor) {

	return {
		connectDropTarget: connect.dropTarget()
	}
}

class PwmaChart extends Component {

	constructor(props) {
		super(props);
		this.state = { status: true, gists: null, shapeD: null };
	}
  
	render() {
	
		var graphData = null;
				/*
		this.props.src && this.props.src.length && fetch(this.props.src)
			.then((response) => response.json())
			.then((responseJson) => {
				
				if(responseJson.ts[0].data === undefined) {
					return;
				}
				
				const localdata = responseJson.ts[0].data.map(function(x) {return {date: x[0], value: x[1]};});				

				const padding = 5;
				const width = $("#charts-container").width() - padding;
				const height = 180;
    
				var x = d3.scaleTime().domain(d3.extent(localdata, function(d) { return d.date; })).rangeRound([0, width]);;
				var y = d3.scaleLinear().domain([0, d3.max(localdata, function(d) { return d.value; })]).rangeRound([height, 0]);;
				
				if(localdata !== null) {
					
					const chartShaper = d3.area()
						.defined(function(d) { return d; })
						.x(function(d) { return x(d.date); })
						.y1(function(d) { return y(d.value); })
						.y0(y(0))
						.curve(d3.curveNatural);
					
					graphData = chartShaper(localdata);   console.log(graphData);
				}		
	  */
	
		const styles = {
			graph: {
				height: 100
			}
		};	
		 
		var dataArea = [];
		
		var margin = {
			top: 20, right: 30, bottom: 20, left: 50
		};

		if(this.props.value) {
			dataArea = this.props.value.map(function(x) {return {date: new Date(x[0]), count: x[1]};});
		}
		
		var $chartsContainer = $("#charts-container");
		if($chartsContainer.length === 0) {
			$chartsContainer = $("div[data-reactroot]");
		}
		
		var width = $chartsContainer.width() - 5;
		
		//alt interpolator curveNatural
		//alt tickFormat %d/%m
		return (
			<div>
				<label>{this.props.title}</label>
				<AreaChart id="multi-area-chart" data={dataArea} width={width} height="300" margin={margin} interpolations="cardinal">
					<yGrid orient="left" className="y-grid" ticks={5}/>
					<xAxis orient="bottom" className="axis" tickFormat="%H:%M" ticks={24}/>
					<yAxis orient="left" className="axis" ticks={10}/>
					<area className="area" fill="#090" value="B"/>
				</AreaChart>
			</div>
		);
	}
}


class DnDPwmaChart extends Component {

	constructor(props) {
		super(props);
		this.onClick = this.onClick.bind(this);
	}
	
	onClick(e) {
		this.props.clickListener(this.props.id);
		e.stopPropagation();
	}
  
	render() {
	
		const {isDragging, connectDragSource, connectDropTarget} = this.props;
		const opacity = isDragging ? 0 : 1;

		const styles = {
			graph: {
				opacity
			},
			droppable: {
				border: "thin solid #777",
				padding: "4px",
				margin: "4px",
				cursor: "move",
				cursor: "grab",
				cursor: "-moz-grab",
				cursor: "-webkit-grab"
			}
		};

		if(this.props.droppable) {
			
			return connectDragSource(
				<div style={styles.droppable}>Chart</div>
			);
		}		
		
		return connectDragSource(connectDropTarget(
			<div style={styles.graph} onClick={this.onClick}>
				<PwmaChart title={this.props.title} value={this.props.value} />
			</div>
		));
	}
}


class Grid extends Component {
 
    componentDidUpdate() { this.renderGrid(); }
    componentDidMount() { this.renderGrid(); }
	
    renderGrid() {
 
        this.grid = d3.svg.axis()
            .scale(this.props.scale)
            .orient(this.props.orient)
            .ticks(this.props.ticks)
            .tickSize(-this.props.len, 0, 0)
            .tickFormat("");
 
        var node = ReactDOM.findDOMNode(this);
        d3.select(node).call(this.grid); 
    }
	
    render() {
        var translate = "translate(0,"+(this.props.h)+")";
        return (
            <g className={this.props.className} transform={this.props.gridType == 'x' ? translate : ""}>
            </g>
        );
    }
}

class Axis extends Component {
 
    componentDidUpdate() { this.renderAxis(); }
    componentDidMount() { this.renderAxis(); }
    
	renderAxis() {
 
       var _self=this;
       this.axis = d3.svg.axis()
            .scale(this.props.scale)
            .orient(this.props.orient)
            .ticks(this.props.ticks);
 
        if(this.props.tickFormat != null && this.props.axisType === 'x')
            this.axis.tickFormat(d3.time.format(this.props.tickFormat));
 
        var node = ReactDOM.findDOMNode(this);
        d3.select(node).call(this.axis);
    }
	
    render() {
 
        var translate = "translate(0,"+(this.props.h)+")";
 
        return (
            <g className={this.props.className} transform={this.props.axisType == 'x' ? translate : ""} >
            </g>
        );
    }
}

const Y_MAX_BUFFER = 10;

class AreaChart extends Component {

	constructor(props) {
		super(props);
		this.state = { width: this.props.width };
	}
	
	createChart(_self) {
	 
		this.w = this.state.width - (this.props.margin.left + this.props.margin.right);
		this.h = this.props.height - (this.props.margin.top + this.props.margin.bottom);
	 
		this.xScale = d3.time.scale()
			.domain(d3.extent(this.props.data, function (d) {
				return d.date;
			}))
			.rangeRound([0, this.w]);
	 
		this.yScale = d3.scale.linear()
			.domain([0, d3.max(this.props.data,function(d){
				return d.count + Y_MAX_BUFFER;
			})])
			.range([this.h, 0]);
	 
		this.area = d3.svg.area()
			.x(function (d) {
				return this.xScale(d.date);
			})
			.y0(this.h)
			.y1(function (d) {
				return this.yScale(d.count);
			}).interpolate(this.props.interpolations);
	 
		this.transform = 'translate(' + this.props.margin.left + ',' + this.props.margin.top + ')';
	}

	createElements(element, i) {

		var object;
		var _self = this;
	 
		switch(element.type){
	 
			case 'xGrid':
				object = <Grid h={this.h} len={this.h} scale={this.xScale} gridType="x" key={i} {...this.props} {...element.props} />;
				break;
	 
			case 'yGrid':
				object = <Grid h={this.h} len={this.w} scale={this.yScale} gridType="y" key={i} {...this.props} {...element.props} />;
				break;
	 
			case 'xAxis':
				object = <Axis h={this.h} scale={this.xScale} axisType="x" key={i} {...this.props} {...element.props} />;
				break;
	 
			case 'yAxis':
				object = <Axis h={this.h} scale={this.yScale} axisType="y" key={i} {...this.props} {...element.props} />;
				break;
	 
			case 'area':
				object = <path className={element.props.className} d={this.area(this.props.data)} key={i} fill={element.props.fill} />;
				break;
		}
		
		return object;
	}
	
	render() {
		
		this.createChart(this);
		
		var _self = this;
		var elements = this.props.children.map(function(element, i) {
			return _self.createElements(element, i);
		});
	 
		return (
			<div>
				<svg id={this.props.chartId} width={this.state.width} height={this.props.height}>
					<g transform={this.transform}>
						{elements}
					</g>
				</svg>
			</div>
		);
	}	
}


const pwmaPropDescription = 
	[{id: "title", name: "title", defaultValue: null},
	 {id: "src", name: "source", defaultValue: null}];
	 
var currentSequenceValue = 0;
const sequence = {
	nextVal() {
		return currentSequenceValue++;
	}
};

const DsPwmaChart = DragSource(ItemTypes.PWMA_CHART, dragSpec, dragCollect)(DnDPwmaChart);

const DsDtPwmaChart = _.flow(DragSource(ItemTypes.PWMA_CHART, dragSpec, dragCollect),
								DropTarget(ItemTypes.PWMA_CHART, dropSpec, dropCollect))(DnDPwmaChart); 

export default PwmaChart;
export {DsPwmaChart, DsDtPwmaChart, pwmaPropDescription, sequence};


PwmaChart.propTypes = {
	src: PropTypes.string,
	color: PropTypes.string,
	max: PropTypes.number,
	min: PropTypes.number
};

PwmaChart.defaultProps = {
	color: 'green',
	max: null,
	min: null
};
