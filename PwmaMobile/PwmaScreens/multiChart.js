import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaChart from '../PwmaComponents/PwmaChart';

const domain = 'tango://srv-padres-srf.fcs.elettra.trieste.it:20000';
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}
function truncateString(str, len) {
  if (str.length > len) return `${str.substring(0, len - 3)}...`;
  return str;
}

export default class multiChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {},
      error: {},
      informationSystem: 'N.A.',
      dateTime: '',
      graphProps: [{data: [ [ 1519780833000, 0 ] ], xAccessor: d => new Date(d[0]), yAccessor: d => d[1], title: 'Chart'}],
    };
    this.ws = '';
    this.readVar = this.readVar.bind(this);
  }
  componentDidMount() {
    console.log('multiChar::componentDidMount()', this.props.globalParams.navigation.state.params.device);
    const vars = this.props.globalParams.navigation.state.params.device.split(',');
    console.log(vars);
    let src = '';
    let offset = 0
    const graphProps = [];
    for (let i = 0; i < vars.length; i++) {
      if (vars[i].indexOf('tango://tom.ecs.elettra.trieste.it:20000/') !== -1) {
        offset = 41;
        src = `http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%2012%20hours&s=${vars[i].substring(offset)}&no_pretimer&no_posttimer&decimation_samples=300`;
      }
      else if (vars[i].indexOf('tango://srv-tango-srf.fcs.elettra.trieste.it:20000/') !== -1) {
        offset = 51;
        src = `http://fcsproxy.elettra.eu/docs/egiga2m/lib/service/hdbpp_plot_service.php?conf=fermi&start=last%2012%20hours&s=${vars[i].substring(offset)}&no_pretimer&no_posttimer&decimation_samples=300`;
      }
      else if (vars[i].indexOf('tango://srv-padres-srf.fcs.elettra.trieste.it:20000/') !== -1) {
        offset = 52;
        src = `http://padrescsproxy.elettra.eu/docs/egiga2m/lib/service/hdbpp_plot_service.php?conf=fermi&start=last%2012%20hours&s=${vars[i].substring(offset)}&no_pretimer&no_posttimer&decimation_samples=300`;
      }
      console.log('fetch graphProps', src, vars[i].substring(offset));
      fetch(src)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('response graphProps', i, vars);
        graphProps[i] = {data: responseJson.ts[0].data, xAccessor: d => new Date(d[0]), yAccessor: d => d[1], title: vars[i].substring(offset)};
        this.setState({graphProps});
      });
    }
    this.ws = this.props.globalParams.initWs();
  }
  componentWillUnmount() {
    if (this.ws) {
      const that = this;
      Object.keys(this.state.value).map((obj) => {
        console.log(`unsubscribe ${obj}`);
        that.ws.send(`{"data":{"type":"unsubscribe"},"event":"${obj}"}`);
        return false;
      });
    }
  }

  readVar(src) {
    const lvar = src.toLowerCase();
    if (!this.state.value[lvar]) {
      this.state.value[lvar] = 'N.A.';
      if (this.ws) this.ws.send(`{"data":{"type":"subscribe"},"event":"${lvar}"}`);
    }
    return { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${getDate()}` };
  }
  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        {this.state.graphProps.map((chart, i)=>
          (<PwmaChart key={`c${i}`} {...chart} xScaleType="time" height={300} title={chart.title} />)
        )}
      </PwmaMain>
    );
  }
}

multiChart.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
