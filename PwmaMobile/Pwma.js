import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  ScrollView,
  Button,
  TouchableOpacity,
  Image,
  Alert,
  Linking,
  Platform,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import RNFirebase from 'react-native-firebase';

import PwmaTree from './PwmaComponents/PwmaTree';
import PwmaStorage from './PwmaComponents/PwmaStorage';
import PwmaMain from './PwmaComponents/PwmaMain';
import PwmaCredentials from './PwmaComponents/PwmaCredentials';
// https://www.npmjs.com/package/base-64
const base64 = require('base-64');

// customizable part starts here
import FermiStatus from './PwmaScreens/FermiStatus';
import ElettraStatus from './PwmaScreens/ElettraStatus';
// import ScwStatus from './PwmaScreens/ScwStatus';
import cAstor from './PwmaScreens/cAstor';
import cAstorFault from './PwmaScreens/cAstorFault';
import cAstorDevice from './PwmaScreens/cAstorDevice';
import cAstorAttribute from './PwmaScreens/cAstorAttribute';
import OperatorShift from './PwmaScreens/OperatorShift';
import PwmaLoader from './PwmaScreens/PwmaLoader';
import PwmaScreenList from './PwmaScreens/PwmaScreenList';
import PwmaAlarms from './PwmaScreens/PwmaAlarms';
// import listEvents from './PwmaScreens/listEvents';
// import EpicsTest from './PwmaScreens/EpicsTest';
// import ScreenTest from './PwmaScreens/ScreenTest';
import pssHardware from './PwmaScreens/pssHardware';
import ElettraPhonebook from './PwmaScreens/ElettraPhonebook';
import FermiPressures from './PwmaScreens/FermiPressures';
import posCcdPesp from './PwmaScreens/posCcdPesp';
import multiChart from './PwmaScreens/multiChart';


const configurationOptions = {
  // 'messagingSenderId': 'pwma-7c60d ',
  'messagingSenderId': '1:723966483617:android:e980018717f957b1',
  debug: true,
};

const firebase = RNFirebase.initializeApp(configurationOptions);
// const firebase = RNFirebase.app(configurationOptions);

export default firebase;

let myToken = 'none';

const components = {
    FermiStatus: FermiStatus,
    ElettraStatus: ElettraStatus,
    // ScwStatus: ScwStatus,
    cAstor: cAstor,
    cAstorFault: cAstorFault,
    cAstorDevice: cAstorDevice,
    cAstorAttribute: cAstorAttribute,
    OperatorShift: OperatorShift,
    PwmaLoader: PwmaLoader,
    PwmaScreenList: PwmaScreenList,
    PwmaAlarms: PwmaAlarms,
    // listEvents: listEvents,
    // ScreenTest: ScreenTest,
    // EpicsTest: EpicsTest,
    pssHardware: pssHardware,
    ElettraPhonebook: ElettraPhonebook,
    FermiPressures: FermiPressures,
    posCcdPesp: posCcdPesp,
    multiChart: multiChart,
  };

const treeData = [{
  title: 'available sreens',
  data: [
    {
      title: 'Alarms',
      component: 'PwmaAlarms',
    },
    {
      title: 'FERMI',
      data: [
        {
          title: 'FERMI Status',
          component: 'FermiStatus',
        },
      ],
    },
    {
      title: 'Elettra',
      data: [
        {
          title: 'Elettra Status',
          component: 'ElettraStatus',
        },
      ],
    },
    {
      title: 'Common Services',
      data: [
        {
          title: 'Phonebook import',
          component: 'ElettraPhonebook',
        },
        {
          title: 'Operator Shifts',
          component: 'OperatorShift',
        },
        {
          title: "User's Screens",
          component: 'PwmaScreenList',
        },
      ],
    },
  ]
}];
// customizable part stops here

let cAstorMode = 'cAstorFault';
let ws = null;
let wsStart = 0;
let alarmCallback = null;
const wsServer = 'ws://pwma.elettra.eu:10080/updates';
const restServer = 'https://pwma.elettra.eu:10443/v1';

function registerAlarmCallback(param) {
  console.log('registerAlarmCallback');
  console.log(param);
  alarmCallback = param;
}
function fetchDebug(url) {
  const request = new XMLHttpRequest();
  request.onreadystatechange = (e) => {
    if (request.readyState !== 4) {
      console.log('request.readyState', request.readyState);
      return;
    }
    if (request.status === 200) {
      console.log('success', request.responseText);
    } else {
      console.warn('error', request);
    }
  };
  request.open('GET', url);
  request.send();
}
function initWs() {
  if (ws === null) {
    console.log('initWs(), ws===null');
    ws = new WebSocket(wsServer);
    wsStart = (new Date).getTime();
  }
  ws.onerror = (e) => {
    // an error occurred
    fetch(`https://pwma.elettra.eu:10443/v1/cs/`)
    .then((response) => {
      console.log('response.status', response.status);
      if (response.status>=300) {
        console.log(`onError() ******************************************* ${e.message} ******************************************* `);
        if (alarmCallback && e.message !== 'Code 2560 is reserved and may not be used.') alarmCallback('ws');
      }
    })
    .catch((err) => {
      console.log(`onerror() ******************************************* ${e.message} ******************************************* `);
      if (alarmCallback && e.message !== 'Code 2560 is reserved and may not be used.') alarmCallback('ws');
    });
  };
  ws.onclose = (e) => {
    // connection closed
    console.log(`onclose: ${e[0]}`);
    ws = new WebSocket(wsServer);
    /*
    console.log(wsStart, (new Date).getTime());
    if (wsStart - (new Date).getTime() > 250) {
      console.log('TODO: reconnect');
    }
    else {
      console.log('this.ws.onclose', (new Date).getTime());
    }
    */
  };

  return ws;
}

class Screens extends React.Component {

  // Nav options can be defined as a function of the screen's props:
  static navigationOptions = ({ navigation }) => {
    const {state, setParams} = navigation;
    this.state = state;
    this.setParams = setParams;
    const iscAstorBase = navigation.state.params.component === 'cAstor';
    const iscAstor = iscAstorBase || navigation.state.params.component === 'cAstorFault';
    return {
      title: navigation.state.params.title,
      headerRight: !iscAstor ? null : (
        <TouchableOpacity onPress={() => setParams({ component: iscAstorBase ? 'cAstorFault' : 'cAstor'})} >
          <Image style={{ width: 18, height: 18, marginRight: 10, }} source={require('./img/mode.png')} />
        </TouchableOpacity>
      ),
    };
  };
  /*
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title,
  });
  */
  componentWillUnmount() {
    // todo close websocket on double R
    console.debug('Screens CLOSE', typeof this.props.navigation.state.params.onClose);
    if (typeof this.props.navigation.state.params.onClose == 'function') this.props.navigation.state.params.onClose();
    // ws.close();
  }
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const { params } = this.props.navigation.state;
    if (params.component.indexOf('http')===0) {
      Linking.openURL(params.component).catch(err => console.error('An error occurred', err));
      return null;
    }
    if (params.component.indexOf('pwma://userscreen')===0) {
      console.log('Screens, params: ', params);
      return (<PwmaLoader globalParams={{initWs: initWs, registerAlarmCallback: registerAlarmCallback, navigation: this.props.navigation, page: params.param, server: restServer}} />);
      // navigate('SubPage', { title: data.target.split('.json')[0], component: 'PwmaLoader', device: param, domain, param });
    }
    else if (params.component) {
      const Screen = components[params.component]? components[params.component]: null;
      //
      console.log('Screens, params: ', params);
      const param = params.param? params.param: null;
      // const param = params.param? params.param: myToken;
      if (Screen) return (<Screen globalParams={{initWs: initWs, registerAlarmCallback: registerAlarmCallback, navigation: this.props.navigation, page: param, server: restServer}} />);
      // if (Screen) return (<Screen initWs={initWs} navigation={this.props.navigation} param={param} />);
      return null;
    }
  }
}

//-------------------------------------------------------------------------------------------------------------
class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {mode: false,},
      modalVisible: false,
      errorMessage: '',
      tree: treeData,
    };
    this.onItemClicked = this.onItemClicked.bind(this);
    this.getAlarms = this.getAlarms.bind(this);
    this.getToken = this.getToken.bind(this);
    this.notifyToken = this.notifyToken.bind(this);
    this.subscribeAlarms = this.subscribeAlarms.bind(this);
    this.onSwitchConfirmed = this.onSwitchConfirmed.bind(this);
    this.saveUsername = this.saveUsername.bind(this);
    this.getUsername = this.getUsername.bind(this);
    this.onSetMessage = this.onSetMessage.bind(this);
    // AsyncStorage.getItem('cAstorMode').then((value) ⇒ this.setState({ 'cAstorMode': value }))
  }
  newToken = null;
  getInitialNotification() {
    console.error('getInitialNotification');
  }
  componentDidMount() {
    console.log('HomeScreen::componentDidMount()', restServer, Platform);

    PwmaStorage('get', 'PwmaUsername', this.getUsername);

    // fetchDebug(restServer);
    fetch(`${restServer}`)
    .then((response) => {
      if (response.status>=300) {
        console.log('fetch.status>=300', restServer, response);
        alarmCallback('rest');
      }
    })
    .catch((err) => {
      console.log('fetch err ', restServer, err);
      alarmCallback('rest');
    });
    // https://github.com/invertase/react-native-firebase
    // https://firebase.google.com/docs/cloud-messaging/android/start/
    // https://rnfirebase.io/docs/v3.0.*/messaging/reference/messaging
    // https://firebase.google.com/docs/cloud-messaging/js/receive
    // https://console.firebase.google.com/?pli=1
    // https://firebase.google.com/docs/cloud-messaging/android/receive#sample-receive
    // https://www.codementor.io/flame3/send-push-notifications-to-android-with-firebase-du10860kb
    // https://developers.google.com/cloud-messaging/android/android-migrate-fcm
    // https://rnfirebase.io/docs/v3.1.*/messaging/android
      /*
      firebase.auth().signInAnonymously()
      .then((user) => {
        console.log(user.isAnonymous);
      });
      */
      try {
        firebase.messaging().getInitialNotification()
        .then((notification) => {
          console.log('Notification which opened the app: ', notification);
          if (notification['google.sent_time']) {
            const { navigate } = this.props.navigation;
            navigate('SubPage', { title: 'Alarms', component: 'PwmaAlarms', param: 'counter'});
          }
        });
        firebase.messaging().onMessage((message) => {
           console.log('firebase.messaging()', message.fcm, alarmCallback !== null);
           if (alarmCallback !== null) alarmCallback(message.fcm);
        })
      } catch (error) {
        console.error(error);
      }
    const ws = initWs();
    // this.ws.addEventListener('message', this.wsListener);
  }

  onSetMessage(message) {
    if (this.state.errorMessage !== message) this.setState({ errorMessage: message });
  }
  saveUsername(myUsername) {
    console.log('myUsername', myUsername.value);
    PwmaStorage('set', 'PwmaUsername', myUsername.value);
    this.getUsername([ myUsername.value ]);
  }
  getUsername(myUsername) {
    const username = myUsername === null ? null: myUsername[0];
    if (myUsername === null){
      console.log('user is Anonymous');
      this.setState({ username: '', modalVisible: true });
    }
    else {
      this.username = myUsername[0];
      this.setState({ username: myUsername[0] });
      fetch(`https://pwma.elettra.eu/pwma_starter.php?tree&mobile&username=${this.username}`)
      .then(response => response.json())
      .then((customTree) => {
        const myAlarms = [
          {
            title: 'Alarms',
            component: 'PwmaAlarms',
          }];
        const data = myAlarms.concat(customTree,
        {
          title: 'Common Services',
          data: [
            {
              title: 'Phonebook import',
              component: 'ElettraPhonebook',
            },
            {
              title: 'Operator Shifts',
              component: 'OperatorShift',
            },
            {
              title: "User's Screens",
              component: 'PwmaScreenList',
            },
          ],
        })
        const myTree = [{
          title: 'available sreens',
          data: data
        }];
        this.setState({ tree: myTree});
        console.log('fetch.status==200', `https://pwma.elettra.eu/pwma_starter.php?tree&mobile&username=${this.username}`, myTree);
      })
      .catch((err) => {
        console.log('fetch err ', `https://pwma.elettra.eu/pwma_starter.php?tree&mobile&username=${this.username}`, err);
      });
    }
    PwmaStorage('get', 'PwmaToken', this.getToken);
  }
  onSwitchConfirmed(username, password) {
    if (username.indexOf('@')!=-1) username = username.split('@')[0];
    const serverArray = restServer.split('/');
    serverArray.pop();
    const url = serverArray.join('/');
    console.debug(`${url}/auth`,username);
    var headers = new Headers();
    headers.append("Authorization", "Basic " + base64.encode(`${username}:${password}`));
    headers.append("Accept", 'application/json');
    // headers.append('Content-Type', 'application/json');
    fetch(`${url}/auth`, {
      method: 'POST',
      headers: headers,
    })
    .then(response => {
      console.log('Pwma::onSwitchConfirmed, response', response);
      if (response.status==200) {
        this.setState({ modalVisible: false });
        this.saveUsername({value: [username]});
      }
      else {
        this.setState({ errorMessage: 'ERROR: invalid username or password' });
      }
    })
    .catch(err => {
        console.log(`Pwma::onSwitchConfirmed, fetch(${url}/auth)`,err);
        this.setState({ errorMessage: 'ERROR: credentials cannot be checked' });
    });
  }

  subscribeAlarms(alarms) {
    console.log('HomeScreen::subscribeAlarms()');
    alarms.map((alarm) => {
      const tok = alarm.formula.split(' ');
      console.log(tok);
      ws.send(`{"data":{"type":"subscribe"},"event":"${tok[0]}"}`);
      return false;
    });
  }

  notifyToken(previousToken) {
    if (!previousToken) {
      console.log('notify to canoned new token');
    }
    if (previousToken && previousToken[0] !== this.newToken) {
      console.log('notify to canoned token changed');
    }
    else {
      console.log('do not notify anything to canoned');
    }
  }

  getToken(token) {
    console.log('getToken', token);
    // Alert.alert( 'token', token, [ {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')}, {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}, {text: 'OK', onPress: () => console.log('OK Pressed')}, ], { cancelable: false } )
    if (token === null){
      try {
        firebase.auth().signInAnonymously()
        .then((user) => {
          console.log(user.isAnonymous);
        });
        // https://firebase.google.com/docs/cloud-messaging/android/client
        firebase.messaging().getToken()
        .then((token) => {
          // myToken = token;
          if (!token || token.length < 20) {
            Alert.alert('OK', 'ERROR\nPwmaMobile cannot successfully initialize token from Google FCM.\n\nPlease check your Data usage and try again\n');
          }
          else {
            PwmaStorage('set', 'PwmaToken', token);
            Alert.alert('OK', 'PwmaMobile successfully initialized.\nA token was received from Google FCM.\n\nToken:\n'+token);
            this.newToken = token;
            PwmaStorage('get', 'PwmaPreviousToken', this.notifyToken);
          }
          // Alert.alert( 'token', JSON.stringify(token));
        });
      } catch (error) {
        Alert.alert('FCM Token initialization Failure', error);
      }
    }
  }

  getAlarms(value) {
    console.log('HomeScreen::getAlarms()', value);
    const alarms = JSON.parse(value);
    if (ws) {
      console.log('HomeScreen::getAlarms(), ws.readyState', ws.readyState);
      if (!ws.readyState) {
        console.log('HomeScreen::getAlarms(), !ws.readyState');
        const that = this;
        ws.onopen = () => {
          console.log('HomeScreen::getAlarms(), ws.onopen');
          that.subscribeAlarms(alarms);
        };
      }
      else {
        console.log('HomeScreen::getAlarms(), ws.readyState');
        this.subscribeAlarms(alarms);
      }
    }
  }
  onItemClicked(node) {
    const { navigate } = this.props.navigation;
    if (node.component) navigate('SubPage', { title: node.title, component: node.component, param: node.param, onItemClicked: this.onItemClicked});
  }
  componentWillUnmount() {
    // todo close websocket on double R
    console.debug('HomeScreen CLOSE');
    // ws.close();
  }
  static navigationOptions = {
    title: 'PWMA',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <PwmaMain globalParams={{initWs: initWs, registerAlarmCallback: registerAlarmCallback, navigation: this.props.navigation, page: 'home', server: restServer}}>
        <PwmaCredentials
          transparent={false}
          modalVisible={this.state.modalVisible}
          onSwitchConfirmed={this.onSwitchConfirmed}
          onSetMessage={this.onSetMessage}
          message={this.state.errorMessage}
        />
        {console.debug('username', this.username)}
        <PwmaTree data={this.state.tree} onItemClicked={this.onItemClicked} />
      </PwmaMain>
    );
  }
}


const PwmaMobile = StackNavigator({
  Home: { screen: HomeScreen },
  SubPage: { screen: Screens },
});


AppRegistry.registerComponent('PwmaMobile', () => PwmaMobile);
