import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Image,
} from 'react-native';
import PwmaTree from '../PwmaComponents/PwmaTree';
import PwmaCredentials from '../PwmaComponents/PwmaCredentials';
import PwmaMain from '../PwmaComponents/PwmaMain';

const onOffImage = require('../img/on_off.png');
// https://www.npmjs.com/package/base-64
const base64 = require('base-64');

const treeData = [{
  title: 'Starter list',
  data: [],
}];
const colorMap = {
  ON: 'green',
  ALARM: 'orange',
  MOVING: 'blue',
  FAULT: 'red',
  OFF: 'red',
};

function compareTitle(a, b) {
  if (a.title < b.title) return -1;
  if (a.title > b.title) return 1;
  return 0;
}

export default class cAstorDevice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      treeData: [],
      modalVisible: false,
      modalDevice: '',
      modalOperation: '',
      errorMessage: '',
    };
    this.args = { username: null, password: null };
    this.appendItem = this.appendItem.bind(this);
    this.onSwitchClicked = this.onSwitchClicked.bind(this);
    this.onSwitchConfirmed = this.onSwitchConfirmed.bind(this);
    this.onSetMessage = this.onSetMessage.bind(this);
    this.onSwitched = this.onSwitched.bind(this);
    this.wsListener = this.wsListener.bind(this);
    this.updateServers = this.updateServers.bind(this);
    this.onItemClicked = this.onItemClicked.bind(this);
  }

  componentDidMount() {
    treeData[0].data = [];
    this.ws = this.props.globalParams.initWs();
    this.ws.addEventListener('message', this.wsListener);
    const src = `${this.props.globalParams.navigation.state.params.domain}/tango/admin/${this.props.globalParams.navigation.state.params.device}/Servers`;
    console.log('this.props.globalParams.navigation.state.params', this.props.globalParams.navigation.state.params);
    fetch(`${this.props.globalParams.server}/cs/${src}`)
    .then(responseJson => responseJson.json())
    .then((state) => {
      this.updateServers(state);
    })
    .catch((err) => {
      console.log(`fetch ${this.props.globalParams.server}/cs/${src}`);
      console.log('.catch((err)', err);
    });
  }
  componentWillUnmount() {
    if (this.ws) {
      // this.ws.send(`{"data":{"type":"unsubscribe"},"event":"${this.props.globalParams.navigation.state.params.domain}/tango/admin/${this.props.globalParams.navigation.state.params.device}/servers"}`);
      this.ws.removeEventListener('message', this.wsListener);
    }
  }
  onItemClicked(node) {
    if (typeof node.data === 'object' && node.data.length === 0) {
      const n = node;
      n.component = 'cAstorAttribute';
      //
      console.debug('cAstorDevice, onItemClicked(), n:', this.props);
      // const { navigate } = this.props.globalParams.navigation;
      // navigate('SubPage', { title: `cAstor ${node.title}`, component: 'cAstorAttribute', device: node.title, domain: this.props.globalParams.navigation.state.params.domain });
    }
  }

  onSwitchClicked(node) {
    const modalOperation = node.color === 'red' ? 'Start' : 'Stop';
    this.setState({ modalVisible: true, modalDevice: node.title, modalOperation, errorMessage: '' });
  }

  onSwitched() {
    const src = `${this.props.globalParams.navigation.state.params.domain}/tango/admin/${this.props.globalParams.navigation.state.params.device}/Servers`;
    this.setState({ modalVisible: false });
    fetch(`${this.props.globalParams.server}/cs/${src}`)
    .then(responseJson => responseJson.json())
    .then((state) => {
      this.updateServers(state);
    })
    .catch((err) => {
      console.log(`fetch ${this.props.globalParams.server}/cs/${src}`);
      console.log('.catch((err)', err);
    });
  }

  onSetMessage(message) {
    if (this.state.errorMessage !== message) this.setState({ errorMessage: message });
  }

  onSwitchConfirmed(username, password) {
    if (username===false && password===false) {
      this.setState({ modalVisible: false });
    }
    else {
      const cmd = `${this.props.globalParams.navigation.state.params.domain}/tango/admin/${this.props.globalParams.navigation.state.params.device}/Dev${this.state.modalOperation}`;
      console.debug(`${this.props.globalParams.server}/cs/${cmd},body: ${JSON.stringify({value: this.state.modalDevice})}`);
      var headers = new Headers();
      headers.append("Authorization", "Basic " + base64.encode(`${username}:${password}`));
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      const serverArray = this.props.globalParams.server.split('/');
      serverArray.pop();
      const url = serverArray.join('/');
      fetch(`${url}/auth`, {
      // fetch(`https://pwma.elettra.eu/cmd.php?cmd=${cmd}&params=${JSON.stringify({value: this.state.modalDevice})}`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify({value: cmd}),
      })
      .then(response => {
        console.log('Pwma::onSwitchConfirmed, response', response);
        if (response.status==200) {
          fetch(`${this.props.globalParams.server}/cs/${cmd}`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({value: this.state.modalDevice}),
          })
          .then(response => {
            console.log('cAstorDevice::onSwitchConfirmed, response', response);
            if (response.ok) {
              fetch(`https://pwma.elettra.eu/cmd_log.php?username=${username}&cmd=${cmd}&params=${this.state.modalDevice}`);        
              setTimeout(this.onSwitched, 3000);
            }
          })
        }
        else {
          console.log(response);
          this.setState({ errorMessage: 'ERROR: invalid username or password' });
        }
      })
      .catch(err => {
          console.log(`cAstorDevice::onSwitchConfirmed, fetch(${this.props.globalParams.server}/cs/${cmd})`,err);
          this.setState({ errorMessage: 'ERROR: credentials cannot be checked' });
      });
    }
  }
  onTextChanged(text, parameter) {
    this.args[parameter] = text;
  }

  wsListener(event) {
    const eventData = JSON.parse(event.data);
    if (eventData.event !== `${this.props.globalParams.navigation.state.params.domain}/tango/admin/${this.props.globalParams.navigation.state.params.device}/servers`) return;
  }

  updateServers(eventData) {
    // console.log('eventData', eventData, `${this.props.globalParams.navigation.state.params.domain}/tango/admin/${this.props.globalParams.navigation.state.params.device}/servers`);
    // if (`tango://${eventData.event}` !== `${this.props.globalParams.navigation.state.params.domain}/tango/admin/${this.props.globalParams.navigation.state.params.device}/servers`) return;
    console.log('cAstorDevice, treeData, eventData: ', treeData, eventData);
    for (let j = 0; j < treeData[0].data.length; j += 1) {
      treeData[0].data[j].color = 'green';
    }
    for (let i = 0; i < eventData.value.length; i += 1) {
      const tok = eventData.value[i].split('\t');
      const level = tok[3] === '0' ? ' Not controlled' : `Level ${tok[3]}`;
      let title = tok[0].trim();
      if (title.length > 24) {
        title = title.split('/').join('/\n');
      }
      let touched = false;
      const color = colorMap[tok[1]];
      for (let j = 0; j < treeData[0].data.length; j += 1) {
        if (treeData[0].data[j].title === level) {
          touched = true;
          let subtouched = false;
          for (let k = 0; k < treeData[0].data[j].data.length; k += 1) {
            if (treeData[0].data[j].data[k].title === title) {
              subtouched = true;
              treeData[0].data[j].data[k].color = color;
              break;
            }
          }
          if (!subtouched) {
            treeData[0].data[j].data.push({ title, data: [], color });
          }
          if (color === 'orange' && treeData[0].data[j].color === 'green') treeData[0].data[j].color = 'orange';
          if (color === 'blue' && (treeData[0].data[j].color === 'green' || treeData[0].data[j].color === 'orange')) treeData[0].data[j].color = 'blue';
          if (color === 'red' && (treeData[0].data[j].color === 'green' || treeData[0].data[j].color === 'orange' || treeData[0].data[j].color === 'blue')) treeData[0].data[j].color = 'red';
          break;
        }
      }
      if (!touched) {
        treeData[0].data.push({ title: level, color, data: [{ title, data: [], color }] });
        treeData[0].data.sort(compareTitle);
      }
    }
    this.setState({ treeData });
  }

  appendItem(type, node) {
    return typeof node.data === 'object' && node.data.length === 0 ? (
      <TouchableOpacity onPress={() => this.onSwitchClicked(node)} >
        <Image style={{ paddingTop: 24, paddingLeft: 12 }} source={onOffImage} />
      </TouchableOpacity>
    ) : null;
  }

  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        <PwmaCredentials
          transparent={false}
          modalVisible={this.state.modalVisible}
          onSwitchConfirmed={this.onSwitchConfirmed}
          onSetMessage={this.onSetMessage}
          message={this.state.errorMessage}
        />
        <PwmaTree
          data={this.state.treeData}
          appendItem={this.appendItem}
          onItemClicked={this.onItemClicked}
        />
      </PwmaMain>
    );
  }

}

cAstorDevice.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
