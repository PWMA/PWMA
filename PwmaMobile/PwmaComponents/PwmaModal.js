import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  ScrollView,
  StyleSheet,
} from 'react-native';

export default class PwmaModal extends Component {
  constructor() {
    super();
    this.state = {
      status: false,
    };
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        marginTop: 22,
        marginLeft: 10,
      },
      modal: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    });
    return (
      <Modal style={styles.modal} animationType="slide" transparent={false} visible={this.props.visible} onRequestClose={() => { console.log('Modal has been closed.'); }} >
        <ScrollView style={styles.continer}>
          {this.props.children}
        </ScrollView>
      </Modal>
    );
  }
}
PwmaModal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  visible: PropTypes.bool,
};
PwmaModal.defaultProps = {
  visible: false,
  children: undefined,
};
