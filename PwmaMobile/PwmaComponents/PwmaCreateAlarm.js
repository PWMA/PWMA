import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  TextInput,
} from 'react-native';

const pixelRatio = 3; // PixelRatio.get();

export default class PwmaCreateAlarm extends Component {
  constructor() {
    super();
    this.state = {
      label: 'mylabel',
      expiry: 7,
      valid: '',
      disequal: '>',
    };
    this.args = { username: null, password: null };
    this.saveExpiry = this.saveExpiry.bind(this);
    this.saveLabel = this.saveLabel.bind(this);
    this.saveValue = this.saveValue.bind(this);
    this.switchDisequal = this.switchDisequal.bind(this);
    this.onClick = this.onClick.bind(this);
  }
  onTextChanged(text, parameter) {
    this.args[parameter] = text;
  }

  onClick() {
    console.log('click', this.state);
    this.props.callback({
      label: this.state.label,
      var: this.props.var,
      expiry: this.state.expiry,
      disequal: this.state.disequal,
      value: this.state.value,
    });
  }
  saveExpiry(expiry) {
    console.log('saveExpiry(), ', expiry);
    this.setState({ expiry: expiry - 0 });
  }
  saveLabel(label) {
    this.setState({ label });
  }
  saveValue(value) {
    this.setState({ value });
  }
  switchDisequal() {
    const disequal = this.state.disequal === '>' ? '<' : '>';
    this.setState({ disequal });
  }

  abort() {
    this.setState({ valid: '' });
    this.props.onSwitchConfirmed(false);
  }

  render() {
    const styles = StyleSheet.create({
      view: {
        marginVertical: 10,
        marginHorizontal: 10,
      },
      container: {
        backgroundColor: '#f8fff8',
        flexDirection: 'row',
      },
      title: {
        fontSize: 8 * pixelRatio,
        color: 'blue',
        fontWeight: 'bold',
        marginLeft: 5,
      },
      label: {
        fontSize: 7 * pixelRatio,
        // textAlign: this.props.labelAlign,
        color: '#333333',
        marginRight: 5,
      },
      data: {
        fontSize: 7 * pixelRatio,
        flex: 0,
        height: 9 * pixelRatio,
        width: this.props.dataWidth,
        borderWidth: 1,
        padding: 2,
        marginBottom: 3,
        textAlign: 'right',
        backgroundColor: '#FFFFFF',
        fontWeight: 'bold',
        color: 'blue',
      },
      button: {
        borderRadius: 3,
        height: 30,
        backgroundColor: '#2196F3',
        borderColor: '#2399F8',
        color: 'white',
        textAlign: 'center',
        textAlignVertical: 'center',
        marginTop: 15,
      },
    });
    return (
      <View style={styles.view}>
        <Text style={styles.title}>Custom Alarm configuration</Text>
        <Text>{' '}</Text>
        <Text style={styles.label}>Alarms must have a unique label.</Text>
        <View style={styles.container}>
          <Text style={styles.label}>Label</Text>
          <TextInput
            style={styles.data} defaultValue="mylabel"
            text="" maxLength={20}
            onChangeText={text => this.saveLabel(text)}
          />
        </View>
        <Text style={styles.label}>Alarms must expire within 99 days.</Text>
        <View style={styles.container}>
          <Text style={styles.label}>Days before expire</Text>
          <TextInput
            style={styles.data} defaultValue="7" text=""
            maxLength={2} keyboardType="numeric"
            onChangeText={text => this.saveExpiry(text)}
          />
        </View>
        <View style={styles.container}>
          <Text style={styles.label}>{this.props.varLabel}</Text>
          <TouchableOpacity onPress={this.switchDisequal}><Text style={styles.title}>{`   ${this.state.disequal}   `}</Text></TouchableOpacity>
          <TextInput
            style={styles.data} defaultValue="" text=""
            keyboardType="numeric"
            onChangeText={text => this.saveValue(text)}
          />
        </View>
        <TouchableOpacity onPress={this.onClick}>
          <Text style={styles.button}>SAVE</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.abort}>
          <Text style={styles.button}>Abort</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
PwmaCreateAlarm.propTypes = {
  var: PropTypes.string.isRequired,
  varLabel: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired,
  onSwitchConfirmed: PropTypes.func.isRequired,
  dataWidth: PropTypes.number,
};
PwmaCreateAlarm.defaultProps = {
  dataWidth: 100,
};
