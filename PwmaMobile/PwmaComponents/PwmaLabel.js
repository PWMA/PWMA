import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
} from 'react-native';

const pixelRatio = 3; // PixelRatio.get();

export default class PwmaLabel extends Component {
  render() {
    return (
      <Text style={this.props.style}>
        {this.props.text}
      </Text>
    );
  }
}
PwmaLabel.propTypes = {
  style: PropTypes.object,
  text: PropTypes.string.isRequired,
};
PwmaLabel.defaultProps = {
  style: {
    fontSize: 11 * pixelRatio,
    color: '#3333ff',
    margin: 1 * pixelRatio,
    textAlign: 'center',
    fontWeight: 'bold',
  },
};
