import React, { Component } from 'react';

export default class PwmaMain extends Component {
  constructor(){
    super();
    this.state ={
      status: false
    }
  }

  render() {
    return (
      <div>
          {this.props.children}
      </div>
    );
  }
}
