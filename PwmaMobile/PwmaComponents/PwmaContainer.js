import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Button,
} from 'react-native';

export default class PwmaContainer extends Component {
  constructor() {
    super();
    this.state = {
      status: false,
    };
  }

  toggleStatus() {
    this.setState({
      status: !this.state.status,
    });
    // console.log(`toggle button ${this.props.label} handler: ${this.state.status}`);
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        margin: 5,
      },
      button: {
        marginBottom: 5,
        paddingBottom: 5,
      },
      children: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: '#f8fff8',
      },
    });
    return this.props.visible ? (
      <View style={styles.container}>
        <Button
          style={styles.button}
          onPress={() => this.toggleStatus()}
          title={this.props.label}
        />
        {this.state.status && (
          <View style={styles.children}>
            {this.props.children}
          </View>
        )}
      </View>
    ) : null;
  }
}
PwmaContainer.propTypes = {
  visible: PropTypes.bool,
  label: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};
PwmaContainer.defaultProps = {
  visible: true,
  label: '',
  children: undefined,
};
