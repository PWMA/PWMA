import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import PwmaCredentials from '../PwmaComponents/PwmaCredentials';
import PwmaMain from '../PwmaComponents/PwmaMain';

const onOffImage = require('../img/on_off.png');
const redRightImage = require('../img/red_right.png');
const blueRightImage = require('../img/blue_right.png');

let domain = '';

export default class cAstorFault extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: {
        red: [],
        orange: [],
        blue: [],
      },
      modalVisible: false,
      modalDevice: '',
      modalOperation: '',
    };
    this.onSwitchClicked = this.onSwitchClicked.bind(this);
    this.onSwitchConfirmed = this.onSwitchConfirmed.bind(this);
    this.wsListener = this.wsListener.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
  }
  componentDidMount() {
    console.debug('cAstorFault, componentDidMount(), props: ', this.props);
    // this.setState({ treeData: treeData });
    if (this.props.globalParams.page) domain = this.props.globalParams.page;
    this.ws = this.props.globalParams.initWs();
    if (this.ws) {
      if (!this.ws.readyState) {
        this.ws.onopen = () => {
          this.subscribeVar(domain);
        };
      } else {
        this.subscribeVar(domain);
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  subscribeVar(domain) {
    console.log('subscribeVar()', `${this.props.globalParams.server}/cs/${domain}/tango/admin/*`);
    fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/*`)
    .then(responseJson => responseJson.json())
    .then((response) => {
      componentList = response;
      console.log('componentList', componentList);
      componentNumber = response.length;
      for (let j=0; j<response.length; j++) {
        console.log(`${this.props.globalParams.server}/cs/${domain}/tango/admin/${response[j]}/Servers`);
        fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/${response[j]}/Servers`)
        .then(responseJson => responseJson.json())
        .then((Servers) => {
          // console.log('HostCollection', j, response[j], Servers.value.length, typeof(Servers.value));
          const value = typeof(Servers.value)==='string' ? [Servers.value] : Servers.value;
          for (let i=0; i<value.length; i++) {
            const stat = value[i].split('\t');
            if (stat[1] === 'FAULT' || stat[1] === 'OFF') {
              this.setState((prevState) => {prevState.listData.red.push({server: stat[0], device: response[j]}); return {listData: prevState.listData};});
            }
            if (stat[1] === 'ALARM') {
              this.setState((prevState) => {prevState.listData.orange.push({server: stat[0], device: response[j]}); return {listData: prevState.listData};});
            }
            if (stat[1] === 'MOVING') {
              this.setState((prevState) => {prevState.listData.blue.push({server: stat[0], device: response[j]}); return {listData: prevState.listData};});
            }
          }
        })
        .catch((err) => {
          console.log('retry HostCollection', j, response[j]);
        });
      }
    })
    .catch((err) => {
      console.log('.catch(err)', err);
      return false;
    });
  }
  onSwitchClicked(i, color) {
    const modalOperation = color === 'red' ? 'Start' : 'Stop';
    console.log('onSwitchClicked', i, color);
    this.setState({ modalVisible: true, modalDevice: i.server, modalOperation: `${i.device}/Dev${modalOperation}` });
  }
  onSwitchConfirmed(valid) {
    if (valid) {
      const cmd = this.state.modalOperation;
      console.debug(`cAstorFault, {"data":{"type":"execute","value": "${this.state.modalDevice}"},"event":"${cmd}"}`);
      fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/${this.state.modalOperation}`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({value: this.state.modalDevice}),
      })
      .then(response => {
        console.log('cAstorFault::onSwitchConfirmed, response', response);
      })
      .catch(err => {
          console.log(`cAstorFault::onSwitchConfirmed, fetch(${this.props.globalParams.server}/cs/${domain}/tango/admin/${this.state.modalOperation})`,err);
      });
      // this.ws.send(`{"data":{"type":"execute","value": "${this.state.modalDevice}"},"event":"${cmd}"}`);
    }
    this.setModalVisible(!this.state.modalVisible);
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  wsListener(event) {
    if (event.data === undefined) {
      console.log('event', event);
      return;
    }
    const eventData = JSON.parse(event.data);
    if (eventData.data === undefined || eventData.data.type === undefined) {
      console.log('eventData', eventData);
      return;
    }
  }

  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        <PwmaCredentials
          transparent={false}
          modalVisible={this.state.modalVisible}
          onSwitchConfirmed={this.onSwitchConfirmed}
        />
        {
          Object.keys(this.state.listData.red).length +
          Object.keys(this.state.listData.blue).length === 0 && (
          <Text> All devices are OK!</Text>
        )}
        {Object.keys(this.state.listData.red).map(i => (
          <View key={this.state.listData.red[i].server} style={{ flexDirection: 'row', marginLeft: 10, marginTop: 10 }} >
            <Image source={redRightImage} />
            <Text style={{ marginRight: 5 }}>{this.state.listData.red[i].server}</Text>
            <TouchableOpacity onPress={() => this.onSwitchClicked(this.state.listData.red[i], 'red')} >
              <Image style={{ width: 18, height: 18 }} source={onOffImage} />
            </TouchableOpacity>
          </View>
        ))}
        {Object.keys(this.state.listData.blue).map(i => (
          <View key={this.state.listData.blue[i].server} style={{ flexDirection: 'row', marginLeft: 10, marginTop: 10 }} >
            <Image source={blueRightImage} />
            <Text style={{ marginRight: 5 }}>{this.state.listData.blue[i].server}</Text>
            <TouchableOpacity onPress={() => this.onSwitchClicked(this.state.listData.blue[i], 'blue')} >
              <Image style={{ width: 18, height: 18 }} source={onOffImage} />
            </TouchableOpacity>
          </View>
        ))}
      </PwmaMain>
    );
  }

}

cAstorFault.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
