import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaInput from '../PwmaComponents/PwmaInput';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaModal from '../PwmaComponents/PwmaModal';
import PwmaStorage from '../PwmaComponents/PwmaStorage';
import PwmaCreateAlarm from '../PwmaComponents/PwmaCreateAlarm';

const thunderImage = require('../img/thunder.png');

const treeData = [{
  title: 'Starter list',
  data: [],
}];

function purgeDomain(device) {
  const tok = device.split('/');
  if (tok.length < 6) return 'N.A.';
  return `${tok[3]}/${tok[4]}/${tok[5]}`;
}
function getDate() {
  const m = new Date();
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${m.getUTCMinutes()}:${m.getUTCSeconds()}`;
}


export default class cAstorAttribute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classList: {},
      modalVisible: false,
      modalDevice: '',
      modalOperation: '',
      alarms: [],
      expiry: 7,
      label: '',
      attr: '',
      attrLabel: '',
      value: '',
      error: null,
      dateTime: '',
    };
    this.token = null;
    this.args = { username: null, password: null };
    this.readVar = this.readVar.bind(this);
    this.wsListener = this.wsListener.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
    this.onSaveAlarm = this.onSaveAlarm.bind(this);
    this.onSaveAttr = this.onSaveAttr.bind(this);
    this.saveExpiry = this.saveExpiry.bind(this);
    this.saveLabel = this.saveLabel.bind(this);
    this.getToken = this.getToken.bind(this);
  }

  componentDidMount() {
    PwmaStorage('get', 'PwmaToken', this.getToken);
    treeData[0].data = [];
    this.ws = this.props.globalParams.initWs();
    // console.debug('cAstorAttribute, props', this.props.globalParams.navigation.state.params);
    if (this.ws) {
      this.ws.send(`{"data":{"type":"get_device_class_list", "value":"${this.props.globalParams.navigation.state.params.device.split('\n').join('')}"},"event":"${this.props.globalParams.navigation.state.params.domain}"}`);
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      if (this.state.attr.length) this.ws.send(`{"data":{"type":"unsubscribe"},"event":"${this.state.attr}"}`);
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  getToken(myToken) {
    console.log('getToken', myToken);
    this.token = myToken;
  }
  onSaveAlarm(alarm) {
    console.log(alarm);
    if (alarm.value) {
      const expiry = Date.now() + 86400 * this.state.expiry;
      // PwmaStorage('append', 'alarms', { formula: `${this.state.attr} ${alarm.target} ${alarm.value}`, label: this.state.label, expiry });
      // this.ws.send(`{"data":{"type":"alarm", "label":"${alarm.label.split(' ').join('_')}", "formula":"${alarm.disequal} ${alarm.value}", "token": "${this.token}"},"event":"${this.state.attr}"}`);
      // console.log(`{"data":{"type":"alarm", "label":"${alarm.label.split(' ').join('_')}", "formula":"${alarm.disequal} ${alarm.value}", "token": "${this.token}"},"event":"${this.state.attr}"}`);
      this.ws.send(`{"data":{"type":"alarm", "label":"${alarm.label}", "formula":"${alarm.disequal} ${alarm.value}", "token": "${this.token}"},"event":"${this.state.attr}"}`);
      console.log(`{"data":{"type":"alarm", "label":"${alarm.label}", "formula":"${alarm.disequal} ${alarm.value}", "token": "${this.token}"},"event":"${this.state.attr}"}`);
    }
    this.setState({ modalVisible: false });
  }
  onSaveAttr(attrUC, attrLabel) {
    const attr = attrUC.toLowerCase();
    console.log('onSaveAttr()', attr);
    if (this.state.attr !== attr) {
      if (this.ws && this.state.attr.length) this.ws.send(`{"data":{"type":"unsubscribe"},"event":"${this.state.attr}"}`);
      this.setState({ modalVisible: true, attr, value: 'N.A.', attrLabel });
      if (this.ws) {
        this.ws.send(`{"data":{"type":"subscribe"},"event":"${attr}"}`);
        this.ws.send(`{"data":{"type":"unsubscribe"},"event":"${attr}"}`);
      }
    } else {
      this.setState({ modalVisible: true, attr, attrLabel });
    }
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  readVar(src) {
    return { data: this.state.value, alt: `src: ${src}\nerror: ${this.state.error}\nlastUpdate: ${getDate()}` };
  }
  wsListener(event) {
    const eventData = JSON.parse(event.data);
    const classList = this.state.classList;
    console.log('cAstorAttribute, wsListener(), eventData: ', eventData);
    if (eventData.data.type === 'get_device_class_list') {
      if (eventData.data.value) {
        for (let i = 0; i < eventData.data.value.length; i += 2) {
          if (eventData.data.value[i + 1] !== 'DServer') {
            const devSrv = eventData.data.value[i];
            // const devClass = eventData.data.value[i + 1];
            classList[`${this.props.globalParams.navigation.state.params.domain}/${devSrv}`] = [];
            this.ws.send(`{"data":{"type":"get_attribute_list", "value":""},"event":"${this.props.globalParams.navigation.state.params.domain}/${devSrv}"}`);
            this.setState({ classList });
          }
        }
      }
    }
    if (eventData.data.type === 'is_event_enabled') {
      // console.debug('is_event_enabled', eventData.data);
      if (eventData.data.value) {
        Object.keys(this.state.classList).map((devSrvFqdn) => {
          classList[devSrvFqdn].map((attr, i) => {
            if (`${devSrvFqdn}/${attr.name.toLowerCase()}` === eventData.event) {
              classList[devSrvFqdn][i].event = true;
              this.setState({ classList });
            }
            return false;
          });
          return false;
        });
      }
    }
    if (eventData.data.type === 'get_attribute_list') {
      for (let i = 0; i < eventData.data.value.length; i += 1) {
        const devAttr = eventData.data.value[i];
        if (devAttr.length > 0) {
          classList[eventData.event].push({ name: devAttr, event: false });
          this.ws.send(`{"data":{"type":"is_event_enabled", "value":""},"event":"${eventData.event}/${devAttr}"}`);
        }
      }
      this.setState({ classList });
    }
    if (eventData.event === this.state.attr) {
      // console.log('Message from server', event.data);
      let value = this.state.value;
      let error = this.state.error;
      value = eventData.data.error ? 'Error' : eventData.data.value;
      error = eventData.data.error ? eventData.data.error : '';
      this.setState({ value, error, dateTime: getDate() });
    }
  }
  saveExpiry(expiry) {
    console.log('saveExpiry(), ', expiry);
    this.setState({ expiry: expiry - 0 });
  }
  saveLabel(label) {
    this.setState({ label: label });
  }

/*
<PwmaScalar label="Custom Alarm configuration" labelAlign="left" />
<PwmaScalar label=" " />
<PwmaScalar label="Alarms must have a unique label" labelAlign="left" />
<PwmaInput
  label="Label" onChangeText={this.saveLabel}
  callback={this.saveLabel} keyboardType="default"
  defaultValue="mylabel" text="" maxLength={20}
/>
<PwmaScalar label=" " />
<PwmaScalar label="Alarms must have an expiry date" labelAlign="left" />
<PwmaScalar label="less than 100 days." labelAlign="left" />
<PwmaInput
  label="Days before expire" onChangeText={this.saveExpiry}
  callback={this.saveExpiry} keyboardType="numeric"
  defaultValue="7" text="" maxLength={2}
/>
<PwmaScalar label=" " />
<PwmaScalar label={this.state.attrLabel} value={this.readVar(this.state.attr)} />
<PwmaInput label={`${this.state.attrLabel} < `} target={'<'} keyboardType="numeric" text="SAVE" callback={this.onSaveAlarm} />
<PwmaInput label={`${this.state.attrLabel} > `} target={'>'} keyboardType="numeric" text="SAVE" callback={this.onSaveAlarm} />
<PwmaInput key="abort" target={''} text="Cancel" callback={this.onSaveAlarm} />
*/

  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        <PwmaModal visible={this.state.modalVisible} >
          <PwmaCreateAlarm callback={this.onSaveAlarm} value={this.readVar(this.state.attr)} varLabel={this.state.attrLabel} />
        </PwmaModal>
        {Object.keys(this.state.classList).map(devSrvFqdn => (
          <PwmaMain key={`m${devSrvFqdn}`}  globalParams={null} >
            <PwmaScalar key={devSrvFqdn} label={purgeDomain(devSrvFqdn)} labelAlign="left" />
            {this.state.classList[devSrvFqdn].map((attr, i) => (
              <PwmaInput key={`${attr.name}${i}`} text={attr.name} isButton={false} image={attr.event ? thunderImage : null} callback={() => this.onSaveAttr(`${devSrvFqdn}/${attr.name}`, attr.name)} />
            ))}
          </PwmaMain>
        ))}
      </PwmaMain>
    );
  }

}

cAstorAttribute.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
