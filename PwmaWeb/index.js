import React from 'react';
import { render } from 'react-dom';
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom';
import PwmaTree from './PwmaComponents/PwmaTree';
import PwmaDesigner from './PwmaComponents/PwmaDesigner';
import ElettraStatus from './PwmaScreens/ElettraStatus';
import FermiStatus from './PwmaScreens/FermiStatus';
import OperatorShift from './PwmaScreens/OperatorShift';

const treeData = [{
  title: 'available sreens',
  data: [
    {
      title: 'FERMI',
      data: [
        {
          title: 'FERMI Status',
          component: 'FermiStatus',
        },
        {
          title: 'FERMI Castor',
          component: 'Astor',
          param: 'tango://srv-tango-srf.fcs.elettra.trieste.it:20000',
        },
        {
          title: 'Other screens',
          data: [
            {
              title: 'N.A.',
            },
          ],
        },
      ],
    },
    {
      title: 'Elettra',
      data: [
        {
          title: 'Elettra Status',
          component: 'ElettraStatus',
        },
        {
          title: 'Elettra Castor',
          component: 'Astor',
          param: 'tango://tom.ecs.elettra.trieste.it:20000',
        },
        {
          title: 'Other screens',
          data: [
            {
              title: 'N.A',
            },
          ],
        },
      ],
    },
    {
      title: 'Common Services',
      data: [
        {
          title: 'Operator Shifts',
          component: 'OperatorShift',
        },
      ],
    },
    {
      title: 'Designer',
      component: 'designer',
    },
  ],
}];

let ws = null;
function initWs() {
  if (ws===null) {
    ws = new WebSocket('ws://pwma.elettra.eu:10000', ['tango8v1']);
  }
  return ws;
}

const Tree = () => (
  <div>
    <PwmaTree data={treeData} />
  </div>
);

const Home = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Tree} />
      <Route path="/designer" component={PwmaDesigner} />
      <Route path="/ElettraStatus" component={ElettraStatus} />
      <Route path="/FermiStatus" component={FermiStatus} />
      <Route path="/OperatorShift" component={OperatorShift} />
    </Switch>
  </BrowserRouter>
);

const Header = () => (
  <header>
    PWMA
  </header>
);

render((
  <BrowserRouter>
    <div>
      <Header />
      <Home />
    </div>
  </BrowserRouter>
), document.getElementById('root'));
