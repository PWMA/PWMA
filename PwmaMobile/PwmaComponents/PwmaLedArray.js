import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';

const redLed = require('../img/ledred.png');
const greenLed = require('../img/ledgreen.png');
const grayLed = require('../img/ledgray.png');


export default class PwmaLedArray extends Component {
  constructor(props) {
    super(props);
    this.globalStyle = this.globalStyle.bind(this);
    this.cellColor = this.cellColor.bind(this);
    this.cellValue = this.cellValue.bind(this);
    this.myPress = this.myPress.bind(this);
  }
  componentDidMount() {
    // console.log('flags:',this.props.flags);
    this.labelArray = !this.props.flags ? [] : ((typeof this.props.flags == 'string')? this.props.flags.split(';'): this.props.flags);
    // console.log(this.labelArray);
  }
  labelArray: [];
  cellValue(index) {
    if (this.labelArray.length == 0) return '';
    // console.log(this.labelArray);
    return  this.labelArray.length > index && this.labelArray[index] ? this.labelArray[index] : this.labelArray[0];
  }

  globalStyle() {
    const flexDirection = this.props.flexDirection !== undefined ? this.props.flexDirection : 'column';
    const justifyContent = this.props.justifyContent !== undefined ? this.props.justifyContent : 'flex-start';
    const alignItems = this.props.alignItems !== undefined ? this.props.alignItems : 'center';
    // console.log(flexDirection);
    return {
      flex: 1,
      flexDirection,
      justifyContent,
      alignItems,
      margin: 3,
    };
  }
  myPress() {
    if (this.props.link) this.props.link(this.props.flags);
  }
  cellColor(index) {
    if (this.props.value.data.length <= index) return grayLed;
    let v = this.props.value.data[index];
    if (this.props.trueColor === 'green') v = !v;
    return v ? redLed : greenLed;
  }

  render() {
    return (
      <TouchableOpacity onPress={this.myPress}>
        <View style={this.globalStyle()}>
          <Text style={{ marginLeft: 5 }}>{this.props.label}</Text>
          {typeof this.props.value.data === 'object' && this.props.value.data.map((v, index) => (
            <View key={`k${index}`} style={{ flex: this.cellValue(index).length ? 1 : 0, flexDirection: 'row' }}>
              <Image source={this.cellColor(index)} />
              <Text style={{ marginLeft: 5 }}>{`${this.cellValue(index)}`}</Text>
            </View>
          ))}
        </View>
      </TouchableOpacity>
    );
  }
}
PwmaLedArray.propTypes = {
  value: PropTypes.shape({
    data: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.bool),
      PropTypes.object,
    ]),
    alt: PropTypes.string,
  }),
  label: PropTypes.string,
  flags: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  trueColor: PropTypes.string,
  link: PropTypes.func,
  flexDirection: PropTypes.string,
  justifyContent: PropTypes.string,
  alignItems: PropTypes.string,
};
PwmaLedArray.defaultProps = {
  value: undefined,
  label: '',
  flags: '',
  trueColor: 'red',
  link: undefined,
  flexDirection: 'column',
  justifyContent: 'flex-start',
  alignItems: 'flex-start',
};
