import _ from 'lodash';

import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';

const dragSpec = {
	beginDrag(props, monitor, component) {
		return {id: props.id, index: props.index, parentId: props.parentId};
	}
};

const dropSpec = {
	hover(props, monitor, component) {
	}
};
	  
function dragCollect(connect, monitor) {

	return {
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging()
	}
}

function dropCollect(connect, monitor) {

	return {
		connectDropTarget: connect.dropTarget()
	}
}

class PwmaEncodedArray extends Component {
	
	constructor(props) {
		super(props);
		this.globalStyle = this.globalStyle.bind(this);
		this.cellStyle = this.cellStyle.bind(this);
		this.cellValue = this.cellValue.bind(this);
	}

  globalStyle() {
    const flexDirection = this.props.flexDirection !== undefined ? this.props.flexDirection : 'row';
    console.log(flexDirection);
    return {
      flex: 1,
      flexDirection,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 5,
    };
  }

  cellStyle(index) {
    const v = this.props.value[index];
    const backgroundColor = v !== undefined && this.props.codeMap[index][v].backgroundColor ? this.props.codeMap[index][this.props.value[index]].backgroundColor : 'white';
    const color = v !== undefined && this.props.codeMap[index][v].color ? this.props.codeMap[index][this.props.value[index]].color : 'black';
    const width = v !== undefined && this.props.codeMap[index][v].width ? this.props.codeMap[index][this.props.value[index]].width : 60;
    return {
      backgroundColor,
      color,
      width,
      height: 30,
      borderWidth: 1,
      padding: 2,
      fontSize: 20,
      textAlign: 'center',
      fontWeight: 'bold',
      marginTop: 5,
    };
  }

  cellValue(index) {
    const v = this.props.value[index];
    return v !== undefined && this.props.codeMap[index][v].value ? this.props.codeMap[index][v].value : 'N.A.';
  }

	render() {
		
		const {isDragging, connectDragSource, connectDropTarget} = this.props;
		const opacity = isDragging ? 0 : 1;
		
		const styles = {
			droppable: {
				border: "thin solid #777",
				padding: "4px",
				margin: "4px",
				cursor: "move",
				cursor: "grab",
				cursor: "-moz-grab",
				cursor: "-webkit-grab"
			}
		};

		if(this.props.droppable) {
			
			return connectDragSource(
				<div style={styles.droppable}>Encoded Array</div>
			);
		}		
		
		return connectDragSource(connectDropTarget(
			<span style={this.globalStyle()}>
				{Object.keys(this.props.codeMap).map(index => (
					<input style={this.cellStyle(index)} value={this.cellValue(index)} type="text" key={`${this.props.id}${index}`} readOnly />
				))}
			</span>
		));
	}
}

export default PwmaEncodedArray;

PwmaEncodedArray.propTypes = {
  value: PropTypes.array,
  codeMap: PropTypes.object,
  flexDirection: PropTypes.string,
  id: PropTypes.string,
};
PwmaEncodedArray.defaultProps = {
  value: [],
  codeMap: {},
  flexDirection: 'row',
};
