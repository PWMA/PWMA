import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaContainer from '../PwmaComponents/PwmaContainer';
import PwmaChart from '../PwmaComponents/PwmaChart';
import PwmaMain from '../PwmaComponents/PwmaMain';

const domain = 'tango://tom.ecs.elettra.trieste.it:20000';
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}

export default class ScwStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {},
      error: {},
      dateTime: '',
    };
    this.ws = '';
    this.onWriteRequest = this.onWriteRequest.bind(this);
    this.readVar = this.readVar.bind(this);
  }
  componentDidMount() {
    console.log('componentDidMount()');
    const that = this;
    this.ws = this.props.initWs();
    this.ws.onopen = () => {
      Object.keys(this.state.value).map((obj) => {
        console.log(`subscribe ${obj}`);
        // connection opened
        that.ws.send(`{"data":{"type":"subscribe"},"event":"${obj}"}`);
        return false;
      });
    };
    that.ws.addEventListener('message', (event) => {
      const value = that.state.value;
      const error = that.state.error;
      const eventData = JSON.parse(event.data);
      value[eventData.event] = eventData.data.error ? 'Error' : eventData.data.value;
      error[eventData.event] = eventData.data.error ? eventData.data.error : '';
      that.setState({ value, error, dateTime: getDate() });
    });
    that.ws.onerror = (e) => {
      // an error occurred
      console.log(`onerror: ${e.message}`);
    };
    that.ws.onclose = (e) => {
      // connection closed
      console.log(`onclose: ${e.code}, ${e.reason}`);
    };
  }
  componentWillUnmount() {
    if (this.ws) {
      const that = this;
      Object.keys(this.state.value).map((obj) => {
        console.log(`unsubscribe ${obj}`);
        that.ws.send(`{"data":{"type":"unsubscribe"},"event":"${obj}"}`);
        return false;
      });
    }
  }

  onWriteRequest(args) {
    console.log(`SEND COMMAND: {"data":{"type":"execute","value": ${args.value},"event":"${args.target}"}`);
    this.ws.send(`{"data":{"type":"execute","value": ${args.value}},"event":"${args.target}"}`);
  }
  readVar(src) {
    const lvar = src.toLowerCase();
    if (!this.state.value[lvar]) {
      this.state.value[lvar] = 'N.A.';
      if (this.ws) this.ws.send(`{"data":{"type":"subscribe"},"event":"${lvar}"}`);
    }
    const date = getDate();
    return { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${date}` };
  }
  render() {
    return (
      <PwmaMain>
        <PwmaChart color="#48F" title="SCW Current (last 8 hours)" src="http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%208%20hours&ts=01805&no_pretimer&no_posttimer" />
        <PwmaContainer label="Status">
          <PwmaScalar label="Magnetic Field" unit="[T]" value={this.readVar(`${domain}/sr/scw/scw_a11.1/MagneticField`)} />
          <PwmaScalar label="Helium Level" unit="[h]" value={this.readVar(`${domain}/sr/scw/jb_a11.1/HeliumLevel`)} />
          <PwmaScalar
            label="Info updated at" dataWidth={200}
            value={{ data: this.state.dateTime }}
          />
        </PwmaContainer>
        <PwmaContainer label="Elettra">
          <PwmaScalar label="Machine Info" unit="" value={this.readVar(`${domain}/elettra/status/userstatus/machinestatus`)} />
          <PwmaScalar label="SR Current" unit="[mA]" value={this.readVar(`${domain}/sr/DIAGNOSTICS/DCCT_S4/Current`)} />
        </PwmaContainer>
      </PwmaMain>
    );
  }
}


ScwStatus.propTypes = {
  initWs: PropTypes.func.isRequired,
};
