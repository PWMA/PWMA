import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  TouchableOpacity,
  // PixelRatio,
} from 'react-native';

const pixelRatio = 3; // PixelRatio.get();

export default class PwmaInput extends Component {
  constructor(props) {
    super(props);
    this.state = { value: 'n.a.' };
    // bundle of input arguments for write operations
    this.args = { target: this.props.target, value: null };
    // console.log('PwmaInput::constructor(), this.args', this.args);
  }

  onTextChanged(text) {
    this.args.value = text;
    if (this.props.onChangeText) this.props.onChangeText(text);
  }

  write() {
    console.log('PwmaInput::write(), this.args', this.args, this.props);
    // this.props.callback(this.args);
    this.props.callback({ target: this.props.target, value: this.args.value });
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        height: this.props.height > 0 ? this.props.height : 10 * pixelRatio,
        backgroundColor: this.props.backgroundColor,
        flexDirection: 'row',
      },
      label: {
        fontSize: 7 * pixelRatio,
        textAlign: this.props.labelAlign,
        width: this.props.labelWidth,
        color: '#333333',
        marginRight: 5,
      },
      data: {
        fontSize: 7 * pixelRatio,
        flex: 0,
        height: 9 * pixelRatio,
        width: this.props.dataWidth > 0 ? this.props.dataWidth : 100,
        borderWidth: 1,
        padding: 2,
        marginBottom: 3,
        textAlign: 'right',
        backgroundColor: '#FFFFFF',
        fontWeight: 'bold',
        color: 'blue',
      },
      unit: {
        flex: 0,
        fontSize: 5 * pixelRatio,
        width: this.props.unitWidth > 0 ? this.props.unitWidth : 35,
        textAlign: 'left',
        color: '#444444',
        marginLeft: 5,
      },
      noButtonText: {
        fontSize: 7 * pixelRatio,
      },
      buttonText: {
        fontSize: 6 * pixelRatio,
        fontWeight: 'bold',
        textAlign: 'center',
        textAlignVertical: 'center',
        height: 8 * pixelRatio,
        marginLeft: 5,
        borderWidth: 1,
        width: this.props.buttonWidth,
        overflow: 'hidden',
        borderRadius: 3,
        backgroundColor: this.props.buttonBackground,
        borderColor: '#2196F3',
        color: 'white',
      },
      image: {
        width: 8 * pixelRatio,
        height: 8 * pixelRatio,
        marginTop: 1 * pixelRatio,
        marginHorizontal: 1 * pixelRatio,
      },
    });
    return (
      <View style={styles.container}>
        {typeof (this.props.label) !== 'undefined' && this.props.label.length > 0 &&
          (<Text style={styles.label}>
            {this.props.label}
          </Text>)
        }
        {typeof (this.props.keyboardType) !== 'undefined' && this.props.keyboardType.length > 0 &&
          (<TextInput
            style={styles.data} keyboardType={this.props.keyboardType}
            defaultValue={this.props.defaultValue} maxLength={this.props.maxLength}
            onChangeText={text => this.onTextChanged(text)}
          />)
        }
        {typeof (this.props.unit) !== 'undefined' && this.props.unit.length > 0 &&
          (<Text style={styles.unit}>
            {this.props.unit}
          </Text>)
        }
        <TouchableOpacity onPress={() => this.write()}>
          <View style={styles.container}>
            {typeof (this.props.image) !== 'undefined' &&
              (<Image style={styles.image} source={this.props.image} />)
            }
            {this.props.text.length > 0 &&
              (<Text style={this.props.isButton ? styles.buttonText : styles.noButtonText}>
                {this.props.text}
              </Text>)
            }
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
PwmaInput.propTypes = {
  target: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string,
    ]),
  label: PropTypes.string,
  labelAlign: PropTypes.string,
  labelWidth: PropTypes.number,
  unit: PropTypes.string,
  text: PropTypes.string,
  buttonWidth: PropTypes.number,
  buttonBackground: PropTypes.string,
  defaultValue: PropTypes.string,
  isButton: PropTypes.bool,
  keyboardType: PropTypes.oneOf(['','default','numeric','email-address','phone-pad']),
  dataWidth: PropTypes.number,
  unitWidth: PropTypes.number,
  height: PropTypes.number,
  callback: PropTypes.func.isRequired,
  onChangeText: PropTypes.func,
  maxLength: PropTypes.number,
  image: PropTypes.object,
  backgroundColor: PropTypes.string,
};
PwmaInput.defaultProps = {
  target: '',
  label: '',
  labelAlign: 'right',
  labelWidth: 130,
  unit: '',
  text: 'APPLY',
  buttonWidth: 100,
  buttonBackground: '#2196F3',
  defaultValue: '',
  isButton: true,
  keyboardType: '',
  dataWidth: 0,
  unitWidth: 35,
  height: 0,
  onChangeText: undefined,
  maxLength: undefined,
  image: undefined,
  backgroundColor: '#f8fff8',
};
