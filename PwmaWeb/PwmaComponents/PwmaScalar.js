import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';

class PwmaScalar extends Component {
  
  constructor(props) {
    super(props);
  }
  
  render() {
    const styles = {
      container: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginTop: 5
      },
      label: {
        flex: 1,
        fontSize: 20,
        textAlign: 'right',
        color: '#f3333',
        marginRight: 5
      },
      value: {
        flex: 0.1,
        height: 30,
        minWidth: this.props.valueWidth > 0 ? this.props.valueWidth : 90,
        borderWidth: 1,
        padding: 2,
        fontSize: 20,
        textAlign: 'right',
        backgroundColor: '#FFFFFF',
        fontWeight: 'bold',
        color: 'blue',
        marginTop: 5
      },
      unit: {
        flex: 0,
        fontSize: 16,
        width: 50,
        minWidth: 35,
        textAlign: 'left',
        color: '#444444',
        marginLeft: 5
      }
    };
  console.log('PwmaScalar, styles.value.width', styles.value.width); 

    return (
      <div style={styles.container} title={this.props.value.alt}>
          {this.props.label !== null && this.props.label.length > 0 && (<div style={styles.label}>{this.props.label}</div>)}
          <input style={styles.value} type="text" name="name" value={this.props.value.data} readOnly />
          {this.props.unit !== null && this.props.unit.length > 0 && (<div style={styles.unit}>{this.props.unit}</div>)}
      </div>
    );
  }
}

export default PwmaScalar;


PwmaScalar.propTypes = {
  label: PropTypes.string,
  labelAlign: PropTypes.string,
  labelWidth: PropTypes.number,
  value: PropTypes.shape({
    data: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    alt: PropTypes.string,
  }),
  valueWidth: PropTypes.number,
  unit: PropTypes.string,
  unitWidth: PropTypes.number,
  padderFlex: PropTypes.number,
  paddingTop: PropTypes.number,
};
PwmaScalar.defaultProps = {
  label: '',
  labelAlign: 'right',
  labelWidth: 130,
  value: undefined,
  valueWidth: 0,
  unit: '',
  unitWidth: 0,
  padderFlex: 0,
  paddingTop: 5,
};



