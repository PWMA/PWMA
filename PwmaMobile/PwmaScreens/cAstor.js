// TODO add link to kg/cmm/cmm-mod-kg04-01/State and possibly also to fermi/pdumapper/1

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PwmaTree from '../PwmaComponents/PwmaTree';
import PwmaMain from '../PwmaComponents/PwmaMain';

let domain = 'tango://srv-tango-srf.fcs.elettra.trieste.it:20000';

const treeData = [{
  title: 'Starter list',
  data: [],
}];
const componentList = [];
let componentNumber = 0;
let treeCounter = 0;
let stateCounter = 0;

function compareTitle(a, b) {
  if (a.title < b.title) return -1;
  if (a.title > b.title) return 1;
  return 0;
}

export default class cAstor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      treeData: [
      ],
    };
    this.onItemClicked = this.onItemClicked.bind(this);
    this.wsListener = this.wsListener.bind(this);
  }
  componentDidMount() {
    console.debug('cAstor componentDidMount(), props: ', this.props);
    // if tree is reloaded a second time default green for level 1 nodes
    treeData[0].data.length = 0;
    componentList = [];
    // this.setState({ treeData: treeData });
    if (this.props.globalParams.page) domain = this.props.globalParams.page;
    this.ws = this.props.globalParams.initWs();
    if (this.ws) {
      if (!this.ws.readyState) {
        this.ws.onopen = () => {
          this.subscribeVar(domain);
        };
      } else {
        // this.ws.send(`{"data":{"type":"get_instance_name_list","value":"Starter"},"event":"${domain}"}`);
        this.subscribeVar(domain);
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      const that = this;
      Object.keys(componentList).map((obj) => {
        console.log(`cAstor, unsubscribe: ${obj}/state`);
        // that.ws.send(`{"data":{"type":"unsubscribe"},"event":"${obj}/state"}`);
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
  }
  subscribeVar(domain) {
    console.log('subscribeVar()', domain);
    fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/*`)
    .then(responseJson => responseJson.json())
    .then((response) => {
      componentList = response;
      console.log('componentList', componentList);
      componentNumber = response.length;
      for (let j=0; j<response.length; j++) {
        fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/${response[j]}/HostCollection`)
        .then(responseJson => responseJson.json())
        .then((hostCollection) => {
          console.log('HostCollection', j, response[j], hostCollection);
          componentList[response[j]] = {hostCollection};
          this.configProperties(response[j], hostCollection);
        })
        .catch((err) => {
          console.log('retry HostCollection', j, response[j]);
          fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/${response[j]}/HostCollection`)
          .then(responseJson => responseJson.json())
          .then((hostCollection) => {
            // console.log('HostCollection', j, response[j], hostCollection);
            componentList[response[j]] = {hostCollection};
            this.configProperties(response[j], hostCollection);
          })
          .catch((err) => {
            console.log(`fetch ${this.props.globalParams.server}/cs/${domain}/tango/admin/${response[j]}/HostCollection`);
            console.log('.catch((err) HostCollection', j, response[j], err);
          });
        });
      }
    })
    .catch((err) => {
      console.log('.catch(err)', err);
      return false;
    });
  }
  onItemClicked(node) {
    if (typeof node.data === 'object' && node.data.length === 0) {
      const n = node;
      n.component = 'Phonebook';
      // console.debug('onItemClicked, n:', this.props);
      const { navigate } = this.props.globalParams.navigation;
      navigate('SubPage', { title: `cAstor ${node.title}`, component: 'cAstorDevice', device: node.title, domain });
    }
  }

  configStarters(starterList) {
    const that = this;
    // console.debug(`configStarters(), typeof starterList: `, typeof starterList);
    if (typeof starterList !== 'object' || starterList === null) return null;
    componentNumber = starterList.length;
    starterList.map((obj) => {
      // that.ws.send(`{"data":{"type":"properties"},"event":"${domain}/tango/admin/${obj}"}`);
      return null;
    });
    return null;
  }
  configProperties(event, value) {
    // console.debug(`configProperties(), `, event);
    const name = event;
    treeCounter += 1;
    let touched = false;
    for (let i = 0; i < treeData[0].data.length; i += 1) {
      console.log('title VS value', treeData[0].data[i].title[0], value[0]);
      if (treeData[0].data[i].title === value[0]) {
        touched = true;
        treeData[0].data[i].data.push({ title: name, data: [], color: 'gray' });
        treeData[0].data[i].data.sort(compareTitle);
      }
    }
    if (!touched) {
      treeData[0].data.push({ title: value[0], color: 'gray', data: [{ title: name, data: [], color: 'gray' }] });
      treeData[0].data.sort(compareTitle);
    }
    componentList[event] = {
      name, hostCollection: value,
    };
    // this.ws.send(`{"data":{"type":"subscribe"},"event":"${event}/state"}`);
    if (treeCounter >= componentNumber) {
      treeCounter = 0;
      this.setState({ treeData });
    }
    if (this.props.globalParams.page) domain = this.props.globalParams.page;
    fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/${name}/State`)
    .then(responseJson => {console.log(name, responseJson.status); return responseJson.json();})
    .then((state) => {
      this.configState(`${domain}//tango/admin/${name}/state`, state);
    })
    .catch((err) => {
      console.log('retry', name, err);
      fetch(`${this.props.globalParams.server}/cs/${domain}/tango/admin/${name}/State`)
      .then(responseJson => responseJson.json())
      .then((state) => {
        this.configState(`${domain}//tango/admin/${name}/state`, state);
      })
      .catch((err) => {
        console.log(`fetch ${this.props.globalParams.server}/cs/${domain}/tango/admin/${name}/State`);
        console.log('.catch((err)', name, err);
      });
    });
  }
  configState(event, data) {
    const name = event.toLowerCase().split('/');
    if (name[7] === 'state') {
      const device = name[6];
      // console.log(device, componentList);
      if (componentList[device]) {
        const hostCollection = componentList[device].hostCollection[0];
        let color = 'gray';
        if (data.value === 'ON' || data.value === [0]) color = 'green';
        else if (typeof (data.value) === 'object' && data.value && Object.keys(data.value)[0] === '0') color = 'green';
        else if (data.value === 'ALARM') color = 'orange';
        else if (data.value === 'MOVING') color = 'blue';
        else if (data.error !== '') color = 'red';
        // console.log(color);
        for (let i = 0; i < treeData[0].data.length; i += 1) {
          console.log('title', treeData[0].data[i].title, hostCollection, device);
          if (treeData[0].data[i].title === hostCollection) {
            let summaryColor = 'gray';
            for (let j = 0; j < treeData[0].data[i].data.length; j += 1) {
              if (treeData[0].data[i].data[j].title === device) {
                treeData[0].data[i].data[j].color = color;
              }
              if (treeData[0].data[i].data[j].color === 'blue') summaryColor = 'blue';
              if (treeData[0].data[i].data[j].color === 'red' && summaryColor !== 'blue') summaryColor = 'red';
              if (treeData[0].data[i].data[j].color === 'orange' && summaryColor !== 'blue' && summaryColor !== 'red') summaryColor = 'orange';
              if (treeData[0].data[i].data[j].color === 'green' && summaryColor !== 'blue' && summaryColor !== 'red' && summaryColor !== 'orange') summaryColor = 'green';
            }
            treeData[0].data[i].color = summaryColor;
            stateCounter += 1;
          }
        }
        if (stateCounter == Math.round(componentNumber/2) || stateCounter > componentNumber-5) {
          // console.log('treeData', treeData);
          // stateCounter = 0;
          this.setState({ treeData });
        }
      }
    }
  }

  wsListener(event) {
    const eventData = JSON.parse(event.data);
    console.debug('Message from server', eventData);
    if (eventData.data !== undefined) {
      if (eventData.data.type === 'get_instance_name_list') {
        this.configStarters(eventData.data.value);
      } else if (eventData.data.type === 'properties') {
        this.configProperties(eventData.event, eventData.data.value);
      } else {
        this.configState(eventData.event, eventData.data);
      }
    }
  }

  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams}>
        <PwmaTree data={treeData} onItemClicked={this.onItemClicked} />
      </PwmaMain>
    );
  }

}

cAstor.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
