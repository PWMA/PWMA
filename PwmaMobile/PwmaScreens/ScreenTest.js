import React, { Component } from 'react';

import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaLabel from '../PwmaComponents/PwmaLabel';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaInput from '../PwmaComponents/PwmaInput';

function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}

export default class ScreenTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/kg05/mod/timer_kg05.02/timetogo': 'N.A.',
      },
      error: {},
      dateTime: '',
      gotoValue: 0,
    };
    this.ws = '';
    this.wsListener = this.wsListener.bind(this);
    this.readVar = this.readVar.bind(this);
    this.saveValue = this.saveValue.bind(this);
    this.gotoValue = this.gotoValue.bind(this);
  }
  componentDidMount() {
    console.log('componentDidMount()');
    const that = this;
    this.ws = this.props.initWs();
    if (this.ws) {
      if (!this.ws.readyState) {
        this.ws.onopen = () => {
          this.subscribeVar();
        };
      } else {
        this.subscribeVar();
      }
      this.ws.addEventListener('message', this.wsListener);
      this.ws.onerror = (e) => {
        // an error occurred
        console.log(`onerror: ${e.message}`);
      };
      this.ws.onclose = (e) => {
        // connection closed
        console.log(`onclose: ${e.code}, ${e.reason}`);
      };
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      const that = this;
      Object.keys(this.state.value).map((obj) => {
        console.log(`unsubscribe ${obj}`);
        that.ws.send(`{"data":{"type":"unsubscribe"},"event":"${obj}"}`);
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  subscribeVar() {
    const that = this;
    Object.keys(this.state.value).map((obj) => {
      console.log(`subscribe: ${obj}`);
      that.ws.send(`{"data":{"type":"subscribe"},"event":"${obj}"}`);
      return false;
    });
  }
  wsListener(event) {
    console.log('ScreenTest::wsListener(), event.data', event.data);
    const value = this.state.value;
    const error = this.state.error;
    const eventData = JSON.parse(event.data);
    value[eventData.event] = eventData.data.error !== undefined ? 'Error' : eventData.data.value;
    error[eventData.event] = eventData.data.error !== undefined ? eventData.data.error : '';
    console.log(value);
    this.setState({ value, error, dateTime: getDate() });
  }
  readVar(src) {
    const lvar = src.toLowerCase();
    console.log('ScreenTest::readVar(), lvar, this.state.value[lvar]', lvar, this.state.value[lvar]);
    console.log('ScreenTest::readVar()', { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${getDate()}` });
    return { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${getDate()}` };
  }

  saveValue(gotoValue) {
     this.setState({ gotoValue });
  }
  gotoValue() {
    this.ws.send(`{"data":{"type":"execute"},"event":"tango://srv-tango-srf.fcs.elettra.trieste.it:20000/kg05/mod/timer_kg05.02/stop"}`);
    this.ws.send(`{"data":{"type":"execute","value":"${this.state.gotoValue}"},"event":"tango://srv-tango-srf.fcs.elettra.trieste.it:20000/kg05/mod/timer_kg05.02/start"}`);
  }

  render() {
   return (
      <PwmaMain>
        <PwmaLabel text="Alarm Starter" />
        <PwmaScalar label="Time to go" padderFlex={1.4} paddingTop={15} value={this.readVar(`tango://srv-tango-srf.fcs.elettra.trieste.it:20000/kg05/mod/timer_kg05.02/timetogo`)}/>
        <PwmaInput label="Go To Value" maxLength={4} keyboardType="numeric" text="Apply" padderFlex={0.65} paddingTop={15} onChangeText={text => this.saveValue(text)} callback={this.gotoValue} />
      </PwmaMain>
    );
  }
}
