/* eslint-disable react/no-array-index-key */
import React, {
  Component,
} from 'react';
import PropTypes from 'prop-types';
import {
  ART,
  Dimensions,
  LayoutAnimation,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import Morph from 'art/morph/path';

import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import * as d3Array from 'd3-array';

const d3 = {
  scale,
  shape,
};

const {
  Group,
  Shape,
  Surface,
} = ART;

const PaddingSize = 20;
const TickWidth = PaddingSize * 2;
const AnimationDurationMs = 500;

const dimensionWindow = Dimensions.get('window');

export default class PwmaChart extends Component {
  static propTypes = {
    /* eslint-disable react/no-unused-prop-types */
    data: PropTypes.array.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    xAccessor: PropTypes.func.isRequired,
    yAccessor: PropTypes.func.isRequired,
    update: PropTypes.bool,
    xScaleType: PropTypes.oneOf(['time', 'linear']),
    fill: PropTypes.bool,
    max: PropTypes.oneOfType([
      'auto',
      PropTypes.number,
    ]),
    min: PropTypes.oneOfType([
      'auto',
      PropTypes.number,
    ]),
    color: PropTypes.string,
    title: PropTypes.string,
    /* eslint-enable */
  }

  static defaultProps = {
    width: Math.round(dimensionWindow.width * 0.9),
    height: Math.round(dimensionWindow.height * 0.4),
    xScaleType: 'linear',
    update: true,
    fill: false,
    max: 'auto',
    min: 'auto',
    color: '#090',
    title: 'Chart',
  };

  state = {
    graphWidth: 0,
    graphHeight: 0,
    linePath: '',
    yMin: 0,
    yMax: 0,
  };

  componentWillMount() {
    this.computeNextState(this.props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    // the props used by PwmaChart are quite complicated
    // this implies that the render() method is always fired
    // even if props are not changed.
    // In order to avoid this behaviour there is a specific
    // prop (update) who fires (or not) the render() method
    return nextProps.update;
  }

  // TODO replace with getDerivedStateFromProps()
  // see https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html
  componentWillReceiveProps(nextProps) {
    this.computeNextState(nextProps);
  }

  createScaleX(start, end, width) {
    return this.props.xScaleType==='linear' ?
      d3.scale.scaleLinear()
      .domain([start, end])
      .range([0, width]) :
      d3.scale.scaleTime()
      .domain([start, end])
      .range([0, width]);
  }
  createScaleY(minY, maxY, height) {
    return d3.scale.scaleLinear()
      .domain([minY, maxY]).nice()
      // We invert our range so it outputs using the axis that React uses.
      .range([height, 0]);
  }
  createLineGraph({
    data,
    xAccessor,
    yAccessor,
    width,
    height,
  }) {
    const lastDatum = data[data.length - 1];
    const scaleX = this.createScaleX(
      xAccessor ? xAccessor(data[0]) : 0,
      xAccessor ? xAccessor(lastDatum) : 0,
      width
    );
    // Collect all y values.
    const allYValues = data.reduce((all, datum) => {
      all.push(yAccessor(datum));
      return all;
    }, []);
    // Get the min and max y value.
    const extentY = d3Array.extent(allYValues);
    const yMin = this.props.min === 'auto' ? extentY[0] : this.props.min;
    const yMax = this.props.max === 'auto' ? extentY[1] : this.props.max;

    const scaleY = this.createScaleY(yMin, yMax, height);

    const lineShape = this.props.fill === false ?
      d3.shape.line()
        .x(d => scaleX(xAccessor(d)))
        .y(d => scaleY(yAccessor(d)))
      : d3.shape.area()
        .defined(function(d) { return d; })
        .x(d => scaleX(xAccessor(d)))
        .y1(d => scaleY(yAccessor(d)))
        .y0(scaleY.invert(yMin))
        .curve(d3.shape.curveNatural);

    return {
      data,
      yMin,
      yMax,
      scale: {
        x: scaleX,
        y: scaleY,
      },
      path: lineShape(data),
      ticks: data ? data.map((datum) => {
        const time = xAccessor(datum);
        const value = yAccessor(datum);
        return {
          x: scaleX(time),
          y: scaleY(value),
          datum,
        };
      }) : {},
    };
  }

  computeNextState(nextProps) {
    const {
      data,
      width,
      height,
      xAccessor,
      yAccessor,
    } = nextProps;

    const fullPaddingSize = PaddingSize * 2;
    const graphWidth = width - fullPaddingSize;
    const graphHeight = height - fullPaddingSize;
    const newData = [];
    data ? data.map((value, index) => {
      newData[index] = [index, value];
    }) : undefined;

    const lineGraph = this.createLineGraph({
      data: this.props.xScaleType==='linear' ? newData: data,
      xAccessor,
      yAccessor,
      width: graphWidth,
      height: graphHeight,
    });
    this.setState({
      graphWidth,
      graphHeight,
      linePath: lineGraph.path,
      ticks: lineGraph.ticks,
      scale: lineGraph.scale,
      yMin: lineGraph.yMin,
      yMax: lineGraph.yMax,
    });

  }

  render() {
    console.log('PwmaChart::render(), start');
    const {
      yAccessor,
    } = this.props;

    const {
      graphWidth,
      graphHeight,
      linePath,
      ticks,
      scale,
    } = this.state;

    const {
      x: scaleX,
      y: scaleY,
    } = scale;

    // https://github.com/d3/d3-scale#time-scales // %b %d
    const tickXFormat = this.props.xScaleType==='time' ? scaleX.tickFormat(null, '%I:%M') : undefined;

    return (
      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center', height: this.props.height+20}}>
        <View style={styles.container}>
          <Text style={{ position: 'absolute', left: 20, top: -20, textAlign: 'center' }}>{this.props.title}</Text>
          <Surface width={graphWidth} height={graphHeight}>
            <Group x={0} y={0}>
              <Shape
                d={linePath}
                fill={this.props.fill ? this.props.color : undefined}
                stroke={this.props.color}
                strokeWidth={3}
              />
            </Group>
          </Surface>
          <View key={'ticksX'}>
            {scaleX.ticks(4).map((tick, index) => {
              const tickStyles = {};
              tickStyles.width = TickWidth;
              tickStyles.left = scaleX(tick) - (TickWidth / 2);
              return (<Text key={index} style={[styles.tickLabelX, tickStyles]}>
                {this.props.xScaleType!=='time' ? tick : tickXFormat(tick)}
              </Text>);
            })
            }
          </View>
          <View key={'ticksY'} style={styles.ticksYContainer}>
            {scaleY.ticks(4).map((tick, index) => {
              const tickStyles = {width: TickWidth-2, top: scaleY(tick) - 10, left: -TickWidth, position: 'absolute', backgroundColor: 'transparent', textAlign: 'right'};
              return (<Text key={`Y${index}`} style={tickStyles}>{tick.toPrecision(tick < 10000 ? 4 : 2)}</Text>);
            })
            }
          </View>
          <View key={'ticksYDot'} style={styles.ticksYContainer}>
            <View key='xAxis' style={{position: 'absolute', width: this.props.width-20, left: 0, top: this.props.height-40, height: 2, borderRadius: 100, backgroundColor: '#000000'}} />
            <View key='yAxis' style={{position: 'absolute', width: 2, left: 0, top: 0, height: this.props.height-38, borderRadius: 100, backgroundColor: '#000000'}} />
          </View>
        {console.log('PwmaChart::render(), stop')}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },

  tickLabelX: {
    position: 'absolute',
    bottom: -18,
    fontSize: 12,
    textAlign: 'center',
  },

  ticksYContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
  },

  tickLabelY: {
    position: 'absolute',
    left: 0,
    backgroundColor: 'transparent',
  },

  tickLabelYText: {
    fontSize: 12,
    textAlign: 'right',
  },

  ticksYDot: {
    position: 'absolute',
    width: 2,
    height: 2,
    borderRadius: 100,
  },
});
