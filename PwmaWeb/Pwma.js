import React from 'react';
import { render } from 'react-dom';

import PwmaDesigner from './PwmaComponents/designer/PwmaDesigner';
import PwmaMain from './PwmaComponents/PwmaMain';
import PwmaArrowTest from './PwmaComponents/PwmaArrow';
import PwmaTree from './PwmaComponents/PwmaTree';

import ElettraStatus from './PwmaScreens/ElettraStatus';
import FermiStatus from './PwmaScreens/FermiStatus';
import FermiPressures from './PwmaScreens/FermiPressures';
import OperatorShift from './PwmaScreens/OperatorShift';
import cAstor from './PwmaScreens/cAstor';

import {
  HashRouter,
  BrowserRouter,
  RouteComponent,
  Switch,
  Route,
  Link
} from 'react-router-dom';

let ws = null;
let wsStart = 0;
const wsServer = 'ws://pwma.elettra.eu:10080/updates';
const restServer = 'https://pwma.elettra.eu:10080/v1';
function initWs() {
  if (ws === null) {
    console.log('initWs(), ws===null');
    ws = new WebSocket(wsServer);
    wsStart = (new Date).getTime();
  }
  ws.onerror = (e) => {
    // an error occurred
    console.log(`onerror() ${e.message}`);
    if (alarmCallback) alarmCallback('ws');
  };
  ws.onclose = (e) => {
    // connection closed
    console.log(`onclose: ${e[0]}`);
    ws = new WebSocket(wsServer);
  };
  return ws;
}

const treeData = [{
  title: 'available sreens',
  data: [
    {
      title: 'FERMI',
      data: [
        {
          title: 'FERMI Status',
          component: 'FermiStatus',
        },
        {
          title: 'FERMI Pressures',
          component: 'FermiPressures'
        },
        {
          title: 'FERMI cAstor',
          component: 'cAstor',
          param: 'tango://srv-tango-srf.fcs.elettra.trieste.it:20000',
        },
        {
          title: 'Other screens',
          data: [
            {
              title: 'N.A.',
            },
          ],
        },
      ],
    },
    {
      title: 'Elettra',
      data: [
        {
          title: 'Elettra Status',
          component: 'ElettraStatus',
        },
        {
          title: 'Elettra cAstor',
          component: 'cAstor',
          param: 'tango://tom.ecs.elettra.trieste.it:20000',
        },
        {
          title: 'Other screens',
          data: [
            {
              title: 'N.A',
            },
          ],
        },
      ],
    },
    {
      title: 'Common Services',
      data: [
        {
          title: 'Operator Shifts',
          component: 'OperatorShift',
        },
      ],
    },
    {
      title: 'Designer',
      component: 'designer',
    },
  ],
}];

const Home = () => (
  <div>
    <h2><img height='24' src="/img/logo.png"></img> PWMA Web</h2>
  </div>
)
  
const Tree = () => (
  <div>
    <PwmaTree data={treeData} />
  </div>
);

// createClass is deprecated
var ElettraWrapper = React.createClass({
  render: function () {
    return (
        <ElettraStatus globalParams={{initWs: initWs, server: restServer}} />
    );
  }
});
var FermiWrapper = React.createClass({
  render: function () {
    return (
        <FermiStatus globalParams={{initWs: initWs, server: restServer}} />
    );
  }
});
var FermiPressuresWrapper = React.createClass({
  render: function () {
    return (
        <FermiPressures globalParams={{initWs: initWs, server: restServer}} />
    );
  }
});

const Main = () => (
	<BrowserRouter>
		<Switch>
			<Route exact path='/' component={Tree}/>
			<Route path='/FermiStatus' component={FermiWrapper}/>
			<Route path='/FermiPressures' component={FermiPressuresWrapper}/>
			<Route path='/ElettraStatus' component={ElettraWrapper} />
			<Route path='/OperatorShift' component={OperatorShift}/>
			<Route path='/designer' component={PwmaDesigner}/>
		</Switch>
	</BrowserRouter>
)

// The Header creates links that can be used to navigate
// between routes.
const Header = () => (
	<header>
		<nav>
			<Home />
		</nav>
	</header>
)
/*
// this component will be rendered by our <___Router>
const App = () => {
return (
	<div>
		<Header />
		{console.log('App, props', this.props)}
		<Main />
		<div style={{margin: "20px 0 0 20px"}}>
			<a href="/formulaeditor/formula_editor.html">create custom alarm</a> | <a href="http://pwma-dev.elettra.trieste.it/apk/">download PWMA Mobile</a>
		</div>
	</div>
)}
*/
class App extends React.Component {
render() {
return (
	<div>
		<Header />
		{console.log('App::props', this.props)}
		<Main pippo={this.props.pippo} />
		<div style={{margin: "20px 0 0 20px"}}>
			<a href="./formulaeditor/formula_editor.html">create custom alarm</a> | <a href="./apk/">download PWMA Mobile</a>
		</div>
	</div>
)}
}

render((
	<App />
), document.getElementById('app'))
