import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaLedArray from '../PwmaComponents/PwmaLedArray';

const domain = 'tango://tom.ecs.elettra.trieste.it:20000';
const fdomain = 'tango://srv-tango-srf.fcs.elettra.trieste.it:20000';
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}

export default class pssHardware extends Component {
  constructor(props) {
    super(props);
    const BoosterBoard = [];
    const SrBoard = [];
    const LinacBoard = [];
    const UhBoard = [];
    for (let bit = 0; bit < 28; bit += 1) {
      BoosterBoard[bit] = { data: [], alt: 'N.A.' };
    }
    for (let bit = 0; bit < 6; bit += 1) {
      SrBoard[bit] = { data: [], alt: 'N.A.' };
    }
    for (let bit = 0; bit < 35; bit += 1) {
      LinacBoard[bit] = { data: [], alt: 'N.A.' };
    }
    for (let bit = 0; bit < 17; bit += 1) {
      UhBoard[bit] = { data: [], alt: 'N.A.' };
    }
    this.state = {
      value: {
        'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/disabled_boards': [],
        'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/resettable_boards': [],
        'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/disabled_boards': [],
        'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/resettable_boards': [],
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_boards_disabled': [],
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_boards_resettable': [],
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_boards_disabled': [],
        'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_boards_resettable': [],
      },
      error: {},
      BoosterBoard,
      SrBoard,
      LinacBoard,
      UhBoard,
      informationSystem: 'N.A.',
      dateTime: '',
    };
    this.ws = '';
    this.onWriteRequest = this.onWriteRequest.bind(this);
    this.onItemClicked = this.onItemClicked.bind(this);
    this.wsListener = this.wsListener.bind(this);
  }
  componentDidMount() {
    /* global fetch */
    fetch('http://pwma.elettra.eu/misc/information_system.php')
    .then((response) => {
      console.debug('response: ', response);
      return response.text();
    }).then((mytext) => {
      // console.debug(mytext);
      this.setState({ informationSystem: mytext.trim(), dateTime: getDate() });
    }).catch((error) => {
      console.debug(`There has been a problem with your fetch operation: ${error.message}`);
    });
    this.ws = this.props.globalParams.initWs();
    if (this.ws) {
      if (!this.ws.readyState) {
        this.ws.onopen = () => {
          this.subscribeVar();
        };
      } else {
        this.subscribeVar();
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      /*
      const that = this;
      Object.keys(this.state.value).map((obj) => {
        that.ws.send(`{"data":{"type":"unsubscribe"},"event":"${obj}"}`);
        return false;
      });
      */
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  onItemClicked(labels) {
    if (this.ws) {
      const that = this;
      Object.keys(this.state.value).map((obj) => {
        that.ws.send(`{"data":{"type":"unsubscribe"},"event":"${obj}"}`);
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }

    const { navigate } = this.props.globalParams.navigation;
    let id = labels.substring(1);
    const boardname = id.split(' ').slice(0, 1).join(' ');
    let device = `acsboardselettra=acsboosterboard${boardname}`;
    if (labels.slice(-1) === ';') {
      id = labels.slice(1, -1);
      device = `acsboardselettra=acsboard${boardname}`;
    }
    if (this.props.globalParams.page === 'fermi') {
      device = `acsboardsfermi=acsboard${boardname}`;
      if (labels.slice(-1) === ';') {
        device = `acsboardsfermi=acsuhboard${boardname}`;
      }
    }
    navigate('SubPage', { title: `board ${id}`, component: 'PwmaLoader', param: device });
  }
  onWriteRequest(args) {
    this.ws.send(`{"data":{"type":"execute","value": ${args.value}},"event":"${args.target}"}`);
  }
  wsListener(event) {
    const value = this.state.value;
    const error = this.state.error;
    const eventData = JSON.parse(event.data);
    if (eventData.data !== undefined) {
      value[eventData.event] = eventData.data.error ? 'Error' : eventData.data.value;
      error[eventData.event] = eventData.data.error ? eventData.data.error : '';
      const disabled = value[`${domain}/booster/accessi/plc_accessi/disabled_boards`];
      const resettable = value[`${domain}/booster/accessi/plc_accessi/resettable_boards`];
      const SrDisabled = value[`${domain}/sr/accessi/pss/disabled_boards`];
      const SrResettable = value[`${domain}/sr/accessi/pss/resettable_boards`];
      const LinacDisabled = value[`${fdomain}/f/access_control/safety/linac_boards_disabled`];
      const LinacResettable = value[`${fdomain}/f/access_control/safety/linac_boards_resettable`];
      const UhDisabled = value[`${fdomain}/f/access_control/safety/undulator_boards_disabled`];
      const UhResettable = value[`${fdomain}/f/access_control/safety/undulator_boards_resettable`];
      let bit = 0;
      const BoosterBoard = [];
      const SrBoard = [];
      const LinacBoard = [];
      const UhBoard = [];
      for (bit = 0; bit < 28; bit += 1) {
        if (Object.prototype.toString.call(disabled) !== '[object Array]' || Object.prototype.toString.call(resettable) !== '[object Array]' || disabled.length < bit || resettable.length < bit) BoosterBoard[bit] = { data: [], alt: `err: board ${bit}` };
        BoosterBoard[bit] = {
          data: [disabled[bit], resettable[bit]],
          alt: `board ${bit}`,
        };
      }
      for (bit = 0; bit < 6; bit += 1) {
        if (Object.prototype.toString.call(SrDisabled) !== '[object Array]' || Object.prototype.toString.call(SrResettable) !== '[object Array]' || SrDisabled.length < bit || SrResettable.length < bit) SrBoard[bit] = { data: [], alt: `err: board ${bit}` };
        SrBoard[bit] = {
          data: [SrDisabled[bit], SrResettable[bit]],
          alt: `board ${bit}`,
        };
      }
      for (bit = 0; bit < 35; bit += 1) {
        if (Object.prototype.toString.call(LinacDisabled) !== '[object Array]' || Object.prototype.toString.call(LinacResettable) !== '[object Array]' || LinacDisabled.length < bit || LinacResettable.length < bit) LinacBoard[bit] = { data: [], alt: `err: board ${bit}` };
        LinacBoard[bit] = {
          data: [LinacDisabled[bit], LinacResettable[bit]],
          alt: `board ${bit}`,
        };
      }
      for (bit = 0; bit < 17; bit += 1) {
        if (Object.prototype.toString.call(UhDisabled) !== '[object Array]' || Object.prototype.toString.call(UhResettable) !== '[object Array]' || UhDisabled.length < bit || UhResettable.length < bit) UhBoard[bit] = { data: [], alt: `err: board ${bit}` };
        UhBoard[bit] = {
          data: [UhDisabled[bit], UhResettable[bit]],
          alt: `board ${bit}`,
        };
      }
      this.setState({
        value, error, BoosterBoard, SrBoard, LinacBoard, UhBoard, dateTime: getDate(),
      });
    }
  }
  subscribeVar() {
    const that = this;
    Object.keys(this.state.value).map((obj) => {
      // that.ws.send(`{"data":{"type":"subscribe"},"event":"${obj}"}`);
      fetch(`${this.props.globalParams.server}/cs/${obj}`, {
        method: 'SUBSCRIBE',
      }).then((response) => {
        if (response.status>=300) {
          const value = this.state.value;
          const error = this.state.error;
          value[obj] = 'Error.';
          error[obj] = response._bodyText;
          this.setState({ value, error, });
          console.log('response', response.status, response._bodyText, obj);
        }
      })
      .catch((err) => {
        console.log('.catch((err)', err, obj);
        // console.error(`Error reading ${obj}\n${err}`);
        const value = this.state.value;
        const error = this.state.error;
        value[obj] = 'N.A.';
        error[obj] = err;
        /*
        this.setState({ value, error, });
        */
      });
      return false;
    });
  }
  render() {
    if (this.props.globalParams.page !== 'fermi') {
      return (
        <PwmaMain globalParams={this.props.globalParams} >
          <PwmaScalar label="Fault Resettable - Storage Ring" labelAlign="left" />
          <PwmaLedArray
            value={this.state.SrBoard[0]} labels=";008 RACK_A4 (porta A2);"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.SrBoard[1]} labels=";024 SPAC_A9.1 (porta A3);"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.SrBoard[2]} labels=";048 SPAC_A9.1 (porta A3);"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.SrBoard[3]} labels=";064 SPAC_A9.1 (porta A3);"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.SrBoard[4]} labels=";080 SPAC_A9.1 (porta A3);"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.SrBoard[5]} labels=";104 SPAC_CR_2;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaScalar label="Fault Resettable - Booster" labelAlign="left" />
          <PwmaLedArray
            value={this.state.BoosterBoard[0]} labels=";008 SPAC_BRG1.1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[1]} labels=";048 RACK_PSA1.1 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[2]} labels=";064 RACK_PSA1.1 Scheda 2"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[3]} labels=";080 RACK_PSA1.1 Scheda 3"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[4]} labels=";100 SPAC_CR"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[5]} labels=";112 SPAC_A9 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[6]} labels=";118 SPAC_A9 Scheda 2"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[27]} labels=";124 SPAC_A9 Scheda 3"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[7]} labels=";130 SPAC_A12 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[8]} labels=";136 SPAC_A12 Scheda 2"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[9]} labels=";142 SPAC_A12 Scheda 3"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[10]} labels=";148 SPAC_B7 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[11]} labels=";223 SPAC_B15 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[12]} labels=";229 SPAC_B13 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          {/* <PwmaLedArray
            value={this.state.BoosterBoard[13]} labels=";235"
            flexDirection="row" link={this.onItemClicked}
          /> */}
          <PwmaLedArray
            value={this.state.BoosterBoard[14]} labels=";247 SPAC_B15 Scheda 2"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[15]} labels=";253 SPAC_B15 Scheda 3"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[16]} labels=";259 SPAC_B15 Scheda 4"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[17]} labels=";265 SPAC_B15 Scheda 5"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[18]} labels=";278 SPAC_B20 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[19]} labels=";291 SPAC_B25 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[20]} labels=";304 SPAC_BSA1.1 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[21]} labels=";310 SPAC_BSA1.1 Scheda 2"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[22]} labels=";316 SPAC_BSA1.1 Scheda 3"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[23]} labels=";321 SPAC_BSA1.1 Scheda 4"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[24]} labels=";333 SPAC_BTS1 Scheda 1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[25]} labels=";329 SPAC_BTS1 Scheda 2"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.BoosterBoard[26]} labels=";345 SPAC_BTS1 Scheda 3"
            flexDirection="row" link={this.onItemClicked}
          />
        </PwmaMain>
      );
    }
    if (this.props.globalParams.page === 'fermi') {
      return (
        <PwmaMain globalParams={this.props.globalParams} >
          <PwmaScalar label="Fault Resettable - Undulator Hall" labelAlign="left" />
          <PwmaLedArray
            value={this.state.UhBoard[6]} labels=";008 plc-acc-uh-01;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[15]} labels=";050 plc-acc-uh-01;" flexDirection="row"
          />
          <PwmaLedArray
            value={this.state.UhBoard[16]} labels=";024 plc-acc-uh-01;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[8]} labels=";126 SPAC UH01;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[9]} labels=";132 SPAC UH01;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[12]} labels=";178 SPAC UH02;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[13]} labels=";184 SPAC UH02;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[0]} labels=";204 SPAC UH03;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[2]} labels=";248 SPAC UH03;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[3]} labels=";214 SPAC UH03;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[10]} labels=";152 SPAC UH04;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[11]} labels=";158 SPAC UH04;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[7]} labels=";100 SPAC UH05;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[14]} labels=";106 SPAC UH05;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[1]} labels=";230 SPAC ESA01;" flexDirection="row"
          />
          <PwmaLedArray
            value={this.state.UhBoard[5]} labels=";288 SPAC ESA01;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.UhBoard[4]} labels=";270 SPAC BL1;"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaScalar label="Fault Resettable - Linac" labelAlign="left" />
          <PwmaLedArray
            value={this.state.LinacBoard[6]} labels=";008 plc-acc-linac-01"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[0]} labels=";040 SPAC CR"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[7]} labels=";100 SPAC LT01"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[31]} labels=";106 SPAC LT01"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[8]} labels=";126 SPAC LT02"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[9]} labels=";132 SPAC LT02"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[10]} labels=";152 SPAC LT03"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[11]} labels=";158 SPAC LT03"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[12]} labels=";178 SPAC LT04"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[13]} labels=";184 SPAC LT04"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[14]} labels=";190 SPAC LT04"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[15]} labels=";196 SPAC LT04"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[16]} labels=";202 SPAC LT04"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[1]} labels=";227 SPAC LT04"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[34]} labels=";232 SPAC LT04"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[30]} labels=";058 SPAC KG02.1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[32]} labels=";068 SPAC KG02.1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[33]} labels=";078 SPAC KG02.1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[29]} labels=";088 SPAC KG02.1"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[2]} labels=";216 SPAC KG03"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[3]} labels=";222 SPAC KG03"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[4]} labels=";246 SPAC KG05"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[5]} labels=";240 SPAC KG05"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[17]} labels=";264 SPAC KG07"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[18]} labels=";252 SPAC KG07"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[19]} labels=";258 SPAC KG07"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[20]} labels=";269 SPAC KG07"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[21]} labels=";297 SPAC KG09"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[22]} labels=";286 SPAC KG09"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[23]} labels=";280 SPAC KG09"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[24]} labels=";292 SPAC KG09"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[25]} labels=";325 SPAC KG14"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[26]} labels=";308 SPAC KG14"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[27]} labels=";314 SPAC KG14"
            flexDirection="row" link={this.onItemClicked}
          />
          <PwmaLedArray
            value={this.state.LinacBoard[28]} labels=";320 SPAC KG14"
            flexDirection="row" link={this.onItemClicked}
          />
        </PwmaMain>
      );
    }
    return '';
  }
}

pssHardware.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
