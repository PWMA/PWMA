import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaScalar from '../PwmaComponents/PwmaScalar';
import PwmaContainer from '../PwmaComponents/PwmaContainer';
import PwmaChart from '../PwmaComponents/PwmaChart';

const domain = 'tango://tom.ecs.elettra.trieste.it:20000';
const xAccessor = d => new Date(d[0]);
const yAccessor = d => d[1];
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}
function truncateString(str, len) {
  if (str.length > len) return `${str.substring(0, len - 3)}...`;
  return str;
}

export default class ElettraStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {},
      error: {},
      informationSystem: 'N.A.',
      dateTime: '',
      graphData: [],
      graphReady: false,
    };
    this.ws = '';
    this.readVar = this.readVar.bind(this);
    this.wsListener = this.wsListener.bind(this);
  }
  componentDidMount() {
    console.log('ElettraStatus::componentDidMount()');
    const src = "http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%208%20hours&ts=00046&no_pretimer&no_posttimer&decimation_samples=100";
    fetch(src)
    .then(response => response.json())
    .then((responseJson) => {
      const graphData = responseJson.ts[0].data;
      graphData && this.setState({graphData, graphReady: true});
    });
    /* global fetch */
    fetch('http://pwma-dev.elettra.trieste.it/misc/information_system.php')
    .then((response) => {
      console.debug('response: ', response);
      return response.text();
    }).then((mytext) => {
      // console.debug(mytext);
      this.setState({ informationSystem: mytext.trim(), dateTime: getDate() });
    }).catch((error) => {
      console.debug(`There has been a problem with your fetch operation: ${error.message}`);
    });
    this.ws = this.props.globalParams.initWs();
    console.log(this.ws===null);
    if (this.ws) {
      if (!this.ws.readyState) {
        console.log('readyState', this.ws.readyState);
        this.ws.onopen = () => {
          this.subscribeVar();
        };
      } else {
        this.subscribeVar();
      }
      this.ws.addEventListener('message', this.wsListener);
    }
  }
  componentWillUnmount() {
    if (this.ws) {
      Object.keys(this.state.value).map((obj) => {
        console.log(`unsubscribe ${obj}`);
        fetch(`${this.props.globalParams.server}/cs/${obj}`, {
          method: 'UNSUBSCRIBE',
        })
        .then((response) => {
          if (response.status>=300) {
            console.log('response', response.status, response._bodyText, obj);
          }
        })
        .catch((err) => {
          console.log('.catch((err)', err, obj);
        });
        return false;
      });
      this.ws.removeEventListener('message', this.wsListener);
    }
  }

  wsListener(event) {
    // console.log('Message from server', event.data);
    const value = this.state.value;
    const error = this.state.error;
    if (event.data === undefined) return;
    const eventData = JSON.parse(event.data);
    if (eventData.data === undefined || eventData.data.error === undefined || eventData.data.value === undefined) return;
    this.setState((prevState) => {
      prevState.value[eventData.event] = eventData.data.error !== "" ? 'Error' : eventData.data.value;
      prevState.error[eventData.event] = eventData.data.error !== "" ? eventData.data.error : '';
      return {value: prevState.value, error: prevState.error, dateTime: getDate()};
    });
  }
  subscribeVar() {
    console.log('subscribeVar()');
    let counter = 0;
    Object.keys(this.state.value).map((obj) => {
      console.log(`subscribe: ${this.props.globalParams.server}/cs/${obj}`);
      fetch(`${this.props.globalParams.server}/cs/${obj}`, {
        method: 'SUBSCRIBE',
      }).then((response) => {
        if (response.status>=300) {
          this.setState((prevState) => {
            prevState.value[obj] = 'Error.';
            prevState.error[obj] = response._bodyText;
            return {value: prevState.value, error: prevState.error, dateTime: getDate()};
          });
          console.log('response', response.status, response._bodyText, obj);
        }
        else if (response._bodyText !== undefined) {
          const eventData = JSON.parse(response._bodyText);
          if (eventData.data === undefined || eventData.data.error === undefined || eventData.data.value === undefined) return false;
          this.setState((prevState) => {
            prevState.value[obj] = eventData.data.error !== "" ? 'Error' : eventData.data.value;
            prevState.error[obj] = eventData.data.error !== "" ? eventData.data.error : '';
            return {value: prevState.value, error: prevState.error, dateTime: getDate()};
          });
        }
      })
      .catch((err) => {
        console.log('.catch((err)', err, obj);
        this.setState((prevState) => {
          prevState.value[obj] = 'N.A.';
          prevState.error[obj] = err;
          return {value: prevState.value, error: prevState.error, dateTime: getDate()};
        });
      });
      return false;
    });
  }
  readVar(src) {
    const lvar = src.toLowerCase();
    if (!this.state.value[lvar]) {
      this.state.value[lvar] = 'N.A.';
    }
    return { data: this.state.value[lvar], alt: `src: ${src}\nerror: ${this.state.error[lvar]}\nlastUpdate: ${getDate()}` };
  }
  render() {
    return (
      <PwmaMain globalParams={this.props.globalParams} >
        {this.state.graphReady &&
          <PwmaChart data={this.state.graphData} update={false} xAccessor={xAccessor} yAccessor={yAccessor} height={300} max={320} min={0} fill={true} xScaleType="time" title="SR Current (last 8 hours)" />
        }
        <PwmaContainer label="Status">
          <PwmaScalar label="Current" unit="[mA]" value={this.readVar(`${domain}/sr/DIAGNOSTICS/DCCT_S4/Current`)} />
          <PwmaScalar label="Energy" unit="[GeV]" value={this.readVar(`${domain}/sr/DIAGNOSTICS/DCCT_S4/Energy`)} />
          <PwmaScalar label="Lifetime" unit="[h]" value={this.readVar(`${domain}/sr/DIAGNOSTICS/DCCT_S4/LifetimeHours`)} />
          <PwmaScalar
            label="Info" valueWidth={308}
            value={{
              data: truncateString(this.state.informationSystem, 32),
              alt: this.state.informationSystem,
            }}
          />
          <PwmaScalar label="Update" valueWidth={250} value={{ data: this.state.dateTime }} />
        </PwmaContainer>
        <PwmaContainer label="Control room">
          <PwmaScalar label="Machine Info" value={this.readVar(`${domain}/elettra/status/userstatus/machinestatus`)} />
          <PwmaScalar label="Operation Mode" value={this.readVar(`${domain}/elettra/status/userstatus/operationmode`)} />
          <PwmaScalar label="Bunch Mode" value={this.readVar(`${domain}/elettra/status/userstatus/bunchmode`)} />
          <PwmaScalar label="Control Room" value={this.readVar(`${domain}/elettra/status/userstatus/controlroom`)} />
        </PwmaContainer>
        <PwmaContainer label="Last User dedicated injection">
          <PwmaScalar label="date" value={this.readVar(`${domain}/elettra/status/userstatus/lastudinjon`)} />
          <PwmaScalar label="time" value={this.readVar(`${domain}/elettra/status/userstatus/lastudinjtime`)} />
          <PwmaScalar label="Next injection on" value={{ data: 'N.A.', alt: 'Not available\nsrc: elettra/status/userstatus/nextudinjtime' }} />
        </PwmaContainer>
      </PwmaMain>
    );
  }
}

ElettraStatus.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
