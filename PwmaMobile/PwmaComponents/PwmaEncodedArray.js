import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
} from 'react-native';

export default class PwmaEncodedArray extends Component {
  constructor(props) {
    super(props);
    this.state = { value: { data: [], alt: '' } };
    this.globalStyle = this.globalStyle.bind(this);
    this.cellStyle = this.cellStyle.bind(this);
    this.cellValue = this.cellValue.bind(this);
  }

  globalStyle() {
    const flexDirection = this.props.flexDirection !== undefined ? this.props.flexDirection : 'row';
    const justifyContent = this.props.justifyContent !== undefined ? this.props.justifyContent : 'flex-start';
    const alignItems = this.props.alignItems !== undefined ? this.props.alignItems : 'center';
    // console.log(flexDirection);
    return {
      flex: 1,
      padding: 5,
      flexDirection,
      justifyContent,
      alignItems,
    };
  }

  cellStyle(index) {
    let v = (typeof this.props.codeMap) === 'object' && this.props.codeMap && this.props.codeMap[index] ? this.props.value.data[index] : undefined;
    if ((typeof this.props.codeMap[index]) !== 'object' || (typeof this.props.codeMap[index][v]) !== 'object') v = undefined;
    const backgroundColor = v !== undefined && this.props.codeMap[index][v].backgroundColor ? this.props.codeMap[index][v].backgroundColor : 'white';
    const color = v !== undefined && this.props.codeMap[index][v].color ? this.props.codeMap[index][v].color : 'black';
    const width = v !== undefined && this.props.codeMap[index][v].width ?
      this.props.codeMap[index][v].width : 60;
    return {
      backgroundColor,
      color,
      width,
      height: 30,
      borderWidth: 1,
      padding: 2,
      fontSize: 20,
      textAlign: 'center',
      fontWeight: 'bold',
    };
  }

  cellValue(index) {
    let v = (typeof this.props.codeMap) === 'object' && this.props.codeMap && this.props.codeMap[index] ? this.props.value.data[index] : undefined;
    if ((typeof this.props.codeMap[index]) !== 'object' || (typeof this.props.codeMap[index][v]) !== 'object') v = undefined;
    console.log(v);
    return v !== undefined && this.props.codeMap[index][v].value ? this.props.codeMap[index][v].value : 'N.A.';
  }

  render() {
    return (
      <View style={this.globalStyle()}>
        {Object.keys(this.props.codeMap).map(index => (
          <View key={`k${index}`} style={{ flex: 1, flexDirection: 'row' }}>
            <Text style={this.cellStyle(index)}>{this.cellValue(index)}</Text>
          </View>
        ))}
      </View>
    );
  }
}
PwmaEncodedArray.propTypes = {
  value: PropTypes.shape({
    data: PropTypes.array,
    alt: PropTypes.string,
  }),
  codeMap: PropTypes.object,
  flexDirection: PropTypes.string,
  justifyContent: PropTypes.string,
  alignItems: PropTypes.string,
};
PwmaEncodedArray.defaultProps = {
  value: { data: [], alt: '' },
  codeMap: {},
  flexDirection: 'row',
  justifyContent: 'flex-start',
  alignItems: 'center',
};
