# WARNING This project was moved to PUMA
[https://gitlab.elettra.eu/puma](https://gitlab.elettra.eu/puma)

# Platform for Web and Mobile Applications (PWMA)
Design of a platform suitable to implement web and mobile applications for Elettra Sincrotrone Trieste and test on a set of instances.
This project was started keeping in mind several previous experiences among which an excellent work done by a thesis worker in late 2016 based on Apache Cordova.

# Requirements
- multiplatform
- event driven 
- components
- all in one application

## Multiplatform
PWMA must support the main evergreen browsers and the main mobile app systems. 
It is acceptable that some old version of browsers and of mobile OS or neglectable market-share browsers are not supported.
It is almost not acceptable to need any configuration of browsers.
Mobile app should perform as much as possible like native app.

## Event driven
The client-server communication is not driven by a periodic refresh triggered by the client, but it is asynchronous on both sides. 
This should minimize the traffic on the web and minimize the latency. 
SSE (Server-sent events) with HTTP/2 (Hypertext Transfer Protocol) offers auto-reconnect, multiplexing on the same TCP/IP (Transmission Control Protocol /  Internet Protocol) connection and unidirectionality; 
this feature together with a REST interface allows a strategy of type [CQRS (Command Query Responsibility Segregation)](https://en.wikipedia.org/wiki/Command%E2%80%93query_separation#Command_query_responsibility_segregation); on the other hand [WebSockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API) are supported by more libraries, among which React Native.
The high efficiency provided by event driven data transfer is a stimulus for performing technology on client side.

The high efficiency provided by event driven data transfer is a stimulus for performant technology on client side.

## Components 
Components should facilitate automation of composition of a large number of pages, the composition of new pages should be eased by a designer.
WebComponents are a standard provided by [W3C](https://www.w3.org/wiki/WebComponents) (World Wide Web Consortium) and as such it will be natively supported by all evergreen browsers.
There are other implementation of components which should be considered as a tradeoff with high efficiency.

## All in one application
Our users asked explicitly for a unique mobile app as an interface for all tasks.
This request imply a way to open the screens of all tasks, the number of screens till now is about a few dozens, but in future it may be much larger. 
Our goal is to have the possibility to scale up to a few thousands screens.
We suppose that the only data structure fit to manage these numbers is a tree-like structure.
A tree is very uncommon on mobile apps, but we opted for it also because our users are accustomed to it.

# Technology
new technologies should become obsolete later, main stream technologies guarantee more support and documentation.
Some librarys/frameworks was evaluated but proved not satisfactory
- angularjs doesn't support components https://pascalprecht.github.io/2014/10/25/integrating-web-components-with-angularjs/
- polymer limited support by browser and low efficiency with polifit

## why React and React Native?
http://2ality.com/2015/08/web-component-status.html

https://github.com/webcomponents/react-integration

React and React Native look like the best compromise available. 
React (https://facebook.github.io/react/) is a JavaScript library created at Facebook in 2011 and open-sourced in 2013.
React implements a state machine with well defined state transitions.
React is declarative and comes with a custom language [JSX](https://facebook.github.io/react/docs/jsx-in-depth.html).
React doesn't support WebComponents, but proprietary components and Virtual DOM instead of Shadow DOM.

React Native shares the same architecture of React but creates native iOS, Android and UWP applications.
In React Native JavaScript code runs in a separated thread and generates elements by calling asynchronously platform native procedures. 
So most of the computation time is managed by native high efficiency procedures.
React Native supports WebSockets and doesn't support SSE (Server Sent Events) over HTTP/2.
This is not ideal for us, we would prefer standard components and standard language
but
React and React Native encountered a huge success which translates into large support (for instance several books, many pages on github.com and stackoverflow.com)
React Native shares with React almost everithing except the DOM

We used a lint checker (https://en.wikipedia.org/wiki/Lint_(software)) with AirBnB configuration, but we did some exceptions to this set of rules. (https://www.npmjs.com/package/eslint-config-airbnb)

# Architecture
There are two main parts: a pool of servers which are mainly event driven but for a few secondary task are traditional servers and 
clients which interact mainly asynchronously with servers.
## Servers
Two main servers connect PWMA with Tango and EPICS on one side and to WebSocket through a JSON (JavaScript Object Notation) API (Application Programming Interface) on the other side. This API was designed to be as generic as to give the possibility to switch Control System and communication protocol from WebSocket to SSE.
The first server is written in C++ and connects asynchronously to Tango, it performs decoupling of the requests, i.e. if two clients subscribe to the same variable  the control system will receive only one subscription. Attributes not configured to support events are polled very slowly. There is a limited implementation of alarms, only very simple formulae are evaluated and each generated alarm is passed to FCM (Firebase Cloud Messaging).
The second server borrows a Python library. It connects asynchronously to EPICS by using monitors andto the rest of PWMA by adapting a WebSocket library [25]. It is still a proof of concept, but it is complete, except the error management.
The variables coming from both control systems can be mixed because we use FQDN (Fully Qualified Domain Name) and we plan to merge the two main servers in a unique server supporting both EPICS and Tango and any other control system which will be supported in the future.

## Clients
the client part is structured in three levels.
- Components
- Screens
- Starter

### Components
Components are the bricks of PWMA architecture. Almost every component was implemented in two versions: one for the web (using React) and the other for mobile (React Native).
Components depends only from React or React Native, they don't depend on websockets or on the control system. 
In order to avoid the duplication of many components and to depend on other parts, our effort was to keep the number of components as little as possible.
So our aim was to made the components as generic as possible. 
The components are:
- PwmaScalar, used for any scalar value.
- PwmaEncodedArray, a set of values can be encoded as integer or boolean
- PwmaLedArray, a set of boolean values displayed through red and green leds
- PwmaChart, shows an historical chart of a scalar numeric value
- PwmaContiner, a group of components with a title that can be hidden
- PwmaCredentials, a modal for entering user's credentials
- PwmaInput, for entering some data, either a button or some scalar value
- PwmaMain, e frame for all ather components
- PwmaModal, opens a modal (used by PwmaCredentials)
- PwmaStorage, save data in local storage
- PwmaTree, show a series of optins as a tree 
- PwmaDesigner, only on web version, build a new screen using drag and drop

### Screens
Screens are made of components like rooms are made of bricks. Normally they are made only of PWMA components, 
because PWMA component behavior is independent on DOM or React Native components.
There are two kind of screens: dynamically loaded and static.
Dynamically loaded screens are downloaded from a web server at runtime and instantly rendered. 
The advantage is to chose from a potentially unlimited list of screens and the user can easily make his/her custom screens.
Dynamic screens are built using a designer (web only) which supports drag-and-drop technology. 
Variables can be selected from some external GUIs and dropped onto the designer.
Dynamically loaded screens can do only a limited set of operations and display patterns.
Static screens are compiled and embedded in the app. Their purpose is to implement more complex tasks.
A complex task may be split in more then one screen.

### Starter
The starter allows to reach the main screen of all tasks and provides the tools for connecting to the PWMA server through a web socket.
Each task is launched with an optional parameter; this parameter is a string that can be a JSON encoded complex type reduced to a string.

### Alarms
Alarms are configured by the user using cAstor; users are notified of new events through FCM (Firebase Cloud Messaging). 

