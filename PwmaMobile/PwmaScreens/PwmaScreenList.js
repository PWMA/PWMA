import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PwmaMain from '../PwmaComponents/PwmaMain';
import PwmaInput from '../PwmaComponents/PwmaInput';
import PwmaLabel from '../PwmaComponents/PwmaLabel';
// import md5 from '../md5';

/* global fetch */
let id = 0;
function getDate() {
  const m = new Date();
  let sec = m.getUTCSeconds();
  if (sec < 10) sec = `0${sec}`;
  let min = m.getUTCMinutes();
  if (min < 10) min = `0${min}`;
  return `${m.getUTCDate()}/${m.getUTCMonth() + 1}/${m.getUTCFullYear()} ${m.getUTCHours()}:${min}:${sec}`;
}

class PwmaScreenList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screens: [],
      value: {},
    };
    this.ws = '';
    this.callBack = this.callBack.bind(this);
    this.getList = this.getList.bind(this);
  }
  componentDidMount() {
    console.log('PwmaScreenList::componentDidMount()', `https://pwma-dev.elettra.eu/pwma_screens.php?list`);
    fetch(`https://pwma-dev.elettra.eu/pwma_screens.php?list`)
    // fetch(`http://pwma-dev.elettra.eu/saveScreen.php?list`)
    .then(response => response.json())
    .then((mytext) => {
      console.log('mytext: ', mytext);
      this.setState({ screens: mytext });
    }).catch((error) => {
      console.debug(`There has been a problem with your fetch operation: ${error.message}`);
    });
  }

  callBack(data) {
    console.log(data);
    const { navigate } = this.props.globalParams.navigation;
    const domain = '';
    const param = 'openScreen='+data.target; //.split('.')[0];
    navigate('SubPage', { title: data.target.split('.json')[0], component: 'PwmaLoader', device: param, domain, param });
  }

  getList(data) {
    const nodes = [];
    for (let i = 0; i < data.length; i += 1) {
      nodes[i] = (<PwmaInput key={i} text={data[i].title} isButton={true} callback={this.callBack} target={data[i].title} onChangeText={this.callBack} buttonWidth={300} />);
    }
    return nodes;
  }

  render() {
    return (
      <PwmaMain>
        <PwmaLabel text="" />
        {this.getList(this.state.screens)}
      </PwmaMain>
    );
  }
}

export default PwmaScreenList;

PwmaScreenList.propTypes = {
  globalParams: PropTypes.object.isRequired,
};
