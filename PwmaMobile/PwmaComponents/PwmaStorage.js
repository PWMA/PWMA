import {
  AsyncStorage,
} from 'react-native';
/*
AsyncStorage.getAllKeys((err, keys) => {
  console.debug('getAllKeys', err, keys);
});


  export function PwmaStorageInit(key) {
    console.log('PwmaStorage()', operation, key, value)
    try {
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      console.log('ERROR: ', error);
    }
  }
  */

async function appendStorage(key, value) {
  try {
    const oldValue = await AsyncStorage.getItem(key);
    if (oldValue !== null) {
      const val = JSON.parse(oldValue);
      val.append(value);
      try {
        await AsyncStorage.setItem(key, JSON.stringify(val));
      } catch (error) {
        console.log('err: ', error);
      }
    }
  } catch (error) {
    console.log('ERROR: ', error);
    try {
      await AsyncStorage.setItem(key, JSON.stringify([value]));
    } catch (err) {
      console.log('error: ', err);
    }
  }
}

async function setStorage(key, value) {
  try {
    await AsyncStorage.setItem(key, JSON.stringify([value]));
  } catch (error) {
    console.log('error: ', error);
  }
}

async function getStorage(key, saveValue) {
  try {
    // const value = await AsyncStorage.getItem(key);
    AsyncStorage.getItem(key).then((value) => {
      console.log('PwmaStorage::getStorage(), value:', value);
      if (value !== null) {
        saveValue(JSON.parse(value));
      }
    });
  } catch (error) {
    console.log('getStorage(), ERROR: ', error);
  }
}

export default function PwmaStorage(operation, key, value) {
  console.log('PwmaStorage()', operation, key, value);
  if (operation === 'append') appendStorage(key, value);
  else if (operation === 'get') getStorage(key, value);
  else if (operation === 'set') setStorage(key, value);
}

// async saveStorage
