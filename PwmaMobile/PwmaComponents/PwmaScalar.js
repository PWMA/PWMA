import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  // PixelRatio,
} from 'react-native';

// WARNING if this.props.value.alt is missing then label is hidden; TODO: remove this side effect
const pixelRatio = 3; // PixelRatio.get();

export default class PwmaScalar extends Component {
  constructor(props) {
    super(props);
    this.state = { altVisible: false };
    this.switchAlt = this.switchAlt.bind(this);
    console.log(this.props.label);
  }
  switchAlt() {
    this.setState({ altVisible: !this.state.altVisible });
    console.debug('altVisible: ', this.state.altVisible);
  }
  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        paddingTop: this.props.paddingTop,
        backgroundColor: '#f8fff8',
      },
      row: {
        height: 10 * pixelRatio + this.props.paddingTop,
        flexDirection: 'row',
      },
      label: {
        flex: 1,
        fontSize: 7 * pixelRatio,
        textAlign: this.props.labelAlign,
        width: this.props.labelWidth,
        color: '#333333',
        marginRight: 5,
        marginTop: 1 * pixelRatio,
      },
      value: {
        flex: 0,
        height: 10 * pixelRatio,
        width: this.props.valueWidth > 0 ? this.props.valueWidth : 33 * pixelRatio,
        borderWidth: 1,
        paddingRight: 1 * pixelRatio,
        fontSize: 7 * pixelRatio,
        textAlign: 'right',
        backgroundColor: '#FFFFFF',
        fontWeight: 'bold',
        color: 'blue',
        marginHorizontal: 1 * pixelRatio,
      },
      unit: {
        flex: 0,
        fontSize: 5 * pixelRatio,
        width: this.props.unitWidth > 0 ? this.props.unitWidth : 13 * pixelRatio,
        textAlign: 'left',
        color: '#444444',
        marginLeft: 5,
        marginTop: 11,
      },
      padder: {
        flex: this.props.padderFlex,
      },
      alt: {
        marginLeft: 5,
      },
    });
    return (
      <View style={styles.container}>
        <View style={styles.row}>
          {typeof (this.props.label) !== 'undefined' && this.props.label.length > 0 &&
            (<Text style={styles.label}>
              {this.props.label}
            </Text>)
          }
          {typeof (this.props.value) !== 'undefined' &&
            (<TouchableOpacity onPress={this.switchAlt}>
              <Text style={styles.value}>
                {this.props.value.data}
              </Text>
            </TouchableOpacity>)
          }
          {typeof (this.props.unit) !== 'undefined' && this.props.unit.length > 0 &&
            (<Text style={styles.unit}>
              {this.props.unit}
            </Text>)
          }
          <Text style={styles.padder}>{' '}</Text>
        </View>
        {this.state.altVisible && typeof (this.props.value.alt) !== 'undefined' && this.props.value.alt.length > 0 &&
          (<View style={styles.alt}>
            <Text>
              {/* `${this.props.value.alt}\npixelRatio: ${pixelRatio}` */}
              {this.props.value.alt}
            </Text>
          </View>)
        }
        {/* TODO fix flexbox and remove following <View> */}
        {typeof (this.props.value) !== 'undefined' &&
          (<View style={{ height: 1, flexDirection: 'row' }}>
            <Text style={{ flex: 0 }}>{'                                                                                                                                                                                                                                                                                                                           '}</Text>
          </View>)
        }
      </View>
    );
  }
}
PwmaScalar.propTypes = {
  label: PropTypes.string,
  labelAlign: PropTypes.string,
  labelWidth: PropTypes.number,
  value: PropTypes.shape({
    data: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    alt: PropTypes.string,
  }),
  valueWidth: PropTypes.number,
  unit: PropTypes.string,
  unitWidth: PropTypes.number,
  padderFlex: PropTypes.number,
  paddingTop: PropTypes.number,
};
PwmaScalar.defaultProps = {
  label: '',
  labelAlign: 'right',
  labelWidth: 130,
  value: undefined,
  valueWidth: 0,
  unit: '',
  unitWidth: 0,
  padderFlex: 0,
  paddingTop: 5,
};
