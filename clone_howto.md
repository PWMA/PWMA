### How to clone a react-native project in folder PwmaMobile
- git clone https://gitlab.com/PWMA/PWMA.git Sources are downloaded in the PWMA folder
- react-native init PwmaMobile
- add all files in main folder
- cd PwmaMobile
- npm install -save react-navigation
- npm install -save d3
- npm install -save prop-types
- mkdir ./android/app/src/main/assets
- react-native bundle --platform android --dev false --entry-file index.android.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/
- react-native run-android

see also 
- (https://medium.com/the-react-native-log/how-to-rename-a-react-native-app-dafd92161c35)
- npm ls -depth=0

Troubleshooting:
- if the package react-native@0.47.2 gives problem use react-native@0.47.1