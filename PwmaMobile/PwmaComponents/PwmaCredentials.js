import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  TextInput,
} from 'react-native';
import PwmaModal from '../PwmaComponents/PwmaModal';
import PwmaStorage from '../PwmaComponents/PwmaStorage';

export default class PwmaCredentials extends Component {
  constructor() {
    super();
    this.state = {
      valid: '',
      username: '',
    };
    this.args = { username: null, password: null };
    this.validate = this.validate.bind(this);
    this.abort = this.abort.bind(this);
    this.getUsername = this.getUsername.bind(this);
  }
  componentDidMount() {
    PwmaStorage('get', 'PwmaUsername', this.getUsername);
  }
  getUsername(myUsername) {
    const username = myUsername === null ? null: myUsername[0];
    if (myUsername === null){
      console.log('user is Anonymous');
      this.setState({ username: '' });
    }
    else {
      this.username = myUsername[0];
      this.setState({ username: myUsername[0] });
      this.onTextChanged(myUsername[0], 'username');
    }
  }
  onTextChanged(text, parameter) {
    this.args[parameter] = text;
  }

  validate() {
    this.setState({ valid: '' });
    this.props.onSwitchConfirmed(this.args.username, this.args.password);
  }

  abort() {
    this.setState({ valid: '' });
    this.props.onSwitchConfirmed(false, false);
  }

  render() {
    const styles = StyleSheet.create({
      view: {
        marginVertical: 10,
        marginHorizontal: 10,
      },
      message: {
        backgroundColor: '#F32121',
        color: 'white',
      },
      button: {
        borderRadius: 3,
        height: 30,
        backgroundColor: '#2196F3',
        borderColor: '#2196F3',
        color: 'white',
        textAlign: 'center',
        textAlignVertical: 'center',
        marginTop: 15,
      },
    });
    return (
      <PwmaModal visible={this.props.modalVisible} >
        <View style={styles.view}>
          {this.props.message !== '' && (
            <Text style={styles.message}>{` ${this.props.message}`}</Text>
          )}
          <Text>Enter credentials!</Text>
          <Text>Username</Text>
          <TextInput defaultValue={this.state.username} onChangeText={text => this.onTextChanged(text, 'username')} />
          <Text>Password</Text>
          <TextInput onChangeText={text => this.onTextChanged(text, 'password')} secureTextEntry />
          <TouchableOpacity onPress={this.validate}>
            <Text style={styles.button}>OK</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.abort}>
            <Text style={styles.button}>Abort</Text>
          </TouchableOpacity>
        </View>
      </PwmaModal>
    );
  }
}
PwmaCredentials.propTypes = {
  modalVisible: PropTypes.bool,
  onSwitchConfirmed: PropTypes.func.isRequired,
  message: PropTypes.string,
};
PwmaCredentials.defaultProps = {
  modalVisible: false,
  message: '',
};
